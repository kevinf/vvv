﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Linq.Expressions;
using Prism.Models;
using System.Text;

namespace Prism.HtmlHelpers
{
    public static class PrismHtmlHelper
    {
        public static MvcHtmlString EnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, object htmlattribs)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            IEnumerable<TEnum> values = Enum.GetValues(typeof(TEnum)).Cast<TEnum>();

            IEnumerable<SelectListItem> items =
                values.Select(value => new SelectListItem
                {
                    Text = value.ToString(),
                    Value = value.ToString(),
                    Selected = value.Equals(metadata.Model)
                });

            return htmlHelper.DropDownListFor(
                expression,
                items, htmlattribs
                );
        }
        public static MvcHtmlString EnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression)
        {
            return EnumDropDownListFor<TModel, TEnum>(htmlHelper, expression, null);
        }

        //private static string Checked(bool b)
        //{
        //    return b ? "checked='checked'" : string.Empty;
        //}
        //public static string SegValueRows(this HtmlHelper html, Prism.Models.PredefinedSegmentAttributeModel model)
        //{
        //    StringBuilder result = new StringBuilder();

        //    if (model.PredefinedSegmentAttribute.Values == null)
        //        return string.Empty;

        //    if (model.PredefinedSegmentAttribute.Values.Length == 0)
        //        return string.Empty;

        //    int i = 0;
        //    foreach (var v in model.PredefinedSegmentAttribute.Values)
        //    {
        //        result.Append("<tr id='valuerow-" + i.ToString() + "' >");

        //        result.Append("<td>");
        //        result.Append("<input type='hidden' name='PredefinedSegmentAttribute.Values.Index' value='" + i.ToString() + "' />");
        //        result.Append("<span class='editor-field'> <input type='text' class='half' name='PredefinedSegmentAttribute.Values[" + i.ToString() + "].Value' value='");
        //        result.Append(System.Net.WebUtility.HtmlEncode(v.Value));
        //        result.Append("' /></span>");
        //        result.Append("</td>");

        //        result.Append("<td>");
        //        result.Append("<span class='editor-field'>");
        //        result.Append("<input type='checkbox' name=''PredefinedSegmentAttribute.Values[" + i.ToString() + "].ApplyColouring' ");
        //        result.Append(Checked(v.ApplyColouring));
        //        result.Append(" />");
        //        result.Append("</span");
        //        result.Append("</td>");

        //        result.Append("<td>");
        //        result.Append("<span class='editor-field'>");
        //        result.Append("<input class='color' name='PredefinedSegmentAttribute.Values[" + i.ToString() + "].ColourCode' value='");
        //        result.Append(System.Net.WebUtility.HtmlEncode(v.ColourCode));
        //        result.Append("' />");
        //        result.Append("</span");
        //        result.Append("</td>");

        //        result.Append("<td>");
        //        result.Append("<span class='editor-field'>");
        //        result.Append("<input type='checkbox' name='PredefinedSegmentAttribute.Values[" + i.ToString() + "].ShowInTOC' ");
        //        result.Append(Checked(v.ShowInTOC));
        //        result.Append(" />");
        //        result.Append("</span");
        //        result.Append("</td>");

        //        result.Append("<td>");
        //        result.Append("<span class='editor-field'>");
        //        result.Append("<input type='checkbox' name=''PredefinedSegmentAttribute.Values[" + i.ToString() + "].IsTOCText' ");
        //        result.Append(Checked(v.IsTOCText));
        //        result.Append(" />");

        //        result.Append("</span");
        //        result.Append("</td>");

        //        result.Append("<td>");
        //        result.Append("<input type='button' onclick='deletevalue(this)' value='Delete' />");
        //        result.Append("</td>");
        //        result.Append("</tr>");
        //        i++;


        //    }


        //    return result.ToString();
        //}

        //public static string SegAttribRows(this HtmlHelper html, Prism.Models.SegmentDefinitionModel model)
        //{
           

        //    StringBuilder result = new StringBuilder();

        //    if (model.Attributes == null)
        //        return string.Empty;

        //    if (model.Attributes.Length == 0)
        //        return string.Empty;

        //    int i = 0;
        //    foreach (var a in model.Attributes)
        //    {
        //        result.Append("<tr id='attribrow-" + i.ToString() + "' >");

        //        result.Append("<td>");
        //        result.Append("<input type='hidden' name='Attributes.Index' value='" + i.ToString() + "' />");
        //        result.Append("<span class='editor-field'> <input type='text' class='half' name='Attributes[" + i.ToString() + "].Name' value='");
        //        result.Append(System.Net.WebUtility.HtmlEncode(a.Name));
        //        result.Append("' /></span>");
        //        result.Append("</td>");

        //        result.Append("<td>");
        //        result.Append("<span class='editor-field'><input type='text' class='half' name='Attributes[" + i.ToString() + "].Value' value='");
        //        result.Append(System.Net.WebUtility.HtmlEncode(a.Value));
        //        result.Append("' /></span>");
        //        result.Append("</td>");
        //        result.Append("<td>");
        //        result.Append("<input type='button' onclick='deleteattrib(this)' value='Delete' />");
        //        result.Append("</td>");
        //        result.Append("</tr>");
        //        i++;
        //    }




        //    return result.ToString();

        //}
    }
}