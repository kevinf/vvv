﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using EblaAPI;
using EblaImpl;
using Prism.Controllers;
using System.Configuration;
using System.Xml;

namespace Prism
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

        }

        protected void Application_Start()
        {
            //RouteTable.Routes.Ignore("{*pathInfo}", new { pathInfo = @"^.*(ChartImg.axd)$" }); 

            AreaRegistration.RegisterAllAreas();

            ModelBinders.Binders.DefaultBinder = new TrimModelBinder();

            RegisterRoutes(RouteTable.Routes);

            EblaCxnMgr.Init(PrismEblaSupport.GetConnection, PrismEblaSupport.SetConnection, Prism.PrismEblaSupport.GetRequestData, PrismEblaSupport.SetRequestData);

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            // Dispose any DB connection
            object o = PrismEblaSupport.GetConnection();
            if (o != null)
            {
                IDisposable d = o as IDisposable;
                if (d != null)
                    d.Dispose();
            }
        } 

        public class TrimModelBinder : DefaultModelBinder
        {
            protected override void SetProperty(ControllerContext controllerContext,
            ModelBindingContext bindingContext,
            System.ComponentModel.PropertyDescriptor propertyDescriptor, object value)
            {
                if (propertyDescriptor.PropertyType == typeof(string))
                {
                    var stringValue = (string)value;
                    if (!string.IsNullOrEmpty(stringValue))
                        stringValue = stringValue.Trim();

                    value = stringValue;
                }

                base.SetProperty(controllerContext, bindingContext,
                propertyDescriptor, value);
            }
        }


    }

    internal class PrismEblaSupport
    {
        internal static object GetConnection()
        {
            // Use the per-request object cache (http://www.4guysfromrolla.com/articles/060904-1.aspx)
            return System.Web.HttpContext.Current.Items["EblaConn"];
        }

        internal static void SetConnection(object conn)
        {
            System.Web.HttpContext.Current.Items["EblaConn"] = conn;
        }

        internal static object GetRequestData()
        {
            // Use the per-request object cache (http://www.4guysfromrolla.com/articles/060904-1.aspx)
            return System.Web.HttpContext.Current.Items["EblaReqData"];
        }

        internal static void SetRequestData(object data)
        {
            System.Web.HttpContext.Current.Items["EblaReqData"] = data;
        }
    
    }

    internal class PrismHelpers
    {
        internal static ICorpus GetCorpus(string name)
        {
            // TODO - consider caching in Session
            ICorpus corpus = EblaFactory.GetCorpus(ConfigurationManager.AppSettings);

            if (!corpus.Open(name, AccountController.SessionEblaUsername, AccountController.SessionEblaPassword))
                throw new Exception("You do not have permission to access the corpus.");

            return corpus;
        }

        internal static bool CanWrite(string CorpusName)
        {
            if (AccountController.SessionEblaIsAdmin)
                return true;

            // TODO - something more efficient than this.
            ICorpusStore store = EblaFactory.GetCorpusStore(ConfigurationManager.AppSettings);

            store.Open(AccountController.SessionEblaUsername, AccountController.SessionEblaPassword);

            var rights = store.GetUserInfo(AccountController.SessionEblaUsername, null);

                foreach (var r in rights[0].CorpusRights)
                {
                    if (string.Compare(r.CorpusName, CorpusName) == 0)
                    {
                        if (r.CanWrite)
                            return true;
                        break;
                    }
                }

            return false;
        }

        internal static IDocument GetBaseText(string corpusName)
        {
            // TODO - consider caching in Session
            IDocument doc = EblaFactory.GetDocument(ConfigurationManager.AppSettings);

            if (!doc.OpenBaseText(corpusName, AccountController.SessionEblaUsername, AccountController.SessionEblaPassword))
                throw new Exception("You do not have permission to access the document.");

            return doc;
        }

        internal static IDocument GetVersion(string corpusName, string versionName)
        {
            // TODO - consider caching in Session
            IDocument doc = EblaFactory.GetDocument(ConfigurationManager.AppSettings);

            if (!doc.OpenVersion(corpusName, versionName, AccountController.SessionEblaUsername, AccountController.SessionEblaPassword))
                throw new Exception("You do not have permission to access the document.");

            return doc;
        }

        internal static IAlignmentSet GetAlignmentSet(string corpusName, string versionName)
        {
            // TODO - consider caching in Session
            IAlignmentSet set = EblaFactory.GetAlignmentSet(ConfigurationManager.AppSettings);

            if (!set.Open(corpusName, versionName, AccountController.SessionEblaUsername, AccountController.SessionEblaPassword))
                throw new Exception("You do not have permission to access the alignment set.");

            return set;
        }


        internal static IDocument GetDocument(string corpusName, string nameIfVersion)
        {
            if (string.IsNullOrEmpty(nameIfVersion))
                return GetBaseText(corpusName);

            return GetVersion(corpusName, nameIfVersion);
        }

        internal static System.Text.Encoding PrismEncodingToTextEncoding(PrismEncoding encoding)
        {
            switch (encoding)
            {
                case PrismEncoding.ASCII:
                    return System.Text.Encoding.ASCII;
                case PrismEncoding.UTF8:
                    return System.Text.Encoding.UTF8;
                case PrismEncoding.UTF7:
                    return System.Text.Encoding.UTF7;
                case PrismEncoding.UTF32:
                    return System.Text.Encoding.UTF32;
                case PrismEncoding.Unicode:
                    return System.Text.Encoding.Unicode;
                case PrismEncoding.BigEndianUnicode:
                    return System.Text.Encoding.BigEndianUnicode;

            }

            throw new Exception("Unrecognised PrismEncoding: " + encoding.ToString());
        }

        internal static XmlDocument XmlDocFromContent(string content)
        {
            string xml = @"<?xml version=""1.0"" ?><body>";

            xml += content;

            xml += "</body>";

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xml);
            return xmlDoc;
        }
    }

    public enum PrismEncoding
    {
        ASCII,
        UTF8,
        Unicode,
        UTF7,
        UTF32,
        BigEndianUnicode
    }
}