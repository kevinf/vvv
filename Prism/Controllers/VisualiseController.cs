﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EblaAPI;
using EblaImpl;
using Prism.Models;

namespace Prism.Controllers
{
    [Authorize]
    public class VisualiseController : PrismController
    {
        //==============================================
        //
        //  Page rendering
        //
        //==============================================

        public ActionResult Index(string CorpusName, string NameIfVersion)
        {
            try
            {
                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);

                // Get the colouring defined for certain segment types depending on their attributes.
                // We'll use this primarily for example purposes - it's more likely on this screen
                // that colour will be used to represent Viv/Eddy values

                //EblaAPI.PredefinedSegmentAttribute[] atts = corpus.GetPredefinedSegmentAttributes();
                //ViewData["colourdata"] = DocumentController.GetColouringData(atts);
                ViewData["corpusname"] = CorpusName;
                ViewData["corpusdescription"] = corpus.GetDescription();

                IDocument basetextDoc = PrismHelpers.GetDocument(CorpusName, null);
                IDocument versionDoc = PrismHelpers.GetDocument(CorpusName, NameIfVersion);

                // For now, just get the whole of each document, with markup for all segments.
                // Note: first 'true' flag requests markup be inserted to add '[' and ']' markers.
                // Second 'true' flag requests markup be inserted around segment text, to make it selectable by ID etc.
                // for colouring purposes.
                // First flag could be set 'false' if markers aren't needed.
                ViewData["basetextcontent"]  = basetextDoc.GetDocumentContent(0, basetextDoc.Length(), false, true, null, false);
				ViewData["basetextdate"] = basetextDoc.GetMetadata().ReferenceDate;
                ViewData["versioncontent"] = versionDoc.GetDocumentContent(0, versionDoc.Length(), false, true, null, false);
				ViewData["versionname"] = NameIfVersion;
				ViewData["versiondate"] = versionDoc.GetMetadata().ReferenceDate;

                return View();

            }
            catch (Exception ex)
            {
                string message = "An error occurred: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }
        }

		//
        public ActionResult Viv(string CorpusName, int? vivFloor, int? vivCeiling)
        {
            try
            {
                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);

                // Get the colouring defined for certain segment types depending on their attributes.
                // We'll use this primarily for example purposes - it's more likely on this screen
                // that colour will be used to represent Viv/Eddy values

                //EblaAPI.PredefinedSegmentAttribute[] atts = corpus.GetPredefinedSegmentAttributes();
                //ViewData["colourdata"] = DocumentController.GetColouringData(atts);
                ViewData["corpusname"] = CorpusName;
                ViewData["corpusdescription"] = corpus.GetDescription();

                int vivFloorValue = 0;
                int vivCeilingValue = 0;

                // Use some hard-coded defaults for now
                vivFloorValue = 50;
                vivCeilingValue = 85;

                if (vivFloor.HasValue)
                    vivFloorValue = vivFloor.Value;
                if (vivCeiling.HasValue)
                    vivCeilingValue = vivCeiling.Value;

                ViewData["vivfloor"] = vivFloorValue.ToString();
                ViewData["vivceiling"] = vivCeilingValue.ToString();

                IDocument basetextDoc = PrismHelpers.GetDocument(CorpusName, null);

                // For now, just get the whole of each document, with markup for all segments.
                // Note: first 'true' flag requests markup be inserted to add '[' and ']' markers.
                // Second 'true' flag requests markup be inserted around segment text, to make it selectable by ID etc.
                // for colouring purposes.
                // First flag could be set 'false' if markers aren't needed.
                ViewData["basetextcontent"] = basetextDoc.GetDocumentContent(0, basetextDoc.Length(), false, true, null, false);

                return View();

            }
            catch (Exception ex)
            {
                string message = "An error occurred: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }
		}

        public ActionResult Alignments(string CorpusName)
        {
            try
            {
                var model = CorpusController.GetModel(CorpusName);

                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);
				String versionList = "<select><option value='' >Select</option>";

                foreach (var v in model.VersionList)
                {
                    string nameWithDate = v.NameIfVersion;
                    if (v.ReferenceDate.HasValue)
                        nameWithDate += " (" + v.ReferenceDate.Value.ToString() + ")";
                    versionList += "<option value='" + v.NameIfVersion + "'>" + nameWithDate + "</option>";
                }

                //foreach ( String nameWithDate in corpus.GetVersionListWithDates() )
                //{
                //    versionList += "<option>" + nameWithDate + "</option>";
                //}
				versionList += "</select>";

                // Get the colouring defined for certain segment types depending on their attributes.
                // We'll use this primarily for example purposes - it's more likely on this screen
                // that colour will be used to represent Viv/Eddy values

                //EblaAPI.PredefinedSegmentAttribute[] atts = corpus.GetPredefinedSegmentAttributes();
                //ViewData["colourdata"] = DocumentController.GetColouringData(atts);
                ViewData["corpusname"] = CorpusName;
				ViewData["corpusdescription"] = corpus.GetDescription();
				ViewData["versionlist"] = versionList;

                IDocument basetextDoc = PrismHelpers.GetDocument(CorpusName, null);
                ViewData["basetextcontent"] = basetextDoc.GetDocumentContent(0, basetextDoc.Length(), false, true, null, false);
				ViewData["basetextdate"] = basetextDoc.GetMetadata().ReferenceDate;

                return View();

            }
            catch (Exception ex)
            {
                string message = "An error occurred: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }
		}

		public ActionResult OthelloMap(string CorpusName)
		{
			try
			{
				ViewData["corpusname"] = CorpusName;
				return View();	
			}
			catch (Exception ex)
			{
				string message = "An error occurred: " + ex.Message;
				return RedirectToAction("Error", "Home", new { message = message });
			}
		}

        //==============================================
        //
        //  AJAX calls
        //
        //==============================================

        [HttpGet]
        public ActionResult VivTest(string CorpusName, int SegmentID)
        {
            ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);

            var v = corpus.CalculateSegmentVariation(SegmentID, VariationMetricTypes.metricA);

			return View("VivReportDialog", v);
        }

        [HttpPost]
        public ActionResult GraphData(string CorpusName, VariationMetricTypes metricType, int? BaseTextSegmentID)
        {
            try
            {
                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);

                var graphModelMap = new Dictionary<string, GraphModel>();

                if (BaseTextSegmentID.HasValue)
                {

                    var v = corpus.CalculateSegmentVariation(BaseTextSegmentID.Value, metricType);

                    foreach (var versionstats in v.VersionSegmentVariations)
                    {
                        GraphModel graphModel = new GraphModel();
                        graphModel.VersionName = versionstats.VersionName;
                        graphModel.EddyValue = versionstats.EddyValue;
                        graphModelMap.Add(graphModel.VersionName, graphModel);
                    }
                }
                else
                {
                    string[] versions = corpus.GetVersionList();

                    foreach (string s in versions)
                    {
                        GraphModel graphModel = new GraphModel();
                        graphModel.VersionName = s;
                        graphModel.EddyValue = corpus.RetrieveAverageVariation(s, metricType);
                        graphModelMap.Add(graphModel.VersionName, graphModel);

                    }
                }

                foreach (string s in graphModelMap.Keys)
                {
                    IDocument doc = PrismHelpers.GetDocument(CorpusName, s);
                    var m = doc.GetMetadata();

                    graphModelMap[s].ReferenceDate = m.ReferenceDate;
                    graphModelMap[s].Genre = m.Genre;
                }

                GraphResultModel result = new GraphResultModel();
                result.Succeeded = true;

                result.GraphModels = graphModelMap.Values.ToArray();

                return MonoWorkaroundJson(result);

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }
        }

    }
}
