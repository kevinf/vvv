﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using System.Web.UI.DataVisualization.Charting;
using System.IO;
using EblaAPI;
using Prism.Models;

namespace Prism.Controllers
{
    public class ChartsController : PrismController
    {
        //
        //// GET: /Charts/

        //public ActionResult Index()
        //{

        //    var Chart1 = new Chart() {Width = 600, Height = 400};

        //    var series1 = Chart1.Series.Add("Series1");
        //    var series2 = Chart1.Series.Add("Series2");
        //    var area = Chart1.ChartAreas.Add("ChartArea1");

        //    Random random = new Random();
        //    for (int pointIndex = 0; pointIndex < 10; pointIndex++)
        //    {
        //        Chart1.Series["Series1"].Points.AddY(random.Next(45, 95));
        //        Chart1.Series["Series2"].Points.AddY(random.Next(5, 75));
        //    }

        //    // Set series chart type
        //    Chart1.Series["Series1"].ChartType = (SeriesChartType)Enum.Parse(typeof(SeriesChartType), "Line", true);
        //    Chart1.Series["Series2"].ChartType = (SeriesChartType)Enum.Parse(typeof(SeriesChartType), "Line", true);

        //    // Set point labels
        //    //if (PointLabelsList.SelectedItem.Text != "None")
        //    //{
        //    //    Chart1.Series["Series1"].IsValueShownAsLabel = true;
        //    //    Chart1.Series["Series2"].IsValueShownAsLabel = true;
        //    //    if (PointLabelsList.SelectedItem.Text != "Auto")
        //    //    {
        //    //        Chart1.Series["Series1"]["LabelStyle"] = PointLabelsList.SelectedItem.Text;
        //    //        Chart1.Series["Series2"]["LabelStyle"] = PointLabelsList.SelectedItem.Text;
        //    //    }
        //    //}

        //    // Set X axis margin
        //    //Chart1.ChartAreas["ChartArea1"].AxisX.IsMarginVisible = ShowMargins.Checked;

        //    //// Show as 2D or 3D
        //    //if (checkBoxShow3D.Checked)
        //    //{
        //    //    Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
        //    //    Chart1.Series["Series1"]["ShowMarkerLines"] = "True";
        //    //    Chart1.Series["Series2"]["ShowMarkerLines"] = "True";
        //    //    Chart1.Series["Series1"].BorderWidth = 1;
        //    //    Chart1.Series["Series2"].BorderWidth = 1;
        //    //    Chart1.Series["Series1"].MarkerStyle = MarkerStyle.None;
        //    //    Chart1.Series["Series2"].MarkerStyle = MarkerStyle.None;
        //    //}
        //    //else
        //    {
        //        Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
        //        Chart1.Series["Series1"].BorderWidth = 3;
        //        Chart1.Series["Series2"].BorderWidth = 3;
        //        Chart1.Series["Series1"].MarkerStyle = MarkerStyle.Square;
        //        Chart1.Series["Series2"].MarkerStyle = MarkerStyle.Triangle;
        //    }


        //    var imgStream = new MemoryStream();
        //    Chart1.SaveImage(imgStream, ChartImageFormat.Png);
        //    imgStream.Seek(0, SeekOrigin.Begin);

        //    // Return the contents of the Stream to the client
        //    return File(imgStream, "image/png");

        //}

        //public ActionResult Test()
        //{
        //    var Chart1 = new Chart() { Width = 600, Height = 400 };

        //    var series1 = Chart1.Series.Add("Series1");
        //    var series2 = Chart1.Series.Add("Series2");
        //    var area = Chart1.ChartAreas.Add("ChartArea1");

        //    Random random = new Random();
        //    for (int pointIndex = 0; pointIndex < 10; pointIndex++)
        //    {
        //        Chart1.Series["Series1"].Points.AddY(random.Next(45, 95));
        //        Chart1.Series["Series2"].Points.AddY(random.Next(5, 75));
        //    }

        //    // Set series chart type
        //    Chart1.Series["Series1"].ChartType = (SeriesChartType)Enum.Parse(typeof(SeriesChartType), "Line", true);
        //    Chart1.Series["Series2"].ChartType = (SeriesChartType)Enum.Parse(typeof(SeriesChartType), "Line", true);

        //    return View(Chart1);
        //}

        public ActionResult SimpleEddyChart(SimpleEddyChartModel model)
        {
            try
            {
                IDocument baseText = PrismHelpers.GetBaseText(model.CorpusName);

                var seg = baseText.GetSegmentDefinition(model.BaseTextSegmentId);
                //ViewData["basetext"] = baseText.GetDocumentContentText(seg.StartPosition, seg.Length);
                ViewData["basetext"] = baseText.GetDocumentContent(seg.StartPosition, seg.Length, false, false, null, false);

                return View(model);

            }
            catch (Exception ex)
            {
                string message = "An error occurred: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }

        }

        public ActionResult EddyOverviewChart(SearchModel model)
        {
            try
            {

                return View(model);
            }
            catch (Exception ex)
            {
                string message = "An error occurred: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }


        }



        public ActionResult EddyHistory(EddyHistorySeriesModel model)
        {
            try
            {

                return View(model);
            }
            catch (Exception ex)
            {
                string message = "An error occurred: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }

        }

        [HttpPost]
        public ActionResult GetEddyOverviewChartData(SearchModel model, int metricTypeVal)
        {
            if (!Enum.IsDefined(typeof(VariationMetricTypes), metricTypeVal))
                throw new Exception ("Unrecognised metric type value: " + metricTypeVal.ToString());

            VariationMetricTypes metricType = (VariationMetricTypes)metricTypeVal;

            try 
            {
                EddyOverviewChartResultModel results = new EddyOverviewChartResultModel();

                IDocument baseText = PrismHelpers.GetBaseText(model.CorpusName);

                var corpus = PrismHelpers.GetCorpus(model.CorpusName);

                var segs = baseText.FindSegmentDefinitions(0, baseText.Length(), false, false, null);

                var segMap = new Dictionary<int, SegmentDefinition>();
                foreach (var seg in segs)
                    segMap.Add(seg.ID, seg);

                var verlist = new List<string>(corpus.GetVersionList());
                verlist.Sort();

                var datelist = new List<int?>();
                foreach (string version in verlist)
                {
                    var versionDoc = PrismHelpers.GetVersion(model.CorpusName, version);
                    datelist.Add(versionDoc.GetMetadata().ReferenceDate);
                }

                var versions = verlist.ToArray();
                var dates = datelist.ToArray();

                var series = new Dictionary<string, List<double>>();
                foreach (string version in versions)
                {
                    series.Add(version, new List<double>());
                }

                var data = corpus.RetrieveMultipleSegmentVariationData(DocumentController.EblaAttribsFromModel(model.AttributeFilters).ToArray(),metricType);

                Dictionary<string, int> versionDenominators = new Dictionary<string, int>();

                List<int> baseTextSegIDs = new List<int>();
                List<int> segStartPositions = new List<int>();

                for (int i = 0; i < data.VersionNames.Length; i++)
                {
                    for (int j = 0; j < data.BaseTextSegmentIDs.Length; j++)
                    {
                        segStartPositions.Add(segMap[data.BaseTextSegmentIDs[j]].StartPosition);
                        baseTextSegIDs.Add(data.BaseTextSegmentIDs[j]);
                        series[data.VersionNames[i]].Add(data.EddyValues[i][j]);

                    }

                }


                //int limit = 50;
                //int count = 0;
                //foreach (var seg in segs)
                //{

      
                //    var variation = corpus.RetrieveSegmentVariation(seg.ID, VariationMetricTypes.metricA);

                //    if (variation == null)
                //        continue
                //            ;

                //    var tempHash = new HashSet<string>(versions);

                //    foreach (var v in variation.VersionSegmentVariations)
                //    {
                //        series[v.VersionName].Add(v.EddyValue);
                //        tempHash.Remove(v.VersionName);
                //    }

                //    foreach (string version in tempHash)
                //        series[version].Add(0);

                //    count++;
                //    if (count > limit)
                //        break;

                //}

                results.Versions = versions;
                results.VersionDates = dates;
                results.Succeeded = true;
                results.BaseTextSegIds = baseTextSegIDs.ToArray();
                results.EddyValues = new double[versions.Length][];
                results.SegmentStartPositions = segStartPositions.ToArray();

                for (int i = 0; i < versions.Length; i++)
                    results.EddyValues[i] = series[versions[i]].ToArray();

                return MonoWorkaroundJson(results);
            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }

        }


        [HttpPost]
        public ActionResult GetEddyHistoryData(EddyHistorySeriesModel model)
        {
            try
            {
                SimpleEddyChartResultModel result = new SimpleEddyChartResultModel();
                ICorpus corpus = PrismHelpers.GetCorpus(model.CorpusName);

                var versionDates = GetVersionDates(model.CorpusName);

                List<EddyPointData> resultPoints = null;
                string legend = string.Empty;

                if (model.BaseTextSegmentId > -1)
                {
                    legend = "Base text segment " + model.BaseTextSegmentId.ToString();

                    resultPoints = GetEddyPoints(versionDates, corpus, model.BaseTextSegmentId);

                    IDocument baseText = PrismHelpers.GetBaseText(model.CorpusName);

                    var seg = baseText.GetSegmentDefinition(model.BaseTextSegmentId);
                    string s = baseText.GetDocumentContentText(seg.StartPosition, seg.Length);

                    //string temp = baseText.GetDocumentContent(seg.StartPosition, seg.Length, false, false, null);

                    //if (temp.Length > 10)
                    //    legend += "\"" + temp + "[...]
                    //else
                    //    legend += "\"" + temp + "\"";

                    int maxLength = 40;
                    int trimLength = maxLength / 2 - 2;
                    if (s.Length > maxLength)
                    {
                        string abbr = s.Substring(0, trimLength) + "[...]" + s.Substring(s.Length - trimLength);
                        s = abbr;
                    }

                    legend += " \"" + s.Trim() + "\"";
                }
                else
                {

                    legend = "All segments";
                    if (model.AttributeFilters != null)
                        if (model.AttributeFilters.Length > 0)
                        {


                        }
                    Dictionary<string, double> eddyTotals = new Dictionary<string, double>();

                    var data = corpus.RetrieveMultipleSegmentVariationData(DocumentController.EblaAttribsFromModel(model.AttributeFilters).ToArray(), VariationMetricTypes.metricA);

                    Dictionary<string, int> versionDenominators = new Dictionary<string, int>();

                    for (int i = 0; i < data.VersionNames.Length; i++)
                    {
                        for (int j = 0; j < data.BaseTextSegmentIDs.Length; j++)
                        {
                            if (data.EddyValues[i][j] >= 0)
                            {
                                if (eddyTotals.ContainsKey(data.VersionNames[i]))
                                    eddyTotals[data.VersionNames[i]] += data.EddyValues[i][j];
                                else
                                    eddyTotals.Add(data.VersionNames[i], data.EddyValues[i][j]);

                                if (versionDenominators.ContainsKey(data.VersionNames[i]))
                                    versionDenominators[data.VersionNames[i]]++;
                                else
                                    versionDenominators.Add(data.VersionNames[i], 1);
                            }

                        }

                    }

                    resultPoints = new List<EddyPointData>();
                    foreach (string v in eddyTotals.Keys)
                    {
                        var p = new EddyPointData();
                        p.Version = v;
                        p.Point = new double[] { versionDates[v], eddyTotals[v] / versionDenominators[v] };
                        resultPoints.Add(p);
                    }

                    /*IDocument baseText = PrismHelpers.GetBaseText(model.CorpusName);

                    var basetextSegDefs = baseText.FindSegmentDefinitions(0, baseText.Length(), false, false, DocumentController.EblaAttribsFromModel(model.AttributeFilters).ToArray());
                    foreach (var seg in basetextSegDefs)
                    {
                        if (seg.Length > 0)
                        {
                            var points = GetEddyPoints(versionDates, corpus, seg.ID);

                            foreach (var point in points)
                            {
                                if (eddyTotals.ContainsKey(point.Version))
                                    eddyTotals[point.Version] += point.Point[1];
                                else
                                    eddyTotals.Add(point.Version, point.Point[1]);
                            }

                        }

                    }

                    resultPoints = new List<EddyPointData>();
                    foreach (string v in eddyTotals.Keys)
                    {
                        var p = new EddyPointData();
                        p.Version = v;
                        p.Point = new double[] { versionDates[v], eddyTotals[v] / basetextSegDefs.Length };
                        resultPoints.Add(p);
                    }*/


                }


                resultPoints.Sort((a, b) => (a.Point[0].CompareTo(b.Point[0])));

                List<double[]> temp = new List<double[]>();
                List<string> temp2 = new List<string>();

                foreach (var p in resultPoints)
                {
                    temp.Add(p.Point);
                    temp2.Add(p.Version);
                }


                result.Points = temp.ToArray();
                result.Versions = temp2.ToArray();
                result.Legend = legend;

                result.Succeeded = true;

                return MonoWorkaroundJson(result);
            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }

        }

        List<EddyPointData> GetEddyPoints(Dictionary<string, int> versionDates, ICorpus corpus, int baseTextSegmentId)
        {
            var points = new List<EddyPointData>();


            var results = corpus.RetrieveSegmentVariationData(baseTextSegmentId, VariationMetricTypes.metricA);

            for (int i = 0; i < results.VersionNames.Length; i++)
            {
                if (versionDates.ContainsKey(results.VersionNames[i]))
                {
                    if (results.EddyValues[i] >= 0)
                    {
                        var d = new EddyPointData();
                        d.Point = new double[2];
                        d.Point[0] = versionDates[results.VersionNames[i]];
                        d.Point[1] = results.EddyValues[i];
                        d.Version = results.VersionNames[i];
                        points.Add(d);

                    }

                }
            }

            //var results = corpus.RetrieveSegmentVariation(baseTextSegmentId, VariationMetricTypes.metricA);

            //foreach (var v in results.VersionSegmentVariations)
            //{
            //    //int? refDate = versionDates[v.VersionName];
            //    if (versionDates.ContainsKey(v.VersionName))
            //    {
            //        var d = new EddyPointData();
            //        d.Point = new double[2];
            //        d.Point[0] = versionDates[v.VersionName];
            //        d.Point[1] = v.EddyValue;
            //        d.Version = v.VersionName;
            //        points.Add(d);

            //    }

            //}

            return points;
        }


        class EddyPointData
        {
            public string Version;
            public double[] Point;
        }

        Dictionary<string, int> GetVersionDates(string corpusName)
        {
            Dictionary<string, int> versionDates = new Dictionary<string, int>();
            ICorpus corpus = PrismHelpers.GetCorpus(corpusName);

            string[] versions = corpus.GetVersionList();
            foreach (string version in versions)
            {
                IDocument doc = PrismHelpers.GetVersion(corpusName, version);
                var m = doc.GetMetadata();
                if (m.ReferenceDate.HasValue)
                    versionDates.Add(version, m.ReferenceDate.Value);
            }
            return versionDates;
        }

        [HttpPost]
        public ActionResult GetSimpleEddyChartData(SimpleEddyChartModel model)
        {
            try
            {
                SimpleEddyChartResultModel result = new SimpleEddyChartResultModel();


                ICorpus corpus = PrismHelpers.GetCorpus(model.CorpusName);

                var points = new List<EddyPointData>();

                Dictionary<string, int?> versionDates = new Dictionary<string, int?>();

                string[] versions = corpus.GetVersionList();
                foreach (string version in versions)
                {
                    IDocument doc = PrismHelpers.GetVersion(model.CorpusName, version);
                    versionDates.Add(version, doc.GetMetadata().ReferenceDate);
                }

                var results = corpus.RetrieveSegmentVariation(model.BaseTextSegmentId, VariationMetricTypes.metricA);

                foreach (var v in results.VersionSegmentVariations)
                {
                    int? refDate = versionDates[v.VersionName];
                    if (refDate.HasValue)
                    {
                        var d = new EddyPointData();
                        d.Point = new double[2];
                        d.Point[0] = refDate.Value;
                        d.Point[1] = v.EddyValue;
                        d.Version = v.VersionName;
                        points.Add(d);

                    }

                }

                points.Sort( (a,b) => (a.Point [0].CompareTo(b.Point[0])) );

                List<double[]> temp = new List<double[]>();
                List<string> temp2 = new List<string>();

                foreach (var p in points)
                {
                    temp.Add(p.Point);
                    temp2.Add(p.Version);
                }


                result.Points = temp.ToArray();
                result.Versions = temp2.ToArray();
                result.Succeeded = true;

                return MonoWorkaroundJson(result);
            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }
        }

        //public ActionResult EddyChart(string CorpusName, int baseTextSegmentId, int chartWidth, int chartHeight)
        //{
        //    var chart = new Chart() { Width = chartWidth, Height = chartHeight };

        //    var series1 = chart.Series.Add("Series1");

        //    series1.ChartType = SeriesChartType.Point;

        //    series1["LabelStyle"] = "Top";

        //    var area = chart.ChartAreas.Add("ChartArea1");

        //    ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);

        //    Dictionary<string, int?> versionDates = new Dictionary<string, int?>();

        //    string[] versions = corpus.GetVersionList();
        //    foreach (string version in versions)
        //    {
        //        IDocument doc = PrismHelpers.GetVersion(CorpusName, version);
        //        versionDates.Add(version, doc.GetMetadata().ReferenceDate);
        //    }

        //    var results = corpus.RetrieveSegmentVariation(baseTextSegmentId, VariationMetricTypes.metricA);

        //    foreach (var v in results.VersionSegmentVariations)
        //    {
        //        int? refDate = versionDates[v.VersionName];
        //        if (refDate.HasValue)
        //        {
        //            var d = new DataPoint();
        //            d.XValue = refDate.Value;

        //            d.YValues = new double[] { v.EddyValue };

        //            d.Label = v.VersionName;
        //            d.ToolTip = d.Label;

        //            series1.Points.Add(d);
        //        }

                
        //        //d.XValue = v.
        //        //var p = series1.Points.Add(
        //    }








        //    return View(chart);


        //    //var imgStream = new MemoryStream();
        //    //chart.SaveImage(imgStream, ChartImageFormat.Png);
        //    //imgStream.Seek(0, SeekOrigin.Begin);

        //    ////string map = chart.GetHtmlImageMap(
            
        //    //// Return the contents of the Stream to the client
        //    //return File(imgStream, "image/png");

        //}
    }
}
