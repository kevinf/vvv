using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using EblaAPI;
using Prism.Models;

namespace Prism.Controllers
{
	public class ApiController : PrismController
	{
		[HttpGet]
		public ActionResult GetVersionText( string CorpusName, string VersionName )
		{
            try
            {
                IDocument versionDoc = PrismHelpers.GetVersion(CorpusName, VersionName);
                var o = new { Succeeded = true, Text = versionDoc.GetDocumentContent(0, versionDoc.Length(), false, true, null, false) };

                return MonoWorkaroundJson(o);
            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });
            }
		}

		[HttpGet]
		public ActionResult GetSegmentInformation(string CorpusName, string VersionName, bool includeAttribs)
		{
			try
			{
				IDocument doc = PrismHelpers.GetVersion(CorpusName, VersionName);
				var segments = doc.FindSegmentDefinitions(0, doc.Length(), false, false, null);
				
				var results = new List<SegmentDefinitionModel2>();
				foreach (var s in segments)
				{
					var result = new SegmentDefinitionModel2();
					result.ID = s.ID;
					result.length = s.Length;
					result.startPos = s.StartPosition;
					if (includeAttribs)
					{
						List<AttributeModel> attribs = new List<AttributeModel>();
						foreach (var a in s.Attributes)
						{
							var attrib = new AttributeModel();
							attrib.Name = a.Name;
							attrib.Value = a.Value;
							attribs.Add(attrib);
						}
						result.Attributes = attribs.ToArray();
					}
					results.Add(result);
				}
				
				var model = new SegmentDefinitionsModel();
				model.Succeeded = true;
				model.DocumentLength = doc.Length();
				model.SegmentDefinitions = results.ToArray();
				return MonoWorkaroundJson(model);
				
			}
			catch (Exception ex)
			{
				return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });
			}
		}

		[HttpGet]
		public ActionResult GetSegmentVariation( string CorpusName, int SegmentID, int? metricType )
		{
			try
			{
				ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);
				VariationMetricTypes temp = metricType.HasValue ? (VariationMetricTypes)(metricType) : VariationMetricTypes.metricA;

				var result = new SegmentVariationResultModel { Succeeded = true };
				result.SegmentVariationData = corpus.CalculateSegmentVariation(SegmentID, temp);

				return MonoWorkaroundJson(result);
			}
			catch (Exception ex)
			{
				return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });
			}
		}
	}
}

