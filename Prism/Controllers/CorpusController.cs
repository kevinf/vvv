/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EblaAPI;
using EblaImpl;
using System.Configuration;
using Prism.Models;
using System.Globalization;

namespace Prism.Controllers
{
    [Authorize]
    public class CorpusController : PrismController
    {
        //==============================================
        //
        //  Page rendering
        //
        //==============================================


        public ActionResult Index(string CorpusName)
        {

            try
            {
                // Since they're 'opening' this corpus, store its name in session
                // to make it the 'active' one
                SetActiveCorpus(CorpusName);

                return View(GetModel(CorpusName));

            }
            catch (Exception ex)
            {
                string message = "An error occurred while trying to open corpus '" + CorpusName + "': " + ex.Message;
                return RedirectToAction("Error", "Home", new  { message = message });
            }


        }

        private static void SetActiveCorpus(string CorpusName)
        {
            //System.Web.HttpContext.Current.Session["prismactivecorpus"] = CorpusName;
            HttpCookie cookie = new HttpCookie("prismactivecorpus");
            cookie.Value = CorpusName;
            cookie.Path = "/";
            cookie.Expires = DateTime.Now.AddDays(7);
            System.Web.HttpContext.Current.Response.Cookies.Remove("prismactivecorpus");
            System.Web.HttpContext.Current.Response.SetCookie(cookie);
        }

        public static string GetActiveCorpus()
        {
            var cookie = System.Web.HttpContext.Current.Request.Cookies["prismactivecorpus"];
            if (cookie == null)
                return string.Empty;

            if (cookie.Value == null)
                return string.Empty;

            return cookie.Value;

            //object o = System.Web.HttpContext.Current.Session["prismactivecorpus"];
            //if (o != null)
            //    return o.ToString();

            //return string.Empty;

        }

        public ActionResult Admin(string CorpusName)
        {

            try
            {
                ViewData["calcstatus"] = BackgroundCalculationMgr.GetCorpusStatus(CorpusName);
                ViewData["corpusbusy"] = BackgroundCalculationMgr.IsCorpusBusy(CorpusName) ? "1" : "0";
                return View(GetModel(CorpusName));

            }
            catch (Exception ex)
            {
                string message = "An error occurred: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }

        }




        public ActionResult CreateVersion(string CorpusName)
        {
            try
            {
                BaseDocumentModel model = new BaseDocumentModel();
                model.CorpusName = CorpusName;
                model.IsVersion = true;
                SetupDocumentDetails(true, true);
                SetupLangCodes();

                return View("DocumentDetails", model);
            }
            catch (Exception ex)
            {
                string message = "An error occurred while trying to open corpus '" + CorpusName + "': " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }

        }

        [HttpPost]
        public ActionResult CreateVersion(BaseDocumentModel model)
        {
            try
            {
                SetupDocumentDetails(true, true);
                if (ModelState.IsValid)
                {

                    if (model.NameIfVersion.Length < 3)
                    {
                        ModelState.AddModelError("versionname", "Please enter a name for the version, at least 3 letters long.");
                        return View("DocumentDetails", model);
                    }

                    ICorpus corpus = PrismHelpers.GetCorpus(model.CorpusName);

                    DocumentMetadata meta = new DocumentMetadata();
                    ModelToDocumentMetadata(model, meta);

                    corpus.CreateVersion(model.NameIfVersion, meta);

                    return RedirectToAction("Index", "Corpus", new { CorpusName = model.CorpusName });

                }


                return View("DocumentDetails", model);
            }
            catch (Exception ex)
            {
                string message = "An error occurred while trying to create the version: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }
        }

        public ActionResult DocumentMetadata(string CorpusName, string NameIfVersion)
        {
            try
            {
                SetupLangCodes();

                BaseDocumentModel model = new BaseDocumentModel();
                model.CorpusName = CorpusName;
                if (!string.IsNullOrEmpty(NameIfVersion))
                {
                    model.IsVersion = true;
                    model.NameIfVersion = NameIfVersion;
                    model.OriginalNameIfVersion = NameIfVersion;
                }

                IDocument doc = PrismHelpers.GetDocument(CorpusName, NameIfVersion);
                DocumentMetadataToModel(doc.GetMetadata(), model);

                SetupDocumentDetails(model.IsVersion, false);

                return View("DocumentDetails", model);


            }
            catch (Exception ex)
            {
                string message = "An error occurred: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }
        }

        [HttpPost]
        public ActionResult DocumentMetadata(BaseDocumentModel model)
        {
            try
            {
                SetupDocumentDetails(model.IsVersion, false);
                SetupLangCodes();

                if (ModelState.IsValid)
                {
                    bool rename = false;
                    if (model.IsVersion)
                    {
                        if (model.NameIfVersion.CompareTo(model.OriginalNameIfVersion) != 0)
                        {
                            if (model.NameIfVersion.Length < 3)
                            {
                                ModelState.AddModelError("versionname", "Please specify a version name of at least 3 characters.");
                                return View("DocumentDetails", model);
                            }
                            rename = true;
                        }
                    }

                    IDocument doc = PrismHelpers.GetDocument(model.CorpusName, model.OriginalNameIfVersion);

                    DocumentMetadata meta = new DocumentMetadata();
                    ModelToDocumentMetadata(model, meta);
                    doc.SetMetadata(meta);

                    if (rename)
                    {
                        ICorpus corpus = PrismHelpers.GetCorpus(model.CorpusName);
                        corpus.RenameVersion(model.OriginalNameIfVersion, model.NameIfVersion);

                    }


                    return RedirectToAction("Index", "Corpus", new { CorpusName = model.CorpusName });

                }


                return View("DocumentDetails", model);
            }
            catch (Exception ex)
            {
                string message = "An error occurred: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }
        }

        public ActionResult DeleteVersion(string CorpusName, string VersionName)
        {

            try
            {

                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);


                corpus.DeleteVersion(VersionName);

                return View("Index", GetModel(CorpusName));


            }
            catch (Exception ex)
            {
                string message = "An error occurred while trying to delete the version: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }
        }

        public ActionResult DeleteAttribute(string CorpusName, int ID)
        {
            try
            {
                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);

                corpus.DeletePredefinedSegmentAttribute(ID);

                return RedirectToAction("PredefinedSegmentAttributes", new { CorpusName = CorpusName });
            }
            catch (Exception ex)
            {
                string message = "An error occurred: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }
        }


        public ActionResult PredefinedSegmentAttributes(string CorpusName)
        {
            try
            {
                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);

                PredefinedSegmentAttributesModel m = new PredefinedSegmentAttributesModel();
                m.CorpusName = CorpusName;

                EblaAPI.PredefinedSegmentAttribute[] names = corpus.GetPredefinedSegmentAttributes();
                List<Prism.Models.PredefinedSegmentAttribute> modelAttribs = new List<Models.PredefinedSegmentAttribute>();
                foreach (var name in names)
                {
                    Prism.Models.PredefinedSegmentAttribute modelAttrib = new Models.PredefinedSegmentAttribute();
                    modelAttrib.Name = name.Name;
                    modelAttrib.ID = name.ID;
                    modelAttrib.AttributeType = name.AttributeType;
                    modelAttrib.ShowInTOC = name.ShowInTOC;
                    List<Models.PredefinedSegmentAttributeValue> vals = new List<Models.PredefinedSegmentAttributeValue>();
                    foreach (var v in name.Values)
                    {
                        Models.PredefinedSegmentAttributeValue val = new Models.PredefinedSegmentAttributeValue();
                        val.Value = v.Value;
                        val.ColourCode = v.ColourCode;
                        val.ApplyColouring = v.ApplyColouring;
                        //val.IsTOCText = v.IsTOCText;
                        //val.ShowInTOC = v.ShowInTOC;
                    }
                    modelAttrib.Values = vals.ToArray();
                    modelAttribs.Add(modelAttrib);
                }


                m.PredefinedSegmentAttributes = modelAttribs.ToArray();

                return View( m);

            }
            catch (Exception ex)
            {
                string message = "An error occurred: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }

        }

        public ActionResult PredefinedSegmentAttribute(string CorpusName, int? ID)
        {
            try
            {
                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);

                PredefinedSegmentAttributeModel m = new PredefinedSegmentAttributeModel();
                m.CorpusName = CorpusName;
                m.PredefinedSegmentAttribute = new Models.PredefinedSegmentAttribute();
                m.PredefinedSegmentAttribute.ID = -1;
                m.PredefinedSegmentAttribute.AttributeType = PredefinedSegmentAttributeType.Text;
                m.PredefinedSegmentAttribute.ShowInTOC = false;
                m.PredefinedSegmentAttribute.Values = new Models.PredefinedSegmentAttributeValue[0];
                if (ID.HasValue)
                {
                    EblaAPI.PredefinedSegmentAttribute n = corpus.GetPredefinedSegmentAttribute(ID.Value);

                    m.PredefinedSegmentAttribute.Name = n.Name;
                    m.PredefinedSegmentAttribute.ID = n.ID;
                    m.PredefinedSegmentAttribute.ShowInTOC = n.ShowInTOC;
                    List<Models.PredefinedSegmentAttributeValue> vals = new List<Models.PredefinedSegmentAttributeValue>();
                    foreach (var v in n.Values)
                    {
                        Models.PredefinedSegmentAttributeValue val = new Models.PredefinedSegmentAttributeValue();
                        val.Value = v.Value;
                        val.ColourCode = v.ColourCode;
                        val.ApplyColouring = v.ApplyColouring;
                        //val.ShowInTOC = v.ShowInTOC;
                        //val.IsTOCText = v.ShowInTOC;
                        vals.Add(val);
                    }
                    m.PredefinedSegmentAttribute.Values = vals.ToArray();
                    m.PredefinedSegmentAttribute.AttributeType = n.AttributeType;
                    
                }
                ViewData["nextvaluerowindex"] = m.PredefinedSegmentAttribute.Values.Length;

                return View(m);

            }
            catch (Exception ex)
            {
                string message = "An error occurred: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }
        }

        [HttpPost]
        public ActionResult PredefinedSegmentAttribute(PredefinedSegmentAttributeModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICorpus corpus = PrismHelpers.GetCorpus(model.CorpusName);

                    EblaAPI.PredefinedSegmentAttribute n = new EblaAPI.PredefinedSegmentAttribute();
                    n.ID = model.PredefinedSegmentAttribute.ID;
                    n.Name = model.PredefinedSegmentAttribute.Name;
                    n.ShowInTOC = model.PredefinedSegmentAttribute.ShowInTOC;
                    List<EblaAPI.PredefinedSegmentAttributeValue> vals = new List<EblaAPI.PredefinedSegmentAttributeValue>();
                    if (model.PredefinedSegmentAttribute.Values != null)
                        foreach (var v in model.PredefinedSegmentAttribute.Values)
                        {
                            EblaAPI.PredefinedSegmentAttributeValue val = new EblaAPI.PredefinedSegmentAttributeValue();
                            val.Value = v.Value;
                            val.ApplyColouring = v.ApplyColouring;
                            //val.ShowInTOC = v.ShowInTOC;
                            //val.IsTOCText = v.IsTOCText;
                            val.ColourCode = v.ColourCode;
                            vals.Add(val);
                        }
                    n.Values = vals.ToArray();
                    n.AttributeType = model.PredefinedSegmentAttribute.AttributeType;

                    if (model.PredefinedSegmentAttribute.ID == -1)
                        corpus.CreatePredefinedSegmentAttribute(n);
                    else
                        corpus.UpdatePredefinedSegmentAttribute(n);


                    return RedirectToAction("PredefinedSegmentAttributes", new { CorpusName = model.CorpusName });
                }

                ViewData["nextvaluerowindex"] = 0;

                if (model.PredefinedSegmentAttribute.Values != null)
                    ViewData["nextvaluerowindex"] = model.PredefinedSegmentAttribute.Values.Length;

                return View(model);
            }
            catch (Exception ex)
            {
                string message = "An error occurred: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }
        }

        [HttpGet]
        public ActionResult CorpusCss(string CorpusName)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            try
            {
                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);

                EblaAPI.PredefinedSegmentAttribute[] atts = corpus.GetPredefinedSegmentAttributes();

                foreach (var a in atts)
                {
                    foreach (var v in a.Values)
                    {
                        if (v.ApplyColouring)
                        {
                            sb.AppendLine("*[data-eblatype='segformat'][data-userattrib-" + a.Name + "='" + v.Value + "']");
                            sb.AppendLine("{");
                            sb.AppendLine("background-color: #" + v.ColourCode);
                            sb.AppendLine("}");

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                // TODO - log
            }

            return Content(sb.ToString(), "text/css");
        }

        //==============================================
        //
        //  AJAX calls
        //
        //==============================================


        [HttpPost]
        public ActionResult BeginVariationCalculation(string CorpusName, int MetricType)
        {
            try
            {
                VariationMetricTypes metricType = (VariationMetricTypes)MetricType;
                BackgroundCalculationMgr.StartCalculation(CorpusName, metricType);

                return MonoWorkaroundJson(new ResultModel { Succeeded = true });

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }

        }

        public ActionResult GetCalcStatus(string CorpusName)
        {
            try
            {

                bool b = BackgroundCalculationMgr.IsCorpusBusy(CorpusName);
                string s = BackgroundCalculationMgr.GetCorpusStatus(CorpusName);

                return MonoWorkaroundJson(new CalculationStatusResultModel { Succeeded = true, Status = s, Busy = b });

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }

        }

        public ActionResult CancelCalc(string CorpusName)
        {
            try
            {

                BackgroundCalculationMgr.Cancel(CorpusName);

                return MonoWorkaroundJson(new ResultModel { Succeeded = true });

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }

        }

        [HttpPost]
        public ActionResult GetNewAttributeRowHtml(string memberName, string CorpusName, int rowindex, string currName)
        {
            try
            {
                return MonoWorkaroundJson(new GetHtmlResultModel { Succeeded = true, html = AttributeRowHtml(memberName, CorpusName, rowindex, currName, null, false) });

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }

        }

        [HttpPost]
        public ActionResult GetNewPredefinedAttributeValueRowHtml(int rowindex)
        {
            try
            {
                return MonoWorkaroundJson(new GetHtmlResultModel { Succeeded = true, html = PredefinedAttributeValueRowHtml(rowindex, null) });

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }

        }


        //==============================================
        //
        //  Implementation
        //
        //==============================================

        void SetupDocumentDetails(bool isVersion, bool creating)
        {
            System.Diagnostics.Debug.Assert(!(creating && !isVersion));
            string verb = creating ? "Create" : "Edit";
            string obj = isVersion ? " version" : " base text";

            ViewData["title"] = verb + obj;
            string subtitle = (verb + obj).ToLower();
            if (!creating)
                subtitle += " details";
            ViewData["subtitle"] = subtitle;

        }

        static internal CorpusModel GetModel(string CorpusName)
        {
            CorpusModel model = new CorpusModel();
            ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);

            model.CorpusName = CorpusName;
            model.Description = corpus.GetDescription();
            model.CanWrite = PrismHelpers.CanWrite(CorpusName);

            DocumentModel baseText = new DocumentModel();
            IDocument baseDoc = PrismHelpers.GetBaseText(CorpusName);
            DocumentMetadata meta = baseDoc.GetMetadata();
            DocumentMetadataToModel(meta, baseText);
            baseText.DocLength = baseDoc.Length();
            model.BaseText = baseText;

            string[] vers = corpus.GetVersionList();

            List<DocumentModel> l = new List<DocumentModel>();
            foreach (string s in vers)
            {
                IDocument d = PrismHelpers.GetDocument(CorpusName, s);
                meta = d.GetMetadata();
                DocumentModel m = new DocumentModel();
                DocumentMetadataToModel(meta, m);
                m.CorpusName = CorpusName;
                m.NameIfVersion = s;
                //meta = d.GetMetadata();
                //m.Description = meta.Description;
                m.DocLength = d.Length();
                l.Add(m);
            }

            l.Sort( (a,b) => (a.SafeDate().CompareTo(b.SafeDate()) )) ;
            model.VersionList = l.ToArray();
            return model;
        }

        private void SetupLangCodes()
        {
            CultureInfo[] cis = CultureInfo.GetCultures(CultureTypes.NeutralCultures);

            List<SelectListItem> items = new List<SelectListItem>();
            SortedDictionary<string, string> cultures = new SortedDictionary<string, string>();
            foreach (var ci in cis)
            {
                if (!cultures.ContainsKey(ci.DisplayName))
                    cultures.Add(ci.DisplayName, ci.ThreeLetterISOLanguageName);
                //items.Add(new SelectListItem() { Text = ci.DisplayName, Value = ci.ThreeLetterISOLanguageName });
            }

            cultures.Add("Latin", "lat" );
            cultures.Add("Sanscrit","sa");
            cultures.Add("Sumerian", "sux" );
            cultures.Add( "Ancient Greek", "grc" );

            foreach (string s in cultures.Keys)
                items.Add(new SelectListItem() { Text = s, Value = cultures[s] });
            //items.Add(new SelectListItem() { Text = "Latin", Value = "lat" });
            //items.Add(new SelectListItem() { Text = "Sanscrit", Value = "sa" });
            //items.Add(new SelectListItem() { Text = "Sumerian", Value = "sux" });
            //items.Add(new SelectListItem() { Text = "Ancient Greek", Value = "grc" });

            ViewData["langcodes"] = items;

        }

        public static string AttributeRowHtml(string memberName, string CorpusName, int rowindex, string currName, string currVal, bool readOnly)
        {
            ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);
            EblaAPI.PredefinedSegmentAttribute[] atts = corpus.GetPredefinedSegmentAttributes();
            EblaAPI.PredefinedSegmentAttribute pre = null;

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string indexval = rowindex.ToString();
            //string rowid = "attribrow-" + indexval;
            string nameselectname = memberName + "[" + indexval + "].Name";
            string valueinputname = memberName + "[" + indexval + "].Value";

            //sb.Append("<tr id='" + rowid + "'>");

            sb.Append("<td>");
            sb.Append("<input type='hidden' name='" + memberName + ".Index' value='" + indexval + "' />");
            sb.Append("<span class='editor-field'>");
            sb.Append(" <select class='attribnameselect' " + (readOnly ? string.Empty /* "disabled='disabled'"*/ : string.Empty) + "  name='" + nameselectname + "'>");

            bool first = true;
            foreach (var a in atts)
            {
                if (first)
                {
                    // If we're being called to add a new, empty row
                    if (currName == null)
                    {
                        // then if the default (first) predefined attrib is a list
                        if (a.AttributeType == PredefinedSegmentAttributeType.List)
                        {
                            // then record it for use in value input fixup
                            pre = a;
                        }
                    }
                    first = false;
                }

                string selected = string.Empty;
                if (currName != null)
                    if (string.Compare(currName, a.Name) == 0)
                    {
                        selected = "selected";
                        if (a.AttributeType == PredefinedSegmentAttributeType.List)
                            pre = a;
                    }
                string temp = System.Net.WebUtility.HtmlEncode(a.Name);
                sb.Append("<option " + selected + " value='" + temp + "'>" + temp + "</option>");

            }

            sb.Append("</select>");
            sb.Append("</span>");
            sb.Append("</td>");

            sb.Append("<td>");
            sb.Append("<span class='editor-field'>");

            if (pre != null)
            {
                sb.Append(" <select " + (readOnly ? string.Empty /* "disabled='disabled'" */ : string.Empty) + "  name='" + valueinputname + "'>");
                foreach (var p in pre.Values)
                {
                    string selected = string.Empty;
                    if (currVal != null)
                        if (string.Compare(currVal, p.Value) == 0)
                            selected = "selected";

                    string temp = System.Net.WebUtility.HtmlEncode(p.Value);
                    sb.Append("<option " + selected + " value='" + temp + "'>" + temp + "</option>");

                }
                sb.Append("</select>");

            }
            else
            {
                sb.Append("<input type='text' " + (readOnly ? string.Empty /* "disabled='disabled'"*/ : string.Empty) + "   class='half' name='" + valueinputname + "' value='" + (currVal == null ? string.Empty : System.Net.WebUtility.HtmlEncode(currVal)) + "' />");
            }

            sb.Append("</span>");
            sb.Append("</td>");

            sb.Append("<td>");
            if (!readOnly)
                sb.Append("<input type='button' onclick='deleteattrib(this)' value='Delete' />");
            sb.Append("</td>");

            //sb.Append("</tr>");

            return sb.ToString();
        }

        private static string Checked(bool b)
        {
            return b ? "checked='checked'" : string.Empty;
        }

        public static string PredefinedAttributeValueRowHtml(int rowindex, Models.PredefinedSegmentAttributeValue v)
        {
            if (v == null)
                v = new Models.PredefinedSegmentAttributeValue();

            System.Text.StringBuilder result = new System.Text.StringBuilder();

            result.Append("<td>");
            result.Append("<input type='hidden' name='PredefinedSegmentAttribute.Values.Index' value='" + rowindex.ToString() + "' />");
            result.Append("<span class='editor-field'> <input type='text' class='half' name='PredefinedSegmentAttribute.Values[" + rowindex.ToString() + "].Value' value='");
            result.Append(System.Net.WebUtility.HtmlEncode(v.Value));
            result.Append("' /></span>");
            result.Append("</td>");

            result.Append("<td>");
            result.Append("<span class='editor-field'>");
            result.Append("<input type='checkbox' class='ApplyColouringCheckbox' id='applycolouringcheckbox-" + rowindex.ToString() + "' ");
            result.Append(Checked(v.ApplyColouring));
            result.Append(" />");
            result.Append("<input type='hidden' id='applycolouringinput-" + rowindex.ToString() + "' name='PredefinedSegmentAttribute.Values[" + rowindex.ToString() + "].ApplyColouring' />");
            result.Append("</span");
            result.Append("</td>");

            result.Append("<td>");
            result.Append("<span class='editor-field'>");
            result.Append("<input class='color' id='colourcode-" + rowindex.ToString() + "'  name='PredefinedSegmentAttribute.Values[" + rowindex.ToString() + "].ColourCode' value='");
            result.Append(System.Net.WebUtility.HtmlEncode(v.ColourCode));
            result.Append("' />");
            result.Append("</span");
            result.Append("</td>");


            result.Append("<td>");
            result.Append("<input type='button' onclick='deletevalue(this)' value='Delete' />");
            result.Append("</td>");

            return result.ToString();
        }



        static internal void DocumentMetadataToModel(DocumentMetadata meta, BaseDocumentModel model)
        {
            model.Genre = meta.Genre;
            model.AuthorTranslator = meta.AuthorTranslator;
            model.Description = meta.Description;
            model.CopyrightInfo = meta.CopyrightInfo;
            model.ReferenceDate = meta.ReferenceDate;
            model.LanguageCode = meta.LanguageCode;
            model.Information = meta.Information;
        }

        static internal void ModelToDocumentMetadata(BaseDocumentModel model, DocumentMetadata meta)
        {
            meta.Genre = model.Genre;
            meta.AuthorTranslator = model.AuthorTranslator;
            meta.Description = model.Description;
            meta.CopyrightInfo = model.CopyrightInfo;
            meta.ReferenceDate = model.ReferenceDate;
            meta.LanguageCode = model.LanguageCode;
            meta.Information = model.Information;
        }

    }

    internal class BackgroundCalculationMgr
    {
        static object _lock = new object();

        public static bool IsCorpusBusy(string CorpusName)
        {
            lock (_lock)
            {
                if (TaskList.ContainsKey(CorpusName))
                {
                    if (TaskList[CorpusName].Status() == System.Threading.ThreadState.Running)
                        return true;

                }
                return false;

            }
                
        }

        public static void Cancel(string CorpusName)
        {
            lock (_lock)
            {
                if (TaskList.ContainsKey(CorpusName))
                {
                    TaskList[CorpusName].RequestStop();

                }

            }

        }

        public static string GetCorpusStatus(string CorpusName)
        {
            lock (_lock)
            {
                if (TaskList.ContainsKey(CorpusName))
                {
                    return TaskList[CorpusName].StatusText();

                }

                return "none.";

            }
        }

        public static void StartCalculation(string CorpusName, VariationMetricTypes metricType)
        {
            lock (_lock)
            {
                if (IsCorpusBusy(CorpusName))
                    throw new Exception("Calculation for the corpus is already in progress.");

                var c = new BackgroundCalculation();

                c.Start(CorpusName, metricType);
                if (TaskList.ContainsKey(CorpusName))
                    TaskList.Remove(CorpusName);
                TaskList.Add(CorpusName, c);
            }

        }

        static private Dictionary<string, BackgroundCalculation> TaskList
        {
            get
            {
                return _taskList;
            }
        }

        // Might be better off with ASP.NET 'application state', but in any event, this is just a cheap MVC
        // implementation of something that ought to be in a separate service
        static private Dictionary<string, BackgroundCalculation> _taskList = new Dictionary<string, BackgroundCalculation>();
    }


    internal class BackgroundCalculation
    {
        System.Threading.Thread _thread;
        ICorpus _corpus;
        string _corpusName;
        object _lock;
        DateTime _timeStarted;
        string _statusText;
        

        public void Start(string CorpusName, VariationMetricTypes metricType)
        {
            _lock = new object();

            _corpus = PrismHelpers.GetCorpus(CorpusName);
            _corpusName = CorpusName;

            _corpus.StartSegmentVariationCalculation(metricType);
            SetStatusText("0% complete.");

            _thread = new System.Threading.Thread(this.DoWork);
            _thread.Priority = System.Threading.ThreadPriority.BelowNormal;
            _timeStarted = DateTime.Now;
            _thread.Start();

        }

        public void DoWork()
        {
            while (true)
            {
                if (_shouldStop)
                {
                    SetStatusText("Cancelled.");
                    break;
                }

                try
                {
                    int progress = 0;
                    bool moreToDo = _corpus.ContinueSegmentVariationCalculation(out progress);
                    if (!moreToDo)
                    {
                        SetStatusText("Completed.");
                        break;

                    }
                    SetStatusText(progress.ToString() + "% complete.");
                }
                catch (Exception ex)
                {
                    SetStatusText("Aborted; error message: " + ex.Message);
                    break;

                }

            }

        }

        private volatile bool _shouldStop;

        public void RequestStop()
        {
            _shouldStop = true;
        }

        public System.Threading.ThreadState Status()
        {
            return _thread.ThreadState;
        }

        public string StatusText()
        {
            lock (_lock)
                return _statusText;
        }

        private void SetStatusText(string s)
        {
            lock (_lock)
                _statusText = s;
        }


    }

}
