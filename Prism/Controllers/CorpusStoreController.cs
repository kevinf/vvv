﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EblaAPI;
using EblaImpl;
using System.Configuration;
using Prism.Models;

namespace Prism.Controllers
{
    [Authorize]
    public class CorpusStoreController : PrismController
    {
  

        //==============================================
        //
        //  Page rendering
        //
        //==============================================

        public ActionResult Index()
        {
            
            
            try
            {

                return View(GetModel());

            }
            catch (Exception ex)
            {
                string message = "An error occurred while trying to open the corpus store: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }
        }

        public ActionResult UsersAdmin()
        {
            try
            {


                UserInfo[] ui = GetCorpusStore().GetUserInfo(null, null);

                return View(ui);

            }
            catch (Exception ex)
            {
                string message = "An error occurred: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }
        }

        public ActionResult UserAdmin(string Username)
        {
            try
            {

                UserInfo[] ui = GetCorpusStore().GetUserInfo(Username, null);

                UserRightsModel u = new UserRightsModel();
                u.CanAdmin = ui[0].IsAdmin;
                u.Username = Username;
                u.Email = ui[0].Email;

                var corporaMap = new Dictionary<string, UserCorpusRightsModel>();
                string[] corpusList = GetCorpusStore().GetCorpusList();

                foreach (string corpus in corpusList)
                    corporaMap.Add(corpus, new UserCorpusRightsModel() {CorpusName=corpus});

                foreach (var r in ui[0].CorpusRights)
                {
                    corporaMap[r.CorpusName].SetRightsValue(r.CanRead, r.CanWrite);
                }

                var rights = new List<UserCorpusRightsModel>();
                foreach (string corpus in corporaMap.Keys)
                    rights.Add(corporaMap[corpus]);

                u.CorpusRights = rights.ToArray();

                return View(u);

            }
            catch (Exception ex)
            {
                string message = "An error occurred: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }

        }

        [HttpPost]
        public ActionResult UserAdmin(UserRightsModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(model);

                GetCorpusStore().UpdateUser(model.Username, model.CanAdmin, model.Email);

                foreach (var r in model.CorpusRights)
                {
                    GetCorpusStore().SetUserCorpusRights(r.CorpusName, model.Username, r.CanRead(), r.CanWrite());
                }
                return RedirectToAction("UsersAdmin", "CorpusStore");


            }
            catch (Exception ex)
            {
                string message = "An error occurred: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }

        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            try
            {
                //GetCorpusStore().ChangePassword(model.NewPassword);
                AccountController.ChangePassword(model.NewPassword);
                return RedirectToAction("ChangePasswordSuccess");
            }
            catch (Exception ex)
            {
                string message = "An error occurred: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }
        }

        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        public ActionResult Delete(string CorpusName)
        {


            try
            {
                GetCorpusStore().DeleteCorpus(CorpusName);


                return View("Index", GetModel());

            }
            catch (Exception ex)
            {
                string message = "An error occurred: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }
        }

        public ActionResult DeleteUser(string Username)
        {


            try
            {
                GetCorpusStore().DeleteUser(Username);
                UserInfo[] ui = GetCorpusStore().GetUserInfo(null, null);

                
                return View("UsersAdmin", ui);

            }
            catch (Exception ex)
            {
                string message = "An error occurred: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }
        }

        public ActionResult CreateUser()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateUser(NewUserModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    GetCorpusStore().CreateUser(model.UserName, model.Password, model.CanAdmin, model.Email);

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);

                    return View(model);
                }

            }
            else
            {
                return View(model);

            }
            return RedirectToAction("UsersAdmin", "CorpusStore");
        }

        public ActionResult Create()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Create(NewCorpusModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    GetCorpusStore().CreateCorpus(model.CorpusName, model.Description);

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);

                    return View(model);
                }

            }
            else
            {
                return View(model);

            }
            return RedirectToAction("Index", "Corpus", new { CorpusName = model.CorpusName });
        }

        //==============================================
        //
        //  AJAX calls
        //
        //==============================================


        //==============================================
        //
        //  Implementation
        //
        //==============================================


        private ICorpusStore _corpusStore = null;



        private ICorpusStore GetCorpusStore()
        {
            // TODO - consider storing in Session

            if (_corpusStore != null)
                return _corpusStore;

            ICorpusStore store = EblaFactory.GetCorpusStore(ConfigurationManager.AppSettings);

            store.Open(AccountController.SessionEblaUsername, AccountController.SessionEblaPassword);

            _corpusStore = store;

            return _corpusStore;
        }

        private CorpusStoreModel GetModel()
        {
            CorpusStoreModel model = new CorpusStoreModel();
            string[] corpusList = GetCorpusStore().GetCorpusList();
            List<CorpusModel> l = new List<CorpusModel>();

            var rights = GetCorpusStore().GetUserInfo(AccountController.SessionEblaUsername, null);


            foreach (string s in corpusList)
            {
                CorpusModel m = new CorpusModel();
                m.CorpusName = s;
                l.Add(m);
                bool openCorpus = false;
                if (AccountController.SessionEblaIsAdmin)
                    openCorpus = true;
                else
                    foreach (var r in rights[0].CorpusRights)
                    {
                        if (string.Compare(r.CorpusName, s) == 0)
                        {
                            // TODO -wrap this logic somewhere else
                            if (r.CanRead || r.CanWrite)
                            {
                                openCorpus = true;

                            }
                            break;
                        }
                    }

                if (openCorpus)
                {
                    ICorpus c = PrismHelpers.GetCorpus(s);
                    m.Description = c.GetDescription();

                }
            }

            model.CorpusList = l.ToArray();
            return model;
        }


    }
}
