﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Prism.Models;
using EblaAPI;
using EblaImpl;
using System.Configuration;

namespace Prism.Controllers
{

    [HandleError]
    public class AccountController : Controller
    {

        public IFormsAuthenticationService FormsService { get; set; }
        //public IMembershipService MembershipService { get; set; }

        private ICorpusStore CorpusStore { get; set; }

        protected override void Initialize(RequestContext requestContext)
        {
            if (FormsService == null) { FormsService = new FormsAuthenticationService(); }
            //if (MembershipService == null) { MembershipService = new AccountMembershipService(); }
            if (CorpusStore == null) { CorpusStore = EblaFactory.GetCorpusStore(ConfigurationManager.AppSettings); }
            base.Initialize(requestContext);
        }

        // **************************************
        // URL: /Account/LogOn
        // **************************************

        public ActionResult LogOn()
        {
            SetupIsPublic();

            var m = new LogOnModel();
            if (GetIsPublic())
            {
                m.UserName = "Guest";
                m.Password = "Guest2012";
            }

            string s = Request.Params.Get("returnUrl");
            if (!string.IsNullOrEmpty(s))
                m.returnUrl = s;

            return View(m);
        }

        private void SetupIsPublic()
        {
            ViewData["ispublic"] = GetIsPublic();

        }

        private bool GetIsPublic()
        {
            object o = ConfigurationManager.AppSettings["IsPublic"];
            if (o != null)
                return bool.Parse(o.ToString());
            return false;
        }


        static internal void ChangePassword(string newPassword)
        {
            var corpusStore = EblaFactory.GetCorpusStore(ConfigurationManager.AppSettings);
            corpusStore.Open(SessionEblaUsername, SessionEblaPassword);
            corpusStore.ChangePassword(newPassword);

            FormsIdentity id = (FormsIdentity)(System.Web.HttpContext.Current.User.Identity);
            FormsAuthenticationTicket ticket = id.Ticket;
            

            string Password = string.Empty;
            bool IsAdmin = false;
            DecodeUserData(ticket.UserData, out Password, out IsAdmin);

            CreateCookie(SessionEblaUsername, newPassword, ticket.IsPersistent, corpusStore);
        }

        static private void CreateCookie(string Username, string Password, bool RememberMe, ICorpusStore corpusStore)
        {
            var ui = corpusStore.GetUserInfo(Username, null);

            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
              Username,
              DateTime.Now,
              DateTime.Now.AddDays(60),
              RememberMe,
              CreateUserData(Password, ui[0].IsAdmin),
              FormsAuthentication.FormsCookiePath);

            // Encrypt the ticket.
            string encTicket = FormsAuthentication.Encrypt(ticket);

            HttpCookie Cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);

            // Set the cookie's expiration time to the tickets expiration time 
            if (ticket.IsPersistent)
                Cookie.Expires = ticket.Expiration;


            // Create the cookie.
            System.Web.HttpContext.Current.Response.Cookies.Add(Cookie);



        }

        public ActionResult Reset()
        {
            ResetPasswordModel model = new ResetPasswordModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Reset(ResetPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    CorpusStore.ResetPassword(model.UserName, model.UserEmail);
                    ViewData["resultmsg"] = "Your password has been reset. Check your email for your new password notification.";
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);

                    return View(model);
                }
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            SetupIsPublic();
            if (ModelState.IsValid)
            {
                
                if (CorpusStore.Open(model.UserName, model.Password))
                {

                    CreateCookie(model.UserName, model.Password, model.RememberMe, CorpusStore);

                    //FormsService.SignIn(model.UserName, model.RememberMe);

                    //// TODO - consider caching
                    //var ui = CorpusStore.GetUserInfo(model.UserName, null);

                    //FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
                    //  model.UserName,
                    //  DateTime.Now,
                    //  DateTime.Now.AddMinutes(600),
                    //  model.RememberMe,
                    //  CreateUserData(model.Password, ui[0].IsAdmin),
                    //  FormsAuthentication.FormsCookiePath);

                    //// Encrypt the ticket.
                    //string encTicket = FormsAuthentication.Encrypt(ticket);

                    //HttpCookie Cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);

                    //// Set the cookie's expiration time to the tickets expiration time 
                    //if (ticket.IsPersistent)
                    //    Cookie.Expires = ticket.Expiration; 


                    //// Create the cookie.
                    //Response.Cookies.Add(Cookie);


                    
                    
                    //SessionEblaUsername = model.UserName;
                    //SessionEblaPassword = model.Password;

                    if (!String.IsNullOrEmpty(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // **************************************
        // URL: /Account/LogOff
        // **************************************

        public ActionResult LogOff()
        {
            FormsService.SignOut();

            return RedirectToAction("Index", "Home");
        }

        // **************************************
        // URL: /Account/Register
        // **************************************

        //public ActionResult Register()
        //{
        //    ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult Register(RegisterModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        // Attempt to register the user
        //        MembershipCreateStatus createStatus = MembershipService.CreateUser(model.UserName, model.Password, model.Email);

        //        if (createStatus == MembershipCreateStatus.Success)
        //        {
        //            FormsService.SignIn(model.UserName, false /* createPersistentCookie */);
        //            return RedirectToAction("Index", "Home");
        //        }
        //        else
        //        {
        //            ModelState.AddModelError("", AccountValidation.ErrorCodeToString(createStatus));
        //        }
        //    }

        //    // If we got this far, something failed, redisplay form
        //    ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
        //    return View(model);
        //}

        //// **************************************
        //// URL: /Account/ChangePassword
        //// **************************************

        //[Authorize]
        //public ActionResult ChangePassword()
        //{
        //    ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
        //    return View();
        //}

        //[Authorize]
        //[HttpPost]
        //public ActionResult ChangePassword(ChangePasswordModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        if (MembershipService.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword))
        //        {
        //            return RedirectToAction("ChangePasswordSuccess");
        //        }
        //        else
        //        {
        //            ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
        //        }
        //    }

        //    // If we got this far, something failed, redisplay form
        //    ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
        //    return View(model);
        //}

        //// **************************************
        //// URL: /Account/ChangePasswordSuccess
        //// **************************************

        //public ActionResult ChangePasswordSuccess()
        //{
        //    return View();
        //}

        internal static string SessionEblaPassword
        {
            get
            {

                // Provisional implementation - password stored in encrypted cookie.

                if (System.Web.HttpContext.Current.User != null)
                    if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                        if (System.Web.HttpContext.Current.User.Identity.AuthenticationType == "Forms")
                        {
                            FormsIdentity id = (FormsIdentity)(System.Web.HttpContext.Current.User.Identity);
                            FormsAuthenticationTicket ticket = id.Ticket;
                            string Password = string.Empty;
                            bool IsAdmin = false;
                            DecodeUserData(ticket.UserData, out Password, out IsAdmin);
                            return Password;
                        }

                return string.Empty;


            }

        }
        public static bool SessionEblaIsAdmin
        {
            get
            {

                // Provisional implementation - password stored in encrypted cookie.

                if (System.Web.HttpContext.Current.User != null)
                    if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                        if (System.Web.HttpContext.Current.User.Identity.AuthenticationType == "Forms")
                        {
                            FormsIdentity id = (FormsIdentity)(System.Web.HttpContext.Current.User.Identity);
                            FormsAuthenticationTicket ticket = id.Ticket;
                            string Password = string.Empty;
                            bool IsAdmin = false;
                            DecodeUserData(ticket.UserData, out Password, out IsAdmin);
                            return IsAdmin;
                        }

                return false;


            }

        }
        public static string SessionEblaUsername
        {
            get
            {


                if (System.Web.HttpContext.Current.User != null)
                    if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                        if (System.Web.HttpContext.Current.User.Identity.AuthenticationType == "Forms")
                        {
                            return System.Web.HttpContext.Current.User.Identity.Name;
                        }

                return string.Empty;
            
            }
            //set
            //{
            //    System.Web.HttpContext.Current.Response.Cookies.Add(new HttpCookie("EblaUsername", value));
            //}
        }

        private static string CreateUserData(string Password, bool IsAdmin)
        {
            return (IsAdmin ? "1" : "0") +  Password;
        }
        private static void DecodeUserData(string UserData, out string Password, out bool IsAdmin)
        {
            Password = UserData.Substring(1);
            IsAdmin = string.Compare(UserData.Substring(0, 1), "1") == 0;

        }
    }
}
