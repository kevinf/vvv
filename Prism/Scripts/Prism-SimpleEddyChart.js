﻿

var PrismNS = PrismNS || {};

PrismNS.SimpleEddyChart = {};

$(function () {

    //$('#search').click(function (e) { PrismNS.CorpusSearch.Search(e); });

    var basetextsegid = $('#basetextsegid').val();
    var corpusname = $('#corpusname').val();

        var url = _siteUrl + "Charts/GetSimpleEddyChartData";
        var args = { CorpusName: corpusname, BaseTextSegmentID: basetextsegid};

        CallControllerAsync(url, args, function (result) {
            if (result != null) {
                if (result.Succeeded) {
                    PrismNS.SimpleEddyChart.ShowChart(result);
                    return;

                }
                alert('Retrieving chart data failed: ' + result.ErrorMsg);

            }

        });

});

    var previousPoint = null;


    PrismNS.SimpleEddyChart.ShowChart = function (chartdata) {

        var points = chartdata.Points;

        if (points.length == 0) {
            alert('No Eddy data was found.');
            return;
        }

        var series = new Array();
        series.push(points);
        var minx = points[0][0] - 10;
        var maxx = points[points.length - 1][0] + 10;

        var miny = points[0][1];
        var maxy = points[0][1];
        for (var i = 1; i < points.length; i++) {
            if (points[i][1] < miny)
                miny = points[i][1];
            if (points[i][1] > maxy)
                maxy = points[i][1];

        }

        var ypadding = (maxy - miny) * 1.0 / 5;
        miny -= ypadding;
        maxy += ypadding;

        var plot = $.plot($('#chart'), series, {
            series: { lines: { show: false }, points: { show: true} },
            xaxis: { min: minx, max: maxx },
            yaxis: { min: miny, max: maxy },
            grid: { hoverable: true }

        });

        //    $.plot($('#chart'), [ [ [1923, 4], [1980, 3]  ] ] );

        var ctx = plot.getCanvas().getContext("2d");
        ctx.textAlign = 'center';

        var plotseries = plot.getData();
        //    for (var i = 0; i < plotseries.length; i++) {
        //        for (var j = 0; j < plotseries[i].data.length; j++) {
        //            var x = plotseries[i].data[j][0];
        //            var y = plotseries[i].data[j][1];

        //            x -= plotseries[i].xaxis.min;
        //            x *= plotseries[i].xaxis.scale;
        //            x += plotseries[i].yaxis.box.width;
        //            //y -= plotseries[i].yaxis.min;
        //            y = plotseries[i].yaxis.max - y;
        //            y *= plotseries[i].yaxis.scale;

        //            ctx.fillText('test' + j, x, y);
        //            //ctx.fillRect(x, y, 5, 5);
        //        }
        //    }


        for (var j = 0; j < points.length; j++) {
            var x = points[j][0];
            var y = points[j][1];

            x -= plotseries[0].xaxis.min;
            x *= plotseries[0].xaxis.scale;
            x += plotseries[0].yaxis.box.width;
            //y -= plotseries[i].yaxis.min;
            y = plotseries[0].yaxis.max - y;
            y *= plotseries[0].yaxis.scale;

            ctx.fillText(chartdata.Versions[j], x, y);
            //ctx.fillRect(x, y, 5, 5);
        }


        $("#chart").bind("plothover", function (event, pos, item) {
            $("#x").text(pos.x.toFixed(2));
            $("#y").text(pos.y.toFixed(2));

            if (true) {
                if (item) {
                    if (previousPoint != item.dataIndex) {
                        previousPoint = item.dataIndex;

                        $("#tooltip").remove();
                        var x = item.datapoint[0].toFixed(2),
                        y = item.datapoint[1].toFixed(2);

                        var text = chartdata.Versions[previousPoint] + " (" + points[previousPoint][0] + ")";

                        showTooltip(item.pageX, item.pageY,
                                text);
                    }
                }
                else {
                    $("#tooltip").remove();
                    previousPoint = null;
                }
            }
        });

    }

function showTooltip(x, y, contents) {
    $('<div id="tooltip">' + contents + '</div>').css({
        position: 'absolute',
        display: 'none',
        top: y + 5,
        left: x + 5,
        border: '1px solid #fdd',
        padding: '2px',
        'background-color': '#fee',
        opacity: 0.80
    }).appendTo("body").fadeIn(200);
}
