﻿
var leftdocelm, rightdocelm;
var jg;

$(function () {
    //alert('1');
    var mycontent = $('#leftdiv').html();
    mycontent = '<div id="leftdivinner" >' + mycontent + '</div>';
    //alert(mycontent);

    leftdocelm = $('#leftiframe')[0].contentDocument.documentElement;

    $('body', leftdocelm).html(mycontent);

    mycontent = $('#rightdiv').html();
    mycontent = '<div id="rightdivinner" >' + mycontent + '</div>';

    rightdocelm = $('#rightiframe')[0].contentDocument.documentElement;

    $('body', rightdocelm).html(mycontent);

    $('#test').click(function (e) { test(); });

    //$('#svgbasics').svg({ settings: { pointerEvents: 'none'} });

    jg = new jsGraphics($('#svgbasics')[0]);

});

function test() {
    var leftspan = $('#plumbleft', leftdocelm);
    var rightspan = $('#plumbright', rightdocelm);

    var leftoffset = $(leftspan).offset();
    var rightoffset = $(rightspan).offset();

    var svgoffset = $('#svgbasics').offset();

    var leftiframeoffset = $('#leftiframe').offset();
    var rightiframeoffset = $('#rightiframe').offset();


    var leftscroll = $(leftdocelm).scrollTop();
    var rightscroll = $(rightdocelm).scrollTop();

//    alert(leftoffset.top);
//    alert($('#leftiframe').offset().top);

//    $('#myline').attr('y1', leftoffset.top);
//    $('#myline').attr('y2', rightoffset.top);

//    var svg = $('#svgbasics').svg('get');

//    svg.line(10, 0, 600, 10,
//			{ stroke: 'blue', 'stroke-width': 2 });

//    svg.line(leftiframeoffset.left - svgoffset.left, leftiframeoffset.top - svgoffset.top, rightiframeoffset.left - svgoffset.left, rightiframeoffset.top - svgoffset.top,
//			{ stroke: 'green', 'stroke-width': 2 });


//    jg.setColor('#ff0000');

//    jg.setStroke(Stroke.DOTTED);

//    jg.drawLine(0, 0, 1500, 1500);

//    jg.setStroke(2);


    //Create jsColor object
    var col = new jsColor("red");

    //Create jsPen object
    var pen = new jsPen(col, 1);


//    svg.line(leftoffset.left + leftiframeoffset.left - svgoffset.left,
//            leftoffset.top + leftiframeoffset.top - svgoffset.top - leftscroll,
//            rightoffset.left + rightiframeoffset.left - svgoffset.left, 
//            rightoffset.top + rightiframeoffset.top - svgoffset.top - rightscroll,
//			{ stroke: 'blue', 'stroke-width': 2 });

//    jg.drawLine(leftoffset.left + leftiframeoffset.left - svgoffset.left,
//                leftoffset.top + leftiframeoffset.top - svgoffset.top - leftscroll,
//                rightoffset.left + rightiframeoffset.left - svgoffset.left, 
//                rightoffset.top + rightiframeoffset.top - svgoffset.top - rightscroll);

//    jg.paint();



    var pt1 = new jsPoint(leftoffset.left + leftiframeoffset.left - svgoffset.left,
                leftoffset.top + leftiframeoffset.top - svgoffset.top - leftscroll);
    var pt2 = new jsPoint(rightoffset.left + rightiframeoffset.left - svgoffset.left,
                rightoffset.top + rightiframeoffset.top - svgoffset.top - rightscroll);
    jg.drawLine(pen, pt1, pt2);

}

