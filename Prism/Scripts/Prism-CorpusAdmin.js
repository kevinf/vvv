﻿/*
Version Variation Visualisation (VVV)
http://www.delightedbeauty.org
    
Copyright (c) Kevin Flanagan, 2012.
http://www.kftrans.co.uk
  
This file is part of VVV.

VVV is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VVV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VVV.  If not, see <http://www.gnu.org/licenses/>.
*/

var PrismNS = PrismNS || {};

PrismNS.CorpusAdmin = {};


$(function () {

    EblaNS.FillVariationTypeSelectElm('#variation-type-select');

    $('#startcalc').click(function (e) { PrismNS.CorpusAdmin.StartCalc (e); });
    $('#stopcalc').click(function (e) { PrismNS.CorpusAdmin.StopCalc(e); });

    $('#test').click(function (e) { PrismNS.CorpusAdmin.Test(e); });

    PrismNS.CorpusAdmin.EnableControls();

    var corpusbusy = $('#corpusbusy').val();
    if (corpusbusy == 1)
        PrismNS.CorpusAdmin.SetTimer();
});


PrismNS.CorpusAdmin.SetTimer = function () {
    setTimeout(function () {
        PrismNS.CorpusAdmin.UpdateStatus();
    }, 5000);

}

PrismNS.CorpusAdmin.UpdateStatus = function () {
    var corpusbusy = $('#corpusbusy').val();


    var url = _siteUrl + 'Corpus/GetCalcStatus';

    var args = { CorpusName: _corpusName };

    CallControllerAsync(url, args, function (result) {
        if (result != null) {
            if (result.Succeeded) {
                if (result.Busy != corpusbusy) {
                    $('#corpusbusy').val(result.Busy ? "1" : "0");
                    PrismNS.CorpusAdmin.EnableControls();
                }
                if (result.Busy)
                    PrismNS.CorpusAdmin.SetTimer();
                $('#calcstatus').text(result.Status);
                return;

            }
            alert('Update status failed: ' + result.ErrorMsg);

        }

    });


}

PrismNS.CorpusAdmin.EnableControls = function () {

    var corpusbusy = $('#corpusbusy').val();

    var startEnabled = (corpusbusy == 0);
    PrismNS.Utils.EnableButton('startcalc', startEnabled);
    PrismNS.Utils.EnableButton('stopcalc', !startEnabled);

}

PrismNS.CorpusAdmin.StartCalc = function (e) {


    var url = _siteUrl + 'Corpus/BeginVariationCalculation';

    var args = { CorpusName: _corpusName, MetricType: $('select#variation-type-select').val() };

    CallControllerAsync(url, args, function (result) {
        if (result != null) {
            if (result.Succeeded) {
                //$('#corpusbusy').val("1");
                PrismNS.CorpusAdmin.UpdateStatus();
                PrismNS.CorpusAdmin.EnableControls();
                PrismNS.CorpusAdmin.SetTimer();
                return;

            }
            alert('Calculation start failed: ' + result.ErrorMsg);

        }

    });

}

PrismNS.CorpusAdmin.Test = function (e) {

    var url = _siteUrl + 'Visualise/GraphData';

    var args = { CorpusName: _corpusName, MetricType: $('select#variation-type-select').val(), BaseTextSegmentID: 38537 };

    CallControllerAsync(url, args, function (result) {
        if (result != null) {
            if (result.Succeeded) {
                var a = JSON.stringify(result.GraphModels);
                alert('aha');
                return;

            }
            alert('Graph data failed: ' + result.ErrorMsg);

        }

    });

}

PrismNS.CorpusAdmin.StopCalc = function (e) {


    var url = _siteUrl + 'Corpus/CancelCalc';

    var args = { CorpusName: _corpusName };

    CallControllerAsync(url, args, function (result) {
        if (result != null) {
            if (result.Succeeded) {
                //$('#corpusbusy').val("1");
                PrismNS.CorpusAdmin.UpdateStatus();
                PrismNS.CorpusAdmin.EnableControls();
                return;

            }
            alert('Calculation cancellation failed: ' + result.ErrorMsg);

        }

    });

}

