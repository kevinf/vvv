﻿/*
Version Variation Visualisation (VVV)
http://www.delightedbeauty.org
    
Copyright (c) Kevin Flanagan, 2012.
http://www.kftrans.co.uk
  
This file is part of VVV.

VVV is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VVV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VVV.  If not, see <http://www.gnu.org/licenses/>.
*/


$(function () {

    SetupEditor(true, 'doceditor', 'editorenclosurediv', 'docextractform', 'GetExtract', 'SaveExtract', 'SegmentDefinitionDialog', function () { SelChanged(); });

    $('#viewbutton').click(function (e) { ViewExtract(false); });
    //$('#edit').click(function (e) { ViewExtract(true); });
    //$('#save').click(function (e) { DoSaveExtract(true); });

    $('#test').click(function (e) { Test(e); });

    $('#concordance').click(function (e) { Concordance(e); });


    $('#addfilterattrib').click(function (e) { AddAttribRow('nextattribfilterrowindex', 'AttributeFilters', 'attribfiltertable', 'attribfilterrow-'); });

    $(window).resize(function () {
        setTimeout(function () {
            SizeEditorToWindow();
        }, 100);
    });


    setTimeout(function () {
        SizeEditorToWindow();
    }, 100);

});


function SizeEditorToWindow() {
//    var editorOffset = $(GetEditorIFrame('editorenclosurediv')).offset();
//    if (!editorOffset)
//        return;

//    var availHeight = $('body').height() - editorOffset.top;
//    var ed = $(GetEditorIFrame('editorenclosurediv'));
//    GetEditorInstanceByName('doceditor').resize('100%', availHeight - 100);

}

function SelChanged() {
    var conc = $('#concordance');
    if (conc.size == 0)
        return;

    var basetextsegid = GetSelectedSegmentID(GetEditorInstanceByName('doceditor'));

    PrismNS.Utils.EnableButton('concordance', basetextsegid > 0);
    PrismNS.Utils.EnableButton('test', basetextsegid > 0);

}

function Concordance() {

    var basetextsegid = GetSelectedSegmentID(GetEditorInstanceByName('doceditor'));


    var data = { CorpusName: _corpusName, baseTextSegmentID: basetextsegid };

    //    var url = _siteUrl + "Document/" + instance._segmentDefinitionAction;
    var url = _siteUrl + "Aligner/SegStats";

    showModal(url, "Test", PrismNS.modalEnum.okCancel, data);

}

function ViewExtract(editing) {

//    if (editing) {
//        var o = GetEditorInstanceByName('doceditor').getCommand('save');

//        return;
//    }

    UpdateExtract('editorenclosurediv', 'docextractform', GetEditorInstanceByName('doceditor'), editing);
}

//function DoSaveExtract() {
//    SaveExtract('editorenclosurediv', 'docextractform', GetEditorInstanceByName('doceditor'));
//    
//}

function Test() {

    var basetextsegid = GetSelectedSegmentID(GetEditorInstanceByName('doceditor'));

    //var data = { CorpusName: _corpusName, baseTextSegmentID: basetextsegid };

    //var url = _siteUrl + "Charts/EddyChart?CorpusName=" + _corpusName + "&baseTextSegmentId=" + basetextsegid + "&chartWidth=1400&chartHeight=600";
    var url = _siteUrl + "Charts/EddyHistory?CorpusName=" + _corpusName + "&BaseTextSegmentId=" + basetextsegid;

    window.location = url;

//    var versionname = GetVersionName('docextractform');

//    var data = { CorpusName: _corpusName, NameIfVersion: versionname, id: 38529 };

//    var instance = GetEditorInstanceByName('doceditor');
//    //    var url = _siteUrl + "Document/" + instance._segmentDefinitionAction;
//    var url = _siteUrl + "Document/" + instance._segmentDefinitionAction + '?' + instance._lastQueryString;

//    showModal(url, "Edit segment", PrismNS.modalEnum.okCancel, data, function () { BindAttribNameSelects(null); }, function () { UpdateExtract(enclosuredivid, formid, instance); });

}

