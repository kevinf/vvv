﻿/*
Version Variation Visualisation (VVV)
http://www.delightedbeauty.org
    
Copyright (c) Kevin Flanagan, 2012.
http://www.kftrans.co.uk
  
This file is part of VVV.

VVV is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VVV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VVV.  If not, see <http://www.gnu.org/licenses/>.
*/



var PrismNS = PrismNS || {};


/////////////////
//
//  PrismNS.Formatting
//
//  Helper functions for apply formatting to content containing segment markup rendered by Ebla.
//
/////////////////

PrismNS.Formatting = {};

// Classes used to format Ebla '[]' segment markers
PrismNS.Formatting._startSegClass = 'startmarker';
PrismNS.Formatting._endSegClass = 'endmarker';

PrismNS.Formatting.ToggleStyleSheet = function (enclosuredivid, href) {
    var doc = GetEditorDocument(enclosuredivid);
    
    var sheet = PrismNS.Formatting.GetStyleSheet(doc, href);
    if (sheet != null) {
        sheet.disabled = !(sheet.disabled);
    }

}


PrismNS.Formatting.ToggleFormatting = function (enclosuredivid, corpusname) {
    PrismNS.Formatting.ToggleStyleSheet(enclosuredivid, '/Corpus/CorpusCss?CorpusName=' + corpusname);

}

PrismNS.Formatting.GetStyleSheet

PrismNS.Formatting.GetStyleSheet = function (doc, stylesheetname, decode) {

    for (var i = 0; i < doc.styleSheets.length; i++) {
        var sheet = doc.styleSheets[i];
        if (!(sheet.href))
            continue;
        var href = sheet.href;
        if (decode)
            href = decodeURI(href);

        if (href.length > stylesheetname.length) {


            var tail = href.substring(href.length - stylesheetname.length);
            if (tail == stylesheetname) {
                return sheet;
            }

        }
    }

    if (!decode)
        return PrismNS.Formatting.GetStyleSheet(doc, stylesheetname, true);

    return null;

}

PrismNS.Formatting.GetRules = function (sheet) {

    if (sheet.cssRules)
        return sheet.cssRules;
    if (sheet.rules)
        return sheet.rules;
    return null;

}

PrismNS.Formatting.ToggleMarkersEx = function (enclosuredivid) {
    PrismNS.Formatting.ToggleStyleSheet(enclosuredivid, 'EditorSegmentMarkers.css');

}


PrismNS.Formatting.RemoveSelectionFormatting = function (enclosureelm, segid) {
    $('[data-eblasegid="' + segid + '"]', enclosureelm).css('text-decoration', 'none');
}

PrismNS.Formatting.AddSelectionFormatting = function (enclosureelm, segid) {
    $('[data-eblasegid="' + segid + '"]', enclosureelm).css('text-decoration', 'underline');

}

