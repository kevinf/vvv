﻿/*
Version Variation Visualisation (VVV)
http://www.delightedbeauty.org
    
Copyright (c) Kevin Flanagan, 2012.
http://www.kftrans.co.uk
  
This file is part of VVV.

VVV is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VVV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VVV.  If not, see <http://www.gnu.org/licenses/>.
*/


$(function () {


    $('#addvalue').click(function (e) { AddValueRow(e); });

    $('#attribtype').change(function () {

        ShowHideList($(this).val());
    });

    var applycolourcheckboxes = $('.ApplyColouringCheckbox');
    for (var i = 0; i < applycolourcheckboxes.length; i++) {

        var checkbox = applycolourcheckboxes[i];
        //        var id = $(checkbox).attr('id');
        //        var index = id.substr(id.indexOf('-') + 1);

        //        var colourinput = $('#colourcode-' + index)[0];

        $(checkbox).change(function () {
            OnColourCheckboxChange(this);
        });

        OnColourCheckboxChange(checkbox);
    }


    ShowHideList($('#attribtype').val());
});

function OnColourCheckboxChange(checkbox) {
    var id = $(checkbox).attr('id');
    var index = id.substr(id.indexOf('-') + 1);

    var colourinput = $('#colourcode-' + index)[0];
    var hiddeninput = $('#applycolouringinput-' + index)[0];

    if ($(checkbox).is(':checked')) {
        $(colourinput).show();
        $(hiddeninput).val('true');
    }
    else {
        $(colourinput).hide();
        $(hiddeninput).val('false');
    }
}

function ShowHideList(val) {
    if (val == "List") {
        $('#attribvallistpanel').css('display', '');

    } else {
        $('#attribvallistpanel').css('display', 'none');

    }
}

function deletevalue(r) {

    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById('valuetable').deleteRow(i);

}

function AddValueRow() {

    var nextrowid = parseInt($('#nextvaluerowindex').val());

    var args = { rowindex: nextrowid };
    var url = _siteUrl + 'Corpus/GetNewPredefinedAttributeValueRowHtml';

    CallControllerAsync(url, args, function (result) {

        if (result.Succeeded) {
        }
        else {
            alert(result.ErrorMsg);
            return;
        }

        var table = document.getElementById('valuetable');
        var rowCount = table.rows.length;
        var row = table.insertRow(rowCount);
        var rowid = 'valuerow-' + nextrowid;
        row.setAttribute('id', rowid);

        $(row).html(result.html);

        var checkbox = $('.ApplyColouringCheckbox', row)[0];

        var colourinput = $('.color', row)[0];

        var temp = new jscolor.color(colourinput);

        $(checkbox).change(function () {
            OnColourCheckboxChange(this);
        });

        OnColourCheckboxChange(checkbox);

        nextrowid++;

        document.getElementById('nextvaluerowindex').value = nextrowid;


    });

//    var result = CallController(url, args);


//    if (result.Succeeded) {
//    }
//    else {
//        alert(result.ErrorMsg);
//        return;
//    }

//    var table = document.getElementById('valuetable');
//    var rowCount = table.rows.length;
//    var row = table.insertRow(rowCount);
//    var rowid = 'valuerow-' + nextrowid;
//    row.setAttribute('id', rowid);

//    $(row).html(result.html);

//    var checkbox = $('.ApplyColouringCheckbox', row)[0];

//    var colourinput = $('.color', row)[0];

//    var temp = new jscolor.color(colourinput);

//    $(checkbox).change(function () {
//        OnColourCheckboxChange(this);
//    });

//    OnColourCheckboxChange(checkbox);

//    nextrowid++;

//    document.getElementById('nextvaluerowindex').value = nextrowid;


//    var cell1 = row.insertCell(0);
//    var element1 = document.createElement("input");
//    element1.type = 'hidden';
//    element1.name = 'PredefinedSegmentAttribute.Values.Index';
//    element1.value = nextrowid;
//    cell1.appendChild(element1);
//    element1 = document.createElement("input");
//    element1.type = "text";
//    element1.setAttribute('class', 'half');
//    element1.name = 'PredefinedSegmentAttribute.Values[' + nextrowid + ']';
//    cell1.appendChild(element1);

//    

//    var cell3 = row.insertCell(1);
//    element1 = document.createElement("input");
//    element1.type = "button";
//    element1.value = "Delete";
//    element1.setAttribute('onclick', 'deletevalue(this);');
//    cell3.appendChild(element1);

//    nextrowid++;

//    document.getElementById('nextvaluerowindex').value = nextrowid;
}
