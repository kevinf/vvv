﻿

var PrismNS = PrismNS || {};

PrismNS.EddyHistory = {};

PrismNS.EddyHistory.ChartData = new Array();

$(function () {

    //$('#search').click(function (e) { PrismNS.CorpusSearch.Search(e); });

    var basetextsegid = $('#basetextsegid').val();
    var corpusname = $('#corpusname').val();

    var url = _siteUrl + "Charts/GetEddyHistoryData";
    var args = { CorpusName: corpusname, BaseTextSegmentID: basetextsegid };

    CallControllerAsync(url, args, function (result) {
        if (result != null) {
            if (result.Succeeded) {

                if (result.Points.length == 0) {
                    alert('No Eddy data was found.');
                    return;
                }

                var series = { label: result.Legend, data: result.Points, _Versions: result.Versions };
                PrismNS.EddyHistory.ChartData.push(series);

                PrismNS.EddyHistory.ShowChart(); //result);
                return;

            }
            alert('Retrieving chart data failed: ' + result.ErrorMsg);

        }

    });

});

var previousPoint = null;


PrismNS.EddyHistory.ShowChart = function (chartdata) {

    //    var points = chartdata.Points;

    //    if (points.length == 0) {
    //        alert('No Eddy data was found.');
    //        return;
    //    }

    //    var series = new Array();
    //    series.push({ label: "test", data: points });

    var firstseriespoints = PrismNS.EddyHistory.ChartData[0].data;

    var minx = firstseriespoints[0][0] - 10;
    var maxx = firstseriespoints[firstseriespoints.length - 1][0] + 10;

    var miny = firstseriespoints[0][1];
    var maxy = firstseriespoints[0][1];

    for (var seriesIndex = 0; seriesIndex < PrismNS.EddyHistory.ChartData.length; seriesIndex++) {
        var thisseriespoints = PrismNS.EddyHistory.ChartData[seriesIndex].data;
        for (var i = 1; i < thisseriespoints.length; i++) {
            if (thisseriespoints[i][1] < miny)
                miny = thisseriespoints[i][1];
            if (thisseriespoints[i][1] > maxy)
                maxy = thisseriespoints[i][1];

        }

    }

    var ypadding = (maxy - miny) * 1.0 / 5;
    miny -= ypadding;
    maxy += ypadding;

    var plot = $.plot($('#chart'), PrismNS.EddyHistory.ChartData, {
        series: { lines: { show: false }, points: { show: true} },
        xaxis: { min: minx, max: maxx },
        yaxis: { min: miny, max: maxy },
        grid: { hoverable: true }

    });


    var ctx = plot.getCanvas().getContext("2d");
    ctx.textAlign = 'center';

    var plotseries = plot.getData();

    for (var seriesIndex = 0; seriesIndex < PrismNS.EddyHistory.ChartData.length; seriesIndex++) {
        var thisseriespoints = PrismNS.EddyHistory.ChartData[seriesIndex].data;

        for (var j = 0; j < thisseriespoints.length; j++) {
            var x = thisseriespoints[j][0];
            var y = thisseriespoints[j][1];

            x -= plotseries[seriesIndex].xaxis.min;
            x *= plotseries[seriesIndex].xaxis.scale;
            x += plotseries[seriesIndex].yaxis.box.width;
            //y -= plotseries[i].yaxis.min;
            y = plotseries[seriesIndex].yaxis.max - y;
            y *= plotseries[seriesIndex].yaxis.scale;

            ctx.fillText(PrismNS.EddyHistory.ChartData[seriesIndex]._Versions[j], x, y);
            //ctx.fillRect(x, y, 5, 5);
        }

    }

//    for (var j = 0; j < points.length; j++) {
//        var x = points[j][0];
//        var y = points[j][1];

//        x -= plotseries[0].xaxis.min;
//        x *= plotseries[0].xaxis.scale;
//        x += plotseries[0].yaxis.box.width;
//        //y -= plotseries[i].yaxis.min;
//        y = plotseries[0].yaxis.max - y;
//        y *= plotseries[0].yaxis.scale;

//        ctx.fillText(chartdata.Versions[j], x, y);
//        //ctx.fillRect(x, y, 5, 5);
//    }


    $("#chart").bind("plothover", function (event, pos, item) {
        $("#x").text(pos.x.toFixed(2));
        $("#y").text(pos.y.toFixed(2));

        if (true) {
            if (item) {
                if (previousPoint != item.dataIndex) {
                    previousPoint = item.dataIndex;

                    $("#tooltip").remove();
                    var x = item.datapoint[0].toFixed(2),
                        y = item.datapoint[1].toFixed(2);

                    var text = PrismNS.EddyHistory.ChartData[item.seriesIndex]._Versions[previousPoint] + " (" + PrismNS.EddyHistory.ChartData[item.seriesIndex].data[previousPoint][0] + ")";

                    showTooltip(item.pageX, item.pageY,
                                text);
                }
            }
            else {
                $("#tooltip").remove();
                previousPoint = null;
            }
        }
    });

}

function showTooltip(x, y, contents) {
    $('<div id="tooltip">' + contents + '</div>').css({
        position: 'absolute',
        display: 'none',
        top: y + 5,
        left: x + 5,
        border: '1px solid #fdd',
        padding: '2px',
        'background-color': '#fee',
        opacity: 0.80
    }).appendTo("body").fadeIn(200);
}
