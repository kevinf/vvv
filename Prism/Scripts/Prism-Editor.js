﻿/*
Version Variation Visualisation (VVV)
http://www.delightedbeauty.org
    
Copyright (c) Kevin Flanagan, 2012.
http://www.kftrans.co.uk
  
This file is part of VVV.

VVV is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VVV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VVV.  If not, see <http://www.gnu.org/licenses/>.
*/

var PrismNS = PrismNS || {};

/////////////////
//
//  PrismNS.Editor
//
//  Client code for using segmentation-aware editor
//
/////////////////

PrismNS.Editor = {};

// Classes used to format temporary markers inserted by the UI
var _tempStartSegClass = 'tempstartmarker';
var _tempEndSegClass = 'tempendmarker';

//var _keytest = new Array();

//=====================================
//
// Setup
//
//=====================================

function SetupEditor(canedit, textareaid, enclosuredivid, formid, updateaction, saveaction, segmentdefinitionaction, onselchange, ondataready, onresize, onscroll) {
    var oldInstance = GetEditorInstanceByName(textareaid);

    if (oldInstance)
        oldInstance.destroy(true);

    var instance = CKEDITOR.replace(textareaid,
    {
        //toolbar_Bare: [{ name: 'baretb', items: ['Source', '-', 'ToggleMarkers', '-', 'ToggleColour', '-', 'CreateSegment', '-', 'EditSegment', '-', 'DeleteSegment']}],
        //toolbar: 'Bare' ,
        extraPlugins: 'stylesheetparser',
        contentsCss: [_siteUrl + 'Content/TaggingStyles.css', _siteUrl + 'Content/EditorSegmentMarkers.css', _siteUrl + 'Corpus/CorpusCss?CorpusName=' + PrismNS.Utils.GetCorpusName()],
        on: {
            selectionChange: function (ev) {
                SelectionChanged(ev, textareaid, enclosuredivid);
                if (onselchange)
                    onselchange();
            },
            resize: function (ev) {
                if (onresize)
                    onresize();
            }
        }
    });

    //instance._scrollTopRestoreVal = 0;
    //instance._scrollChrome = false;
    instance._prismScrollInfo = null;
    instance._prismEditing = false;
    instance._prismCanEdit = canedit;
    instance._updateExtractAction = updateaction;
    instance._saveExtractAction = saveaction;
    instance._segmentDefinitionAction = segmentdefinitionaction;

    instance.on('pluginsLoaded', function (ev) {
        SetupEditorButtons(instance, enclosuredivid, formid);

    });


    instance.on('dataReady', function (ev) {
        if (instance.mode == 'wysiwyg') {

            if (instance._prismScrollInfo) {
                if (instance._prismScrollInfo.scrollChrome)
                    $(GetEditorDocBody(enclosuredivid)).scrollTop(instance._prismScrollInfo.scroll);
                else
                    $(GetEditorDocElm(enclosuredivid)).scrollTop(instance._prismScrollInfo.scroll);
            }
            //AddSelectionHandler(enclosuredivid, instance);
            $(GetEditorDocElm(enclosuredivid)).bind('contextmenu', function (e) {
                setTimeout(function () {
                    ShowSelectedSegment(enclosuredivid, instance);
                }, 1);
                return false;
            });

            if (onscroll) {
                var frm = GetEditorIFrame(enclosuredivid).contentWindow;
                frm.onscroll = function () {
                    onscroll();
                };

            }

            if (ondataready)
                ondataready();

            PrismNS.Editor.EnableControls(instance);
        }
        else
            instance._prismScrollInfo = null;

//        if (!instance._prismEditing)
//            instance.getCommand('save').disable();
//        else {
//            instance.getCommand('save').enable();
//            instance.getCommand('save').exec = function (instance) { alert('save'); }
//        }

    });


    instance.on('key', function (keyEvent) {
        var mykeycode = keyEvent.data.keyCode;
        if (mykeycode & 2228224)
            mykeycode -= 2228224; // shift key

        if (instance._prismEditing)
            return;

        switch (mykeycode) {
            case 33: // pageup
            case 34: // pagedown
            case 35: // end
            case 36: // home
            case 37: // leftarrow
            case 38: // uparrow
            case 39: // rightarrow
            case 40: // downarrow
                //                setTimeout(function () {
                //                    // sel change event alone is not reliable enough
                //                    //ShowSelectedSegment(enclosuredivid, instance);
                //                }, 1);

                return;
        }
        //        _keytest.push(keyEvent.data.keyCode);
        keyEvent.cancel();
    });


    instance.on('paste', function (keyEvent) {
        keyEvent.cancel();
    });


}


function SetupEditorButtons(instance, enclosuredivid, formid) {

    instance.getCommand('save').exec = function () { PrismNS.Editor.Save(enclosuredivid, formid, instance); }

		instance.addCommand('edit',
        {
            exec: function () {
                PrismNS.Editor.BeginEditing(enclosuredivid, formid, instance);                
            },
            canUndo: false

        });

        instance.ui.addButton('Edit',
		{
		    label: 'Edit document',
		    command: 'edit',
		    icon: _siteUrl + 'Images/editor/edit.png'
		});

		instance.addCommand('canceledit',
        {
            exec: function () {
                PrismNS.Editor.CancelEditing(enclosuredivid, formid, instance);                
            },
            canUndo: false

        });

		instance.ui.addButton('Cancel',
		{
		    label: 'Cancel edit',
		    command: 'canceledit',
		    icon: _siteUrl + 'Images/editor/canceledit.png'
		});

        instance.addCommand('toggleMarkers',
        {
            exec: function () {
                ToggleMarkers(enclosuredivid);
            },
            canUndo: false

        });

        instance.ui.addButton('ToggleMarkers',
		{
		    label: 'Toggle segment symbols',
		    command: 'toggleMarkers',
		    icon: _siteUrl + 'Images/editor/togglemarkers.png'
		});

		instance.addCommand('toggleColour',
        {
            exec: function () {
                ToggleFormatting(enclosuredivid);
            },
            canUndo: false

        });

        instance.ui.addButton('ToggleColour',
		{
		    label: 'Toggle segment colouring',
		    command: 'toggleColour',
            icon: _siteUrl + 'Images/editor/togglecolouring.png'
		});


		instance.addCommand('editSegment',
        {
            exec: function () {
                EditSegment(enclosuredivid, formid, instance);
            },
            canUndo: false

        });

        instance.ui.addButton('EditSegment',
		{
		    label: 'Edit segment information',
		    command: 'editSegment',
		    icon: _siteUrl + 'Images/editor/editseg.png'
		});

		instance.addCommand('createSegment',
        {
            exec: function () {
                ShowCreateSegmentDialog(enclosuredivid, formid, instance);
            },
            canUndo: false

        });

        instance.ui.addButton('CreateSegment',
		{
		    label: 'Create segment (around selected text)',
		    command: 'createSegment',
		    icon: _siteUrl + 'Images/editor/newsegsel.png'
		});

		instance.addCommand('createSegmentFromMarkers',
        {
            exec: function () {
                ShowCreateSegmentDialog(enclosuredivid, formid, instance, true);
            },
            canUndo: false

        });

        instance.ui.addButton('CreateSegmentFromMarkers',
		{
		    label: 'Create segment (between { and } markers)',
		    command: 'createSegmentFromMarkers',
		    icon: _siteUrl + 'Images/editor/newsegmark.png'
		});


		instance.addCommand('deleteSegment',
        {
            exec: function () {
                DeleteSingleSegment(enclosuredivid, formid, instance);
            },
            canUndo: false

        });

        instance.ui.addButton('DeleteSegment',
		{
		    label: 'Delete the selected segment',
		    command: 'deleteSegment',
		    icon: _siteUrl + 'Images/editor/deleteseg.png'
		});

		instance.addCommand('deleteAllSegments',
        {
            exec: function () {
                DeleteSegments(enclosuredivid, formid, instance);
            },
            canUndo: false

        });

        instance.ui.addButton('DeleteAllSegments',
		{
		    label: 'Delete all segments',
		    command: 'deleteAllSegments',
		    icon: _siteUrl + 'Images/editor/deleteallsegs.png'
		});

		instance.addCommand('insertStartMarker',
        {
            exec: function () {
                InsertStartMarker(enclosuredivid, instance);
            },
            canUndo: false

        });

        instance.ui.addButton('InsertStartMarker',
		{
		    label: 'Insert start marker',
		    command: 'insertStartMarker', 
            icon: _siteUrl + 'Images/editor/startmarker.png'
		});

		instance.addCommand('insertEndMarker',
        {
            exec: function () {
                InsertEndMarker(enclosuredivid, instance);
            },
            canUndo: false

        });

        instance.ui.addButton('InsertEndMarker',
		{
		    label: 'Insert end marker',
		    command: 'insertEndMarker',
		    icon: _siteUrl + 'Images/editor/endmarker.png'
		});

		instance.addCommand('moveToStartMarker',
        {
            exec: function () {
                MoveToStartMarker(enclosuredivid, formid, instance);
            },
            canUndo: false

        });

        instance.ui.addButton('MoveToStartMarker',
		{
		    label: 'Make the selected segment begin at the start marker position',
		    command: 'moveToStartMarker',
		    icon: _siteUrl + 'Images/editor/movestartmarker.png'
		});


		instance.addCommand('moveToEndMarker',
        {
            exec: function () {
                MoveToEndMarker(enclosuredivid, formid, instance);
            },
            canUndo: false

        });

        instance.ui.addButton('MoveToEndMarker',
		{
		    label: 'Make the selected segment end at the end marker position',
		    command: 'moveToEndMarker',
		    icon: _siteUrl + 'Images/editor/moveendmarker.png'
		});

}

//function AddSelectionHandler(enclosuredivid, instance) {

//    $(GetEditorDocBody(enclosuredivid)).click(function () {

//        //ShowSelectedSegment(enclosuredivid, instance);
//        
//    });

////    $(GetEditorDocBody(enclosuredivid)).keypress(function () {

////        ShowSelectedSegment(enclosuredivid, instance);

////    });

//}


function SetEditorScroll(scrollInfo, enclosuredivid) {
    if (scrollInfo.scrollChrome)
        $(GetEditorDocBody(enclosuredivid)).scrollTop(scrollInfo.scroll);
    else
        $(GetEditorDocElm(enclosuredivid)).scrollTop(scrollInfo.scroll);

}

//=====================================
//
// Selection-related
//
//=====================================

function SelectionChanged(ev, textareaid, enclosuredivid) {
    ShowSelectedSegment(enclosuredivid, GetEditorInstanceByName(textareaid));
}

function GetSelectedSegmentID(instance) {
    return instance._selectedSegmentID;
}

function ShowSelectedSegment(enclosuredivid, instance) {
    var selrange = GetSelectionRange(enclosuredivid, true);
    if (selrange == null && instance._selectedSegmentID != null) {
        RemoveInstanceSelectionFormatting(enclosuredivid, instance);
        instance._selectedSegmentID = null;
        return;
    }

    var segid = GetSegmentIDForNode(selrange.startContainer);
    if (segid == -1 && instance._selectedSegmentID != null) {
        RemoveInstanceSelectionFormatting(enclosuredivid, instance);
        instance._selectedSegmentID = null;
        return;
    }

    if (segid == instance._selectedSegmentID)
        return;

    RemoveInstanceSelectionFormatting(enclosuredivid, instance);
    AddInstanceSelectionFormatting(enclosuredivid, instance, segid);

}

function RemoveInstanceSelectionFormatting(enclosuredivid, instance) {
    if (instance._selectedSegmentID)
        RemoveSelectionFormatting(enclosuredivid, instance._selectedSegmentID);

}

function RemoveSelectionFormatting(enclosuredivid, segid) {
    var doc = GetEditorDocument(enclosuredivid);

    var startMarkerElm = doc.getElementById(StartMarkerElementId(segid));
    var endMarkerElm = doc.getElementById(EndMarkerElementId(segid));
    $(startMarkerElm).css('color', '');
    $(startMarkerElm).css('background-color', '');
    $(endMarkerElm).css('color', '');
    $(endMarkerElm).css('background-color', '');

    $('[data-eblasegid="' + segid + '"]', GetEditorDocElm(enclosuredivid)).css('text-decoration', 'none');
}

function AddInstanceSelectionFormatting(enclosuredivid, instance, segid) {
    var doc = GetEditorDocument(enclosuredivid);

    var startMarkerElm = doc.getElementById(StartMarkerElementId(segid));
    var endMarkerElm = doc.getElementById(EndMarkerElementId(segid));
    $(startMarkerElm).css('color', 'White');
    $(startMarkerElm).css('background-color', 'Black');
    $(endMarkerElm).css('color', 'White');
    $(endMarkerElm).css('background-color', 'Black');


    $('[data-eblasegid="' + segid + '"]', GetEditorDocElm(enclosuredivid)).css('text-decoration', 'underline');
    instance._selectedSegmentID = segid;
}

//=====================================
//
// Element/object access
//
//=====================================


function GetEditorInstanceByName(name) {

    for (var i in CKEDITOR.instances) {
        if (CKEDITOR.instances[i].name == name)
            return CKEDITOR.instances[i];
    }
    return null;
}


function GetEditorEnclosureDiv(enclosuredivid) {
    return $('#' + enclosuredivid)[0];
}

function GetEditorIFrame(enclosuredivid) {
    var iframe = $("iframe", GetEditorEnclosureDiv(enclosuredivid));
    return iframe[0];

}

function GetEditorDocument(enclosuredivid) {
    return GetEditorIFrame(enclosuredivid).contentDocument;
}

function GetEditorDocElm(enclosuredivid) {
    return GetEditorIFrame(enclosuredivid).contentDocument.documentElement;
}

function GetEditorDocBody(enclosuredivid) {
    return $("body", GetEditorDocElm(enclosuredivid))[0];
}

//=====================================
//
// UI management
//
//=====================================

PrismNS.Editor.EnableControls = function (instance) {

    var editing = instance._prismEditing;
    if (!editing)
        editing = false;

    PrismNS.Editor.EnableButton(instance, 'canceledit', editing);
    PrismNS.Editor.EnableButton(instance, 'save', editing);

    PrismNS.Editor.EnableButton(instance, 'source', !editing); // don't allow source view while editing
    PrismNS.Editor.EnableButton(instance, 'edit', !editing && instance._prismCanEdit && !!(instance._lastQueryString)); // only allow edit if stuff has been retrieved

    PrismNS.Editor.EnableButton(instance, 'toggleMarkers', !editing);
    PrismNS.Editor.EnableButton(instance, 'toggleColour', !editing);
    PrismNS.Editor.EnableButton(instance, 'editSegment', !editing);
    PrismNS.Editor.EnableButton(instance, 'createSegment', !editing);
    PrismNS.Editor.EnableButton(instance, 'createSegmentFromMarkers', !editing);
    PrismNS.Editor.EnableButton(instance, 'deleteSegment', !editing);
    PrismNS.Editor.EnableButton(instance, 'deleteAllSegments', !editing);
    PrismNS.Editor.EnableButton(instance, 'insertStartMarker', !editing);
    PrismNS.Editor.EnableButton(instance, 'insertEndMarker', !editing);
    PrismNS.Editor.EnableButton(instance, 'moveToStartMarker', !editing);
    PrismNS.Editor.EnableButton(instance, 'moveToEndMarker', !editing);


}

PrismNS.Editor.EnableButton = function (instance, buttonid, enabled) {
    var o = instance.getCommand(buttonid);
    if (!o) {
        alert('Unable to retrieve toolbar button with id: ' + buttonid);
        return;
    }
    if (enabled)
        o.enable();
    else
        o.disable();
}

//=====================================
//
// Editing
//
//=====================================

PrismNS.Editor.BeginEditing = function (enclosuredivid, formid, instance) {
    UpdateExtract(enclosuredivid, formid, instance, true);
}

PrismNS.Editor.CancelEditing = function (enclosuredivid, formid, instance) {
    UpdateExtract(enclosuredivid, formid, instance, false);
}

PrismNS.Editor.Save = function (enclosuredivid, formid, instance) {
    if (!instance._prismEditing) {
        alert('Document is not being edited');
        return;
    }

    var queryString = $('#' + formid).formSerialize();

    instance._prismScrollInfo = GetEditorScrollInfo(enclosuredivid);

    var url = _siteUrl + 'Document/' + instance._saveExtractAction + '?' + queryString;

    var args = { newHtml: instance.getData() };

    CallControllerAsync(url, args, function (result) {
        if (result != null) {
            if (result.Succeeded) {
                instance._prismEditing = false;
                UpdateExtract(enclosuredivid, formid, instance, false);
                PrismNS.Editor.EnableControls(instance);
                return;

            }
            alert('Save failed: ' + result.ErrorMsg);
        }
    });


}



//=====================================
//
// Formatting
//
//=====================================

function StartMarkerElementId(segid) {
    return 'ebla-segstart-' + segid;
}

function EndMarkerElementId(segid) {
    return 'ebla-segend-' + segid;
}


function ToggleMarkers(enclosuredivid) {

    PrismNS.Formatting.ToggleMarkersEx(enclosuredivid);

}

function ToggleFormatting(enclosuredivid) {

    PrismNS.Formatting.ToggleFormatting(enclosuredivid, PrismNS.Utils.GetCorpusName());

}

function GetEditorScrollInfo(enclosuredivid) {
    var scroll = $(GetEditorDocElm(enclosuredivid)).scrollTop();
    // Chrome workaround
    var scroll2 = $(GetEditorDocBody(enclosuredivid)).scrollTop();

    var result = new Object();
    result.scrollChrome = false;
    result.scroll = scroll;
    if (scroll2 > scroll) {
        result.scrollChrome = true;
        result.scroll = scroll2;
    }

    return result;
}

function UpdateExtract(enclosuredivid, formid, instance, editing) {

    var queryString = $('#' + formid).formSerialize();

    instance._prismScrollInfo = GetEditorScrollInfo(enclosuredivid);

    if (editing) {
        queryString += '&Editing=true';
        //instance.getCommand('source').disable();
    }

    var url = _siteUrl + 'Document/' + instance._updateExtractAction + '?' + queryString;

    CallControllerAsync(url, null, function (result) {
        if (result != null) {
            if (result.Succeeded) {
                instance._selectedSegmentID = null;
                instance.setData(result.html);
                instance._lastQueryString = queryString;
                instance._prismEditing = editing;
                PrismNS.Editor.EnableControls(instance);
                return;

            }
            alert('Extract refresh failed: ' + result.ErrorMsg);
        }
    });


}


function CheckWysiwyg(instance) {
    if (instance.mode == 'wysiwyg')
        return true;
    alert('The editor must be in WYSIWYG mode to use this function.');
    return false;
}

function EditSegment(enclosuredivid, formid, instance) {
    if (!CheckWysiwyg(instance))
        return;

    // Anything showing yet?
    if (!(instance._lastQueryString))
        return;

    var selrange = GetSelectionRange(enclosuredivid);
    if (selrange == null)
        return;

    var versionname = GetVersionName(formid);

    var segid = GetSegmentIDForNode(selrange.startContainer);
    if (segid == -1) {
        alert('To edit a segment definition, position the cursor within an existing segment.');
        return;
    }

    var data = { CorpusName: _corpusName, NameIfVersion: versionname, id: segid };

//    var url = _siteUrl + "Document/" + instance._segmentDefinitionAction;
    var url = _siteUrl + "Document/" + instance._segmentDefinitionAction + '?' + instance._lastQueryString;

    showModal(url, "Edit segment", PrismNS.modalEnum.okCancel, data, function () { BindAttribNameSelects(null); }, function () { UpdateExtract(enclosuredivid, formid, instance); });

}

function GetVersionName(formid) {
    var form = document.getElementById(formid);
    var version = $('#versionname', form);
    var versionname = '';
    if (version.length > 0)
        versionname = version.val();

    return versionname;
}

function ShowCreateSegmentDialog(enclosuredivid, formid, instance, useMarkers) {
    if (!CheckWysiwyg(instance))
        return;

    // Anything showing yet?
    if (!(instance._lastQueryString))
        return;

    var startOffset = 0;
    var endOffset = 0;
    var length = 0;

    if (useMarkers) {
        var startMarker = GetMarker(enclosuredivid, true);
        var endMarker = GetMarker(enclosuredivid, false);

        if (!startMarker || !endMarker) {
            alert("Please position both start and end markers - '{' and '}' - in the text before using this function.");
            return;
        }

        
        startOffset = CountToContainer( GetEditorDocBody(enclosuredivid) , startMarker, new Object());
        endOffset = CountToContainer(GetEditorDocBody(enclosuredivid), endMarker, new Object());

    }
    else {
        var selrange = GetSelectionRange(enclosuredivid);
        if (selrange == null)
            return;

        startOffset = GetStartOffset(enclosuredivid, selrange);
        endOffset = GetEndOffset(enclosuredivid, selrange); 
       
    }

    length = endOffset - startOffset;
    if (length == 0)
        if (!confirm('Are you sure you want to create a zero-length segment?'))
            return;

    var versionname = GetVersionName(formid);

    //var queryString = $('#' + formid).formSerialize();

    var url = _siteUrl + "Document/" + instance._segmentDefinitionAction + '?' + instance._lastQueryString;

    var data = { CorpusName: _corpusName, NameIfVersion: versionname, startOffset: startOffset, length: length };

    showModal(url, "Create segment", PrismNS.modalEnum.okCancel, data, function () { BindAttribNameSelects(null) }, function () { UpdateExtract(enclosuredivid, formid, instance); });
    
    
}



function InsertEndMarker(enclosuredivid, instance) {
    InsertMarker(enclosuredivid, instance, false);
}

function InsertStartMarker(enclosuredivid, instance) {
    InsertMarker(enclosuredivid, instance, true);

}

function GetMarkerId(enclosuredivid, start) {
    if (start)
        return enclosuredivid + '-startmarker';
    return enclosuredivid + '-endmarker';
}

function GetMarkerClass(start) {
    if (start)
        return _tempStartSegClass;

    return _tempEndSegClass;
}
function GetMarker(enclosuredivid, start) {
    var id = GetMarkerId(enclosuredivid, start);

    var current = $('#' + id, GetEditorDocElm(enclosuredivid));

    if (current.length > 0)
        return current[0];

    return null;
}

function RemoveMarker(markerspan) {
    markerspan.parentNode.removeChild(markerspan);

}

function InsertMarker(enclosuredivid, instance, start) {
    if (!CheckWysiwyg(instance))
        return;

    var selrange = GetSelectionRange(enclosuredivid);
    if (selrange == null)
        return;

    var startOffset = GetStartOffset(enclosuredivid, selrange);
    var endOffset = GetEndOffset(enclosuredivid, selrange); 

    var length = endOffset - startOffset;

    if (length > 0) {
        alert('Please position the cursor where you want the start marker inserted, and ensure no text is selected.');
        return;
    }


    var id = GetMarkerId(enclosuredivid, start);
    var spanclass = GetMarkerClass(start);
    var markerchar = '}';
    if (start)
        markerchar = '{';

    var current = GetMarker(enclosuredivid, start); //  $('#' + id, _editordocelm);

    if (current)
        RemoveMarker(current);

    // This is all we want to insert. But because of the Chrome issue described at http://dev.ckeditor.com/ticket/9004, we have
    // to go around the houses.
    //var toInsert = '<span class="' + spanclass + '" id="' + id + '">' + markerchar + '</span>';
    // Insert something temporary that Chrome won't screw up
    var toInsert = '<strong id="' + id + '">' + markerchar + '</strong>';

    instance.insertHtml(toInsert);

    // Now fixup the temp Chrome insertion
    setTimeout(function () {
        var strongelm = $('#' + id, GetEditorDocElm(enclosuredivid))[0];
        // Need to remove strongelm and insert a span in its place
        var newspan = GetEditorDocument(enclosuredivid).createElement('span');
        var txt = GetEditorDocument(enclosuredivid).createTextNode(markerchar);
        newspan.appendChild(txt);
        newspan.setAttribute('class', spanclass);
        newspan.setAttribute('id', id);
        strongelm.parentNode.replaceChild(newspan, strongelm);

    }, 1);


    if (start) {
        var endmarker = GetMarker(enclosuredivid, false);
        if (endmarker) {
            var endmarkerstart = CountToContainer(GetEditorDocBody(enclosuredivid), endmarker, new Object());
            if (endmarkerstart < startOffset)
                RemoveMarker(endmarker);
        }

    }
    else {
        var startmarker = GetMarker(enclosuredivid, true);
        if (startmarker) {
            var startmarkerstart = CountToContainer(GetEditorDocBody(enclosuredivid), startmarker, new Object());
            if (startmarkerstart > startOffset)
                RemoveMarker(startmarker);

        }
    }
}


function GetSegmentIDForNode(node){

    return PrismNS.Utils.GetSegmentIDForNode(node);

}

function DeleteSingleSegment(enclosuredivid, formid, instance) {
    if (!CheckWysiwyg(instance))
        return;

    var selrange = GetSelectionRange(enclosuredivid);
    if (selrange == null)
        return;

    var segid = GetSegmentIDForNode(selrange.startContainer);
    if (segid == -1) {
        alert('To delete a segment definition, position the cursor within an existing segment.');
        return;
    }

    if (!confirm('Are you sure you want to delete the selected segment definition?'))
        return;

    var args = { CorpusName: _corpusName, NameIfVersion: GetVersionName(formid), id: segid };
    var url = _siteUrl + 'Document/DeleteSeg';

    CallControllerAsync(url, args, function (result) {
        if (result != null) {
            if (result.Succeeded) {
                //alert('Segment deleted.');
                UpdateExtract(enclosuredivid, formid, instance);
                return;

            }
            alert('Segment deletion failed: ' + result.ErrorMsg);

        }

    });


}

function DeleteSegments(enclosuredivid, formid, instance) {

    if (!confirm('Are you sure you want to delete all segment definitions for this document?'))
        return;

    var args = { CorpusName: _corpusName, NameIfVersion: GetVersionName(formid) };
    var url = _siteUrl + 'Document/DeleteSegs';

    CallControllerAsync(url, args, function (result) {
        if (result != null) {
            if (result.Succeeded) {
                //alert('Segments deleted.');
                UpdateExtract(enclosuredivid, formid, instance);
                return;

            }
            alert('Segment deletion failed: ' + result.ErrorMsg);

        }

    });

}


//=====================================
//
// Selection measurement code
//
//=====================================

function GetSelectionRange(enclosuredivid, suppressmsg) {
    var sel = rangy.getIframeSelection(GetEditorIFrame(enclosuredivid) );

    if (sel) {
        if (sel._ranges) {
            if (sel._ranges.length > 0) {

                if (sel._ranges.length > 1) {
                    alert('There are too many selections.');
                    return null;
                }
                return sel._ranges[0];
            }

        }
    }

    if (suppressmsg)
        return null;
    alert('No selection is available.');
    return null;

}

function GetStartOffset(enclosuredivid, selrange) {
    return CountToContainer(GetEditorDocBody(enclosuredivid), selrange.startContainer, new Object()) + selrange.startOffset;

}
function GetEndOffset(enclosuredivid, selrange) {
    return CountToContainer(GetEditorDocBody(enclosuredivid), selrange.endContainer, new Object()) + selrange.endOffset;
}

function CountToContainer(element, container, status) {

    var count = 0;

    if (element === container) {
        status.found = true;
        return 0;
    }

    if (element.nodeType == 3) {
        // text

        if (element.textContent) {
            // if it's a segment marker, don't count it
            var segmark = false;
            var parent = $(element.parentNode);
            var parentType = parent.attr("data-eblatype");
            var parentClass = parent.attr("class");
            if (parentType == PrismNS.Formatting._startSegClass || parentType == PrismNS.Formatting._endSegClass || parentClass == _tempStartSegClass || parentClass == _tempEndSegClass)
                segmark = true;

            if (!segmark)
                count += element.textContent.length;
        }
    }

    var ch = element.childNodes;

    for (var i = 0; i < ch.length; i++) {
        var temp = CountToContainer(ch[i], container, status);
        count += temp;
        if (status.found)
            break;
    }

    return count;
}

function MoveToStartMarker(enclosuredivid, formid, instance) {
    MoveToMarker(enclosuredivid, formid, instance, true);
}

function MoveToEndMarker(enclosuredivid, formid, instance) {
    MoveToMarker(enclosuredivid, formid, instance, false);
}

function MoveToMarker(enclosuredivid, formid, instance, start) {
    if (!CheckWysiwyg(instance))
        return;

    var selrange = GetSelectionRange(enclosuredivid);
    if (selrange == null)
        return;

    var segid = GetSegmentIDForNode(selrange.startContainer);
    if (segid == -1) {
        alert('To move a segment marker, position the cursor within an existing segment.');
        return;
    }

    var marker = GetMarker(enclosuredivid, start);

    if (!marker) {
        alert("Please position the marker - '{' or '}' - in the text before using this function.");
        return;
    }

    offset = CountToContainer(GetEditorDocBody(enclosuredivid) , marker, new Object());

    var args = { CorpusName: _corpusName, NameIfVersion: GetVersionName(formid), SegmentDefinitionID : segid, newOffset: offset, start: start ? 'true' : 'false' };

    var url = _siteUrl + 'Document/ChangeSegmentMarker';

    CallControllerAsync(url, args, function (result) {
        if (result != null) {
            if (result.Succeeded) {

                UpdateExtract(enclosuredivid, formid, instance);
                return;

            }
            alert('Error: ' + result.ErrorMsg);

        }

    });



}


//=====================================
//
// Code to deal with SegmentDefinition popup and attributes filter
//
//=====================================

function deleteattrib(r) {

    var i = r.parentNode.parentNode.rowIndex;
    var t = r.parentNode.parentNode.parentNode.parentNode;
    t.deleteRow(i);
    //document.getElementById('attribtable').deleteRow(i);

}

function AddAttribRow(nextrowindexinput, membername, tableid, rowidstem) {

    var nextrowindex = parseInt($('#' + nextrowindexinput).val());

    var args = { memberName: membername, CorpusName: _corpusName, rowindex: nextrowindex };
    var url = _siteUrl + 'Corpus/GetNewAttributeRowHtml';

    CallControllerAsync(url, args, function (result) {

        if (result.Succeeded) {
        }
        else {
            alert(result.ErrorMsg);
            return;
        }

        var table = document.getElementById(tableid);
        var rowCount = table.rows.length;
        var row = table.insertRow(rowCount);
        var rowid = rowidstem + nextrowindex;
        row.setAttribute('id', rowid);

        $(row).html(result.html);

        nextrowindex++;

        document.getElementById(nextrowindexinput).value = nextrowindex;

        BindAttribNameSelects(rowid, membername);


    });
}

function BindAttribNameSelects(rowid, membername) {

    var selector = '.attribnameselect';
    if (rowid)
        selector = '#' + rowid + ' ' + selector;

    $(selector).bind('change', function () {

        UpdateAttribRow(this, membername);
    });
}

function UpdateAttribRow(select, membername) {
    //var nextrowid = parseInt($('#nextattribrowindex').val());

    var newAttribName = select.value;

    var row = select[0].parentNode.parentNode.parentNode.parentNode;
    var rowid = row.getAttribute('id');
    var rowindexstr = rowid.substr(rowid.indexOf('-', 0) + 1);

    var args = { memberName: membername, CorpusName: _corpusName, rowindex: rowindexstr, currName: newAttribName };
    var url = _siteUrl + 'Corpus/GetNewAttributeRowHtml';

    CallControllerAsync(url, args, function (result) {

        if (result.Succeeded) {
        }
        else {
            alert(result.ErrorMsg);
            return;
        }

        $(row).html(result.html);

        BindAttribNameSelects(rowid);

    });
}

