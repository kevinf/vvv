﻿/*
Version Variation Visualisation (VVV)
http://www.delightedbeauty.org
    
Copyright (c) Kevin Flanagan, 2012.
http://www.kftrans.co.uk
  
This file is part of VVV.

VVV is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VVV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VVV.  If not, see <http://www.gnu.org/licenses/>.
*/

$(function () {

//    $('#backup').click(function (e) { Backup(e); });

});

//function Backup() {

//    var url = _siteUrl + 'CorpusStore/Backup';

//    CallControllerAsync(url, null, function (result) {
//        if (result != null) {
//            if (result.Succeeded) {
//                alert('Backup has been requested.');
//                return;

//            }
//            alert('Backup failed: ' + result.ErrorMsg);
//        }
//    });

//}