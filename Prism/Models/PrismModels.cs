using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
//using EblaAPI;
using EblaAPI;

namespace Prism.Models
{
    public class EmailValidationAttribute : RegularExpressionAttribute 
    { 
        public EmailValidationAttribute() : 
            base(@"^([\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+\.)*[\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+@((((([a-zA-Z0-9]{1}[a-zA-Z0-9\-]{0,62}[a-zA-Z0-9]{1})|[a-zA-Z])\.)+[a-zA-Z]{2,6})|(\d{1,3}\.){3}\d{1,3}(\:\d{1,5})?)$") 
        { 
        } 
    }

    public class ResetPasswordModel
    {

        //[DisplayName("Corpus name")]
        //[Required]
        //[StringLength(50, MinimumLength = 3)]
        //public string CorpusName { get; set; }

        [Required]
        [DisplayName("User name")]

        
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        
        [DisplayName("Email address")]
        [EmailValidation(ErrorMessage = "Not a valid email address")] 
        public string UserEmail { get; set; }
    }

    public class BaseCorpusModel
    {
        [DisplayName("Corpus name")]
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string CorpusName { get; set; }

    }
    public class CorpusModel : BaseCorpusModel
    {
        public string Description { get; set; }
        public DocumentModel BaseText { get; set; }
        public DocumentModel[] VersionList { get; set; }

        public bool CanWrite;
    }

    public class CorpusStoreModel
    {
        public CorpusModel[] CorpusList;

    }

    public class NewCorpusModel : BaseCorpusModel
    {
        public string Description { get; set; }

    }

    [PropertiesMustMatch("Password", "ConfirmPassword", ErrorMessage = "The password and confirmation password do not match.")]
    public class NewUserModel
    {
        [Required]
        [DisplayName("User name")]
        
        [RegularExpression(".{5,15}", ErrorMessage="The user name must be between 5 and 15 characters")] 
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [DisplayName("Email address")]
        [EmailValidation(ErrorMessage = "Not a valid email address")] 
        public string Email { get; set; }


        [Required]
        [RegularExpression("(?=^.{8,}$)((?=.*\\d)|(?=.*\\W+))(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*$", ErrorMessage = "The password must contain a mixture of upper and lower case letters, with at least one number or special character, and be at least 8 characters long")]
        [DataType(DataType.Password)]
        [DisplayName("Password")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Confirm password")]
        public string ConfirmPassword { get; set; }

        [Required]
        [DisplayName("Is an administrator")]
        public bool CanAdmin { get; set; }

    }

    [PropertiesMustMatch("NewPassword", "ConfirmPassword", ErrorMessage = "The new password and confirmation password do not match.")]
    public class ChangePasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Current password")]
        public string OldPassword { get; set; }

        [Required]
        [ValidatePasswordLength]
        [DataType(DataType.Password)]
        [DisplayName("New password")]
        [RegularExpression("(?=^.{8,}$)((?=.*\\d)|(?=.*\\W+))(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*$", ErrorMessage = "The password must contain a mixture of upper and lower case letters, with at least one number or special character, and be at least 8 characters long")]
        public string NewPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Confirm new password")]
        public string ConfirmPassword { get; set; }
    }


    public class UserRightsModel
    {
        [DisplayName("User name")]
        public string Username { get; set; }

        
        [DisplayName("Is an administrator")]
        public bool CanAdmin { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [DisplayName("Email address")]
        [EmailValidation(ErrorMessage = "Not a valid email address")] 
        public string Email { get; set; }

        public UserCorpusRightsModel[] CorpusRights { get; set; }
    }

    public class UserCorpusRightsModel
    {
        public string CorpusName { get; set; }
        public int RightsValue { get; set; }

        public bool CanRead() { return RightsValue > 0; }
        public bool CanWrite() { return (RightsValue & 2) > 0; }
        public void SetRightsValue(bool canRead, bool canWrite)
        {
            RightsValue = (canRead ? 1 : 0) + (canWrite ? 2 : 0);

        }

    }

    public class BaseDocumentModel : BaseCorpusModel
    {
        [DisplayName("Version name")]
        public string NameIfVersion { get; set; }
        public string Description { get; set; }
        [DisplayName("Author/translator")]
        public string AuthorTranslator { get; set; }
        [DisplayName("Reference date")]
        [Range(-10000, 2500, ErrorMessage= "Reference date must be between -10,000 and 2,500")]
        public int? ReferenceDate { get; set; }

        public string Genre { get; set; }
        [DisplayName("Copyright information")]
        public string CopyrightInfo { get; set; }

        [DisplayName("Segments")]
        public long SegmentCount { get; set; }

        public bool IsVersion { get; set; }

        public string LanguageCode { get; set; }
        public string OriginalNameIfVersion { get; set; }
        public string Information { get; set; }

        public int SafeDate() { if (ReferenceDate.HasValue) return ReferenceDate.Value; return -1; }
    }

    //public class DocumentMetadataModel : BaseDocumentModel
    //{
 
    //}

    public class DocumentModel : BaseDocumentModel
    {
        public int DocLength { get; set; }

    }

    public class ChangeSegmentMarkerModel : BaseDocumentModel
    {
        public int SegmentDefinitionID { get; set; }
        public int newOffset { get; set; }
        public bool start { get; set; }
    }

    public class DocumentExtractModel : DocumentModel
    {
        
        //[DisplayName("Start position")]
        //public int StartPos { get; set; }
        //public int Length { get; set; }
        //[DisplayName("Add segment markers")]
        //public bool AddSegmentMarkers { get; set; }
        //[DisplayName("Add segment formatting")]
        //public bool AddSegmentFormatting { get; set; }

        [DisplayName("Extract to show")]
        public int ExtractSegmentID { get; set; }

        public AttributeModel[] AttributeFilters { get; set; }

        public bool Editing { get; set; }

    }

    //public class SegmentDefinitionModel : DocumentExtractModel
    //{
    //    int? id { get; set; }
    //    int? startOffset { get; set; }
    //    int? length { get; set; }

    //}

    public class AlignModel
    {
        public DocumentExtractModel BaseTextModel { get; set; }

        public DocumentExtractModel VersionModel { get; set; }

        //[DisplayName("Extract to show")]
        //public int BaseTextExtractSegmentID { get; set; }

        //[DisplayName("Extract to show")]
        //public int VersionExtractSegmentID { get; set; }

        //public AttributeModel[] BaseTextAttributeFilters { get; set; }

        //public AttributeModel[] VersionAttributeFilters { get; set; }

    }

    public class AutoAlignModel : AlignModel
    {
        public int ConfirmedAlignmentBaseTextSegId { get; set; }
        public int ConfirmedAlignmentVersionSegId { get; set; }

    }

    public class HtmlUploadModel
    {
        public string CorpusName { get; set; }
        public string NameIfVersion { get; set; }

        public PrismEncoding encoding { get; set; }

        
    }


    public class PrismError
    {
        public string Message;
    }


    public class SegmentDefinitionModel : BaseDocumentModel
    {
        // Note - this needs refactoring. The following attribs should be replaced by 'SegmentDefinitionModel2'
        public int ID { get; set; }
        public int startPos { get; set; }
        public int length { get; set; }
        public AttributeModel[] Attributes { get; set; }

    
        public string Content; // display purposes only
        public int ExtractSegmentID { get; set; }


     
    }

    public class SegmentDefinitionModel2
    {
        public int ID { get; set; }
        public int startPos { get; set; }
        public int length { get; set; }
        public AttributeModel[] Attributes { get; set; }

    }

    public class SegmentDefinitionsModel : ResultModel
    {
		public int DocumentLength { get; set; }
        public SegmentDefinitionModel2[] SegmentDefinitions { get; set; }
    }

    public class AlignerBaseTextDocumentExtractModel
    {
        public DocumentExtractModel BaseTextModel { get; set; }
    }

    public class AlignerVersionDocumentExtractModel
    {
        public DocumentExtractModel VersionModel { get; set; }
    }



    public class AttributeModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }


    public class ResultModel
    {
        public bool Succeeded { get; set; }
        public string ErrorMsg { get; set; }
    }

    public class CalculationStatusResultModel : ResultModel
    {
        public string Status { get; set; }
        public bool Busy { get; set; }
    }

    public class GetHtmlResultModel : ResultModel
    {
        public string html { get; set; }
    }

	public class SegmentVariationResultModel : ResultModel
	{
		public SegmentVariation SegmentVariationData { get; set; }
	}

    public class PredefinedSegmentAttributesModel : BaseCorpusModel
    {
        public PredefinedSegmentAttribute[] PredefinedSegmentAttributes { get; set; }
    }

    public class PredefinedSegmentAttributeModel : BaseCorpusModel
    {
        public PredefinedSegmentAttribute PredefinedSegmentAttribute { get; set; }
    }

    public class PredefinedSegmentAttribute
    {
        public int ID { get; set; }

        // NOTE Ebla dependency - ':' is a legal first character, but Ebla uses that to signify 'internal' attributes (e.g. viv/eddy values)
        [RegularExpression("[a-z_][-a-z0-9_.]*", ErrorMessage="The name must be all lowercase, starting with either a letter or an underscore, then any combination of letters, numbers, full stops, underscores and hyphens.")] // see http://www.razzed.com/2009/01/30/valid-characters-in-attribute-names-in-htmlxml/
        public string Name { get; set; }

        [DisplayName("Show in Contents")]
        public bool ShowInTOC { get; set; }

        [DisplayName("Attribute type")]
        public EblaAPI.PredefinedSegmentAttributeType AttributeType { get; set; }

        public PredefinedSegmentAttributeValue[] Values { get; set; }
    }

    public class PredefinedSegmentAttributeValue
    {
        public string Value { get; set; }

        [DisplayName("Apply colouring")]
        public bool ApplyColouring { get; set; }



        //public bool IsTOCText { get; set; }

        public string ColourCode { get; set; }
    }

    public class AlignSegmentPairModel : BaseCorpusModel
    {
        public string VersionName { get; set; }
        public int BaseTextSegmentID { get; set; }
        public int VersionSegmentID { get; set; }
    }

    public class RemoveAlignmentsModel : AutoAlignModel
    {
        public bool DraftOnly { get; set; }
    }

    public class AlignSegmentPairResultModel : ResultModel
    {
        public int AlignmentID { get; set; }
        public bool isNew { get; set; }
        
    }

    public class GetAlignmentsResultModel : ResultModel
    {
        public EblaAPI.SegmentAlignment[] Alignments { get; set; }
    }

    public class GraphResultModel : ResultModel
    {
        public GraphModel[] GraphModels { get; set; }
    }

    public class GraphModel
    {
        public string VersionName { get; set; }
        public double EddyValue { get; set; }
        public string Genre { get; set; }
        public int? ReferenceDate { get; set; }
    }

    public class SearchModel : BaseCorpusModel
    {
        public AttributeModel[] AttributeFilters { get; set; }

    }

    public class EddyHistorySeriesModel : BaseCorpusModel
    {
        // if -1, ignore, otherwise the series just shows data for this segment.
        public int BaseTextSegmentId { get; set; }

        // if BaseTextSegmentId == -1, list of segment filters to apply, otherwise null,
        // in which case series shows data for all segments.
        public AttributeModel[] AttributeFilters { get; set; }

    }

    //public class EddyHistoryViewModel : BaseCorpusModel
    //{
    //    EddyHistorySeriesModel[] Series { get; set; }
    //}

    public class SimpleEddyChartModel : BaseCorpusModel
    {
        public int BaseTextSegmentId { get; set; }
    }

    public class EddyOverviewChartTooltipModel : BaseCorpusModel
    {
        public int BaseTextSegmentId { get; set; }
        public string VersionName { get; set; }
    }

    public class EddyOverviewChartTooltipResultModel : ResultModel
    {
        public string BaseTextSegmentText { get; set; }
        public string VersionSegmentText { get; set; }
    }

    public class SimpleEddyChartResultModel : ResultModel
    {
        public double[][] Points { get; set; }
        public string[] Versions { get; set; }
        public string Legend { get; set; }
    }

    public class EddyOverviewChartResultModel : ResultModel
    {
        public double[][] EddyValues { get; set; }
        public string[] Versions { get; set; }
        public int?[] VersionDates { get; set; }
        public int[] BaseTextSegIds { get; set; }
        public int[] SegmentStartPositions { get; set; }

    }

    public class SegmentTextResultModel : ResultModel
    {
        public string SegmentText { get; set; }
    }
}