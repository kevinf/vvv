<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Othello Corpus Map
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="subnav subnav-fixed">
        <div class="container">
        	<div class="row title">
        		<div class="span12">
        			<h1><%: ViewData["corpusname"] %> <small>Map View</small></h1>
        		</div>
        	</div>
        </div>
    </div>
	
    <div class="container map withSubnav">
    	<div class="row">
    		<div class="span12">
    			<iframe frameborder="0" src="http://othellomap.s3-website-eu-west-1.amazonaws.com/embed.html" width="100%" height="83%"></iframe>
    		</div>
    	</div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="<%= Url.Content("~/Scripts/Prism-Formatting.js") %>" type="text/javascript"></script>
	<script src="<%= Url.Content("~/Scripts/Prism-Utils.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/jquery.scrollto.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/jquery.sb.min.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/vvv/nanoScroller/jquery.nanoscroller.js") %>" type="text/javascript"></script>

    <script src="<%= Url.Content("~/Scripts/alignView/translation.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/alignView/nav.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/alignView/filter.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/alignView/connector.js") %>" type="text/javascript"></script>

    <link href="<%= Url.Content("~/Content/css/alignView.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= Url.Content("~/Content/css/nanoscroller.css") %>" rel="stylesheet" type="text/css" />
 	<link href="<%= Url.Content("~/Content/css/customselect.css") %>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Help" ContentPlaceHolderID="HelpMenuItem" runat="server">
<a id="help-popover" href="#"><i class="icon-question-sign icon-white"></i> What&#8217;s this?</a>
<div id="help-content" style="display:none">
	<p>An interactive map showing locations for creations, editions and publishing in the Othello corpus that was collected during VVV phase 1 &amp; 2.</p>
</div>
</asp:Content>