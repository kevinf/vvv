﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.ChangePasswordModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    VVV - Change password
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="page-header">
            <h1>
                Change password</h1>
            <p>
                <%: Html.ValidationSummary(false) %></p>
        </div>
        <div class="row">
            <% using (Html.BeginForm())
               {%>
            <%: Html.ValidationSummary(true) %>
            <fieldset>
                <legend>Details</legend>
                <div class="editor-label">
                    <%: Html.LabelFor(model => model.OldPassword) %>
                </div>
                <div class="editor-field">
                    <%: Html.EditorFor(model => model.OldPassword) %>
                    <%: Html.ValidationMessageFor(model => model.OldPassword) %>
                </div>
                <div class="editor-label">
                    <%: Html.LabelFor(model => model.NewPassword) %>
                </div>
                <div class="editor-field">
                    <%: Html.EditorFor(model => model.NewPassword)%>
                    <%: Html.ValidationMessageFor(model => model.NewPassword) %>
                </div>
                <div class="editor-label">
                    <%: Html.LabelFor(model => model.ConfirmPassword) %>
                </div>
                <div class="editor-field">
                    <%: Html.EditorFor(model => model.ConfirmPassword)%>
                    <%: Html.ValidationMessageFor(model => model.ConfirmPassword) %>
                </div>
                <p>
                    <input type="submit" value="Change Password" />
                </p>
            </fieldset>
            <% } %>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
