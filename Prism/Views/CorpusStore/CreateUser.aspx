﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.NewUserModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    VVV - Create user
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <% using (Html.BeginForm())
           {%>
        <div class="page-header">
            <h1>
                Create user</h1>
            <p>
                <%: Html.ValidationSummary(true) %></p>
        </div>
        <div class="row">
            <fieldset>
                <legend>User details</legend>
                <div class="editor-label">
                    <%: Html.LabelFor(model => model.UserName) %>
                </div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => model.UserName) %>
                    <%: Html.ValidationMessageFor(model => model.UserName) %>
                </div>
                <div class="editor-label">
                    <%: Html.LabelFor(model => model.Password) %>
                </div>
                <div class="editor-field">
                    <%: Html.EditorFor(model => model.Password) %>
                    <%: Html.ValidationMessageFor(model => model.Password) %>
                </div>
                <div class="editor-label">
                    <%: Html.LabelFor(model => model.ConfirmPassword) %>
                </div>
                <div class="editor-field">
                    <%: Html.EditorFor(model => model.ConfirmPassword)%>
                    <%: Html.ValidationMessageFor(model => model.ConfirmPassword) %>
                </div>
                <div class="editor-label">
                    <%: Html.LabelFor(model => model.CanAdmin) %>
                </div>
                <div class="editor-field">
                    <%: Html.CheckBoxFor(model => model.CanAdmin) %>
                    <%: Html.ValidationMessageFor(model => model.CanAdmin) %>
                </div>
                <div class="editor-label">
                    <%: Html.LabelFor(m => m.Email) %>
                </div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(m => m.Email) %>
                    <%: Html.ValidationMessageFor(m => m.Email) %>
                </div>

                <p>
                    <input type="submit" value="Create" />
                </p>
            </fieldset>
        </div>
        <% } %>
        <div>
            <%: Html.ActionLink("Back to List", "UsersAdmin") %>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
