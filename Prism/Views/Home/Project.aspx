﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="aboutTitle" ContentPlaceHolderID="TitleContent" runat="server">
    VVV - Project
</asp:Content>

<asp:Content ID="aboutContent" ContentPlaceHolderID="MainContent" runat="server">
	<div class="container project-page">
		<div class="row">
			<div class="span3 bs-docs-sidebar">
				<ul class="nav nav-list bs-docs-sidenav">
					<li><a href="#introduction"><i class="icon-chevron-right"></i> 1. Introduction</a></li>
					<li><a href="#tools"><i class="icon-chevron-right"></i> 2. Tools</a></li>
					<li><a href="#textstock"><i class="icon-chevron-right"></i> 3. Text stock</a></li>
					<li><a href="#eblaandprism"><i class="icon-chevron-right"></i> 4. Ebla and Prism</a></li>
					<li><a href="#visualinterfacedesign"><i class="icon-chevron-right"></i> 5. Visual interface design</a></li>
					<li><a href="#eddyandviv"><i class="icon-chevron-right"></i> 6. Eddy &amp; Viv</a></li>
					<li><a href="#transvis"><i class="icon-chevron-right"></i> 7. What&#8217;s next? TransVis</a></li>
					<li><a href="#copyrightholders"><i class="icon-chevron-right"></i> 8. Copyright holders</a></li>
					<li><a href="#references"><i class="icon-chevron-right"></i> 9. References</a></li>
				</ul>
			</div>
			<div class="span9">
				<section id="overview" class="page-head">
          			<div class="page-header">
          				<h1>Version Variation Visualization</h1>
	    				<p class="lead">Explore great works with their world-wide translations.</p>
          			</div>
          			<h2>Translation Array Prototype 1<br />
        			<small>Shakespeare&#8217;s Othello (Act 1, Scene 3) with 37 German translations (1766–2010)</small></h2>
        			<p><small><strong>Project Overview</strong> By Tom Cheesman, Kevin Flanagan and Stephan Thiel</small></p>
          		</section>
          		
          		<section id="introduction">
          			<div class="page-header">
          				<h1>1. Introduction</h1>
          			</div>
      				<p>The most important works of world literature, philosophy, and religion have been re-translated over and over again in many languages. The differences between these re-translations can help us understand (1) cross-cultural dynamics, (2) the histories of translating cultures, and (3) the translated works themselves, and their capacity to provoke re-interpretation.</p>
      				<p>We are building digital tools to help people explore, compare and analyse re-translations. As a &#8216;telescope array&#8217; examines a celestial object from many slightly different angles, so a Translation Array explores a cultural work through its refractions in different languages, times and places.</p>
      				<p>The scope is vast. Numerous re-translations exist of Aesop, Aristotle, Hans Christian Andersen, Avicenna, the Bhagavad Gita, the Bible, Buddhist scriptures, Chekhov, Confucius, Dante, Dostoyevsky, Dumas…</p>
      				<p>Shakespeare makes a great case study. Our experiment here uses 37 of the German translations of Shakespeare&#8217;s Othello. That&#8217;s a small fraction of Othello translations: see our <a href="http://www.delightedbeauty.org">crowd-sourcing site</a>.</p>
      				<p>We aim to create useful, instructive and enjoyable experiences for many kinds of text users, and to build cross-cultural exploration networks.</p>
      				<p>Our report on the work on this site, submitted to our main funder, the AHRC, in October 2012, is attached <a href="http://www.scribd.com/doc/122777775/Translation-Arrays-Version-Variation-Visualization-Phase-2-report-to-AHRC-October-2012">here</a>.</p>
          		</section>
          		
          		<section id="tools">
          			<div class="page-header">
          				<h1>2. Tools</h1>
          			</div>
      				<p>The backbone of this site is Ebla, the corpus store, and Prism, the segmentation and alignment tool – both developed by Kevin Flanagan. See section 4 below.</p>
      				<p>The navigation and interaction interfaces, designed by Stephan Thiel and Sebastian Sadowski (Studio Nand), retrieve and visualize text data and mathematical data from the segmented, aligned corpus. See sections 5 and 6 below.</p>
      				<p>Another exploratory interface, &#8216;ShakerVis&#8217;, has been developed by Zhao Geng and Robert Laramee, using a sample of our Othello texts (seven speeches in ten versions). Currently there is no online installation. See section 6 below (video). Further documentation is <a href="http://cs.swan.ac.uk/~cszg/vv/">here</a>.</p>
          		</section>
          		
          		<section id="textstock">
          			<div class="page-header">
          				<h1>3. Text stock</h1>
          			</div>
      				<p>To stock this Translation Array with texts, we:</p>
      				<ul>
      					<li>established a &#8216;base text&#8217; of the translated work: our base text is an inclusive collation of the &#8216;Moby Shakespeare&#8217; Othello (<a href="http://shakespeare.mit.edu/othello/full.html">from MIT</a>) with Michael Neill&#8217;s <a href="http://ukcatalogue.oup.com/product/9780199535873.do#.UP2IfB26erY">OUP edition</a> (2006), with some further modernization</li>
      					<li>identified a large number of comparable translation texts, using the widest possible selection criteria. Our Othello corpus includes verse and prose versions, full and &#8216;faithful&#8217; translations, and free adaptations which make lots of cuts and add new material. The texts are from books for readers, books for students, and typescripts for theatre productions. We include &#8216;on-translations&#8217;, such as a German translation of Boito&#8217;s Italian libretto for Verdi&#8217;s opera Otello</li>
      					<li>secured rights to use all the translations in this experiment – rights holders are identified below (section 8)</li>
      					<li>digitized the texts which were not &#8216;born digital&#8217;: photocopy, scan, OCR – we thank ABBYY for a free trial of Recognition Server; Alison Ehrmann did a lot of this work</li>
      					<li>corrected OCR output against page images</li>
      					<li>normalised text formatting, so that component segment types could be automatically identified in Prism (in Othello: stage directions, speech prefixes, speeches)</li>
      					<li>converted to HTML and uploaded to Ebla</li>
      					<li>used Prism to check alignments of pre-created text segments (i.e. speeches), and to create and align additional segments – sentences or phrases within speeches</li>
      				</ul>
          		</section>
          		
          		<section id="eblaandprism">
          			<div class="page-header">
          				<h1>4. Ebla &amp; Prism</h1>
          			</div>
      				<p>To create a Translation Array, a base text must be aligned with numerous versions in a given language. Some of those alignments may be straightforward, and others, less so. This application provides tools for doing this and for working with the results. It is designed to be as generally useful as possible. The user can define any given base text and align it with an arbitrary number of versions. Base text and versions are referred to here as documents. The base text and set of versions of it are together referred to as a corpus.</p>
      				<p>The kinds of relationship that may exist between a text and a translation (or version) of it are complex, and their definitions contested. Alignment refers to dividing up a text and a translation somehow into parts and drawing correspondences between the respective parts. This may lead to a straightforward, sequential, one-to-one correspondence between (say) sentences in a text and their renderings in a translation. However, with literary translation in particular, the correspondences are often much less straightforward.</p>
      				<p>The software powering this website has two main components:</p>
      				<ul>
      					<li>Ebla: stores documents, configuration details, segment and alignment information, calculates variation statistics, and renders documents with segment/variation information</li>
      					<li>Prism: provides a web-based interface for uploading, segmenting and aligning documents, then visualizing document relationships.</li>
      				</ul>
      				<p>Areas of interest in a document are demarcated using segments, which also can be nested or overlapped. Each segment can have an arbitrary number of attributes. For a play these might be &#8216;type&#8217; (with values such as &#8216;Speech&#8217;, &#8216;Stage Direction&#8217;), or &#8216;Speaker&#8217; (with values such as &#8216;Othello&#8217;, &#8216;Desdemona&#8217;), and so on.</p>
      				<p>In a similar way to <a href="http://www.catma.de/">CATMA</a>, segment positions are stored as character offsets within the document. (But unlike in <a href="http://www.catma.de/">CATMA</a>, texts in Ebla can be edited without losing this information.) Documents uploaded can contain markup, so HTML documents can be stored (in which case, some sanitisation is performed). When uploaded HTML documents are retrieved from Ebla and displayed by Prism, their HTML markup is preserved, so providing a WYSIWYG rendering of the document when segmenting, aligning or visualizing. If HTML formatting is to be applied to segments for display within Prism – which can fall anywhere within an HTML document structure – Ebla applies a recursive &#8216;greedy&#8217; algorithm to add the minimum additional tags required to achieve the formatting without breaking the document structure. If only part of a document is to be displayed – say, a section from the middle – Ebla ensures that any tags required from outside that part are appended or prepended, to ensure the partial content rendered is both valid and correctly formatted.</p>
      				<p>After segmentation, documents can be aligned using an interactive WYWISYG tool. Attribute filters can be applied to select only segments of interest, and limited &#8216;auto-align&#8217; functionality used to expedite the process.</p>
      				<p>Ebla can be used to calculate different kinds of variation statistics for base text segments based on aligned corpus content. These can potentially be aggregated-up for more coarse-grained use. The results can be navigated and explored using the visualization functionality in Prism. However, translation variation is just one of the corpus properties that could be investigated. Once aligned, the data could be analysed in many other ways.</p>      				
          		</section>
          		
          		<section id="visualinterfacedesign">
          			<div class="page-header">
          				<h1>5. Visual interface design</h1>
          			</div>
      				<p>Interface design should provide simple ways to explore complex data, so as to support research, but also appeal to non-research users who are interested in important cultural texts. The design work demonstrated here consists of experimental approaches, developed in an ongoing dialogue with linguists and software engineers.</p>
      				<p>The designs offer high-level visualizations of the corpus and of the structures of versions, as well as text-based views.</p>
      				<p>The &#8216;<a href="<%= Url.Content("~/Visualise/OthelloMap?CorpusName=Othello%2C%20Act%201%20Scene%203") %>">German Othello Timemap</a>&#8217; provides an interactive overview of the corpus metadata.</p>
      				<p>&#8216;<a href="<%= Url.Content("~/Visualise/Alignments?CorpusName=Othello%2C%20Act%201%20Scene%203") %>">Alignment Maps</a>&#8217; are high-level structure visualizations, based on segmentation and alignment information. They make it possible to find comparative patterns, and identify texts of special interest.</p>
      				<p>For text-based views, text readability is a key aspect. The &#8216;Parallel View&#8217; (i.e. for &#8216;<a href="<%= Url.Content("~/Visualise?CorpusName=Othello%2C%20Act%201%20Scene%203&NameIfVersion=Schiller%20and%20Voss")%>" >Schiller and Voss, 1805</a>&#8217;) offers visualization tools to simplify navigation in the base text and a selected version. Filter and sort functions, using segment attributes, speed up a search for a specific passage in one or more versions, or comparison of versions of speeches by a selected speaker. The &#8216;Parallel View&#8217; could be extended to multiple parallel texts, with further navigational aids.</p>
      				<p>The &#8216;<a href="<%= Url.Content("~/Visualise/Viv?CorpusName=Othello%2C%20Act%201%20Scene%203") %>">Eddy and Viv</a>&#8217; view enables a new kind of understanding of the base text, which is visually &#8216;annotated&#8217; on the basis of metrics derived algorithmically from the translations, while the varying versions of any selected segment are displayed (see section 6).</p>
      				<p>Another aspect of text-based views, which we aim to implement in future releases, is the ability to edit text in a new collaborative way.</p>
          		</section>
          		
          		<section id="eddyandviv">
          			<div class="page-header">
          				<h1>6. Eddy and Viv</h1>
          			</div>
      				<p>If we quantify differences among multiple translations, segment by segment, then we can explore how different parts of a work provoke different responses among translators as a group, and we can also devise new kinds of navigational aids for exploring a large corpus of translations. This is the basic idea behind the &#8216;Eddy and Viv&#8217; view (&#8216;E&amp;V&#8217;). Eddy and Viv are both calculated by algorithms (explained below).</p>
      				<p>In the &#8216;Eddy and Viv&#8217; view, a colour underlay annotates the base text, segment by segment. This shows which segments provoke most and least variation among translations. It means you can read Shakespeare through his German translators, even if you can&#8217;t read German.</p>
      				<p>The colour underlay represents &#8216;Viv&#8217; values for base text segments. These values are derived from the &#8216;Eddy&#8217; values calculated for each segment version. &#8216;Eddy&#8217; values measure how different one segment version is from all others. So the more variation there is among versions, the bigger the range of Eddy values is, and the higher the Viv value is.</p>
      				<p>In the &#8216;Eddy and Viv&#8217; view, when you select a base text segment, you see all the segment translations displayed. They are ranked in order from most typical to most unusual: that is, from lowest to highest Eddy value. Machine translations into English are also supplied. You can assess the variation (up to a point!) without knowing German.</p>
      				<p>Eddy values can also be used to visualize higher-level differences among translations. Our &#8216;Eddy History&#8217; graph shows average Eddy values for each version, on a timeline: this gives an overview of translators&#8217; general behaviour over time. Our &#8216;Eddy Variation Overview&#8217; graph shows Eddy values for all segments in all versions: this enables detailed comparison of version variation.</p>
      				<p><a href="http://cs.swan.ac.uk/~cszg/vv/">Geng and Laramee&#8217;s work</a> also uses Eddy analysis to explore how versions vary. Their &#8216;ShakerVis&#8217; interface presents Eddy values in scatterplots and parallel coordinates, and enables us to select and compare groups of segments and versions. Word lists are presented in heatmaps, and the use of words can be tracked across versions.</p>
      				<p><iframe class="geng-video" src="http://player.vimeo.com/video/58241751?byline=0&amp;portrait=0&amp;color=ffffff" width="870" height="544" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></p>
      				<p><small>Screencast of the interface and visualisations developed by <a href="http://cs.swan.ac.uk/~cszg/vv/">Geng and Laramee</a></small></p>
      				
      				<h2>6.1. So, what are Eddy and Viv?</h2>
      				<p>Eddy is a measure of how much one translation of a segment differs from all the other translations of it, in the same language, calculated in terms of the words used. Viv is a measure of how much the set of Eddys associated with one base text segment differs from the sets of Eddys for other base text segments.</p>
      				<p>(To put it differently: Eddy is the disturbance created when the flows of cultural histories intersect with the flows of individual re-translation decisions. Viv is the energy in the relation between the translated text and all its translations: all those disturbances, cultural changes and individual decisions.)</p>
      				<p>Eddy and Viv are not measures of properties of translations. They are measures of differences among translations. The purpose of implementing Eddy and Viv is to enable us to create new navigational aids for exploring such differences.</p>
      				<p>We are experimenting with different mathematical formulae for Eddy and Viv, based on methods from information retrieval and stylometry.</p>
      				
      				<h2>6.2. Eddy and Viv formulae</h2>
      				<p>The algorithms applied here operate on a corpus of texts, consisting of a base text and a number of versions. But these algorithms process only the text within those version segments which are aligned with one base text segment. They do not compare the version segment texts in any way with the base text, nor with the rest of the text of the versions.</p>
      				<p>The algorithms begin by tokenising each of the version segments, to produce a list of words used and their frequencies (total number of occurrences) for each version. These are referred to below as 'version word lists'. These lists are combined to produce a list of all words found across all versions, with their respective overall frequencies. This is referred to below as the 'corpus word list'.</p>
      				<p>Note that we do not exclude some function words (&#8216;stopwords&#8217;), nor do we stem or lemmatise the texts, or analyse compound words. Words of &#8216;low&#8217; semantic value, and inflections, can be important in the differences between small-scale segment translations. However, we will experiment with these kinds of processing in future work.</p>
      				
      				<h3>6.2.1. Metric A – Euclidean distance (Kevin Flanagan)</h3>
      				<p>Each word in the corpus word list is considered as representing an axis in N-dimensional space, where N is the length of the corpus word list. For each version, a point is plotted within this space whose co-ordinates are given by the word frequencies in the version word list for that version. (Words not used in that version have a frequency of zero.) The position of a notional 'average' translation is established by finding the <a href="http://en.wikipedia.org/wiki/Centroid#Of_a_finite_set_of_points">centroid</a> of that set of points.</p>
      				<p>An initial 'Eddy' variation value for each version is calculated by measuring the <a href="http://en.wikipedia.org/wiki/Euclidean_distance#N_dimensions">Euclidean distance</a> between the point for that version and the centroid.</p>
      				<p>The values produced provide a meaningful basis for comparison of versions within that corpus (that is, comparing version segments aligned with a given base text segment). However, their magnitude is affected by the lengths of the texts involved. In order to arrive at variation values that allow (say) translation variation for a long segment to be compared with translation variation for a short segment, a normalisation needs to be applied to compensate for the effect of text length. To establish a suitable normalisation, we calculated variation for a large number of base text segments of varying lengths, then plotted average Eddy value against segment length. We found a logarithmic relationship between the two, and arrived at a normalisation function that gives an acceptably consistent average Eddy value regardless of text length.</p>
      				<p>The Viv value for a base text segment is the average Eddy value of its aligned version segments.</p>
      				
      				<h3>6.2.2. Metric B – Tom Cheesman's primitive method</h3>
      				<p>A &#8216;distinctiveness value&#8217; for each word in the corpus word list is derived by dividing the corpus frequency value into the number of versions in the corpus. This produces a high value for rarely-used words, and a low value for commonly-used words. The Eddy value for a version is the sum of these values for all the words in that version word list. The Viv value for a base text segment is again defined as the average Eddy value of its aligned version segments. (See <a href="http://www.scribd.com/doc/101114673/Eddy-and-Viv">TC, &#8216;Translation Sorting&#8217;</a>.)</p>
      				
      				<h3>6.2.3. Metric C – with improved Viv</h3>
      				<p>Eddy is calculated as in Metric A, but Viv is calculated as the standard deviation of a segment version&#8217;s Eddy values, rather than the average. This takes account of the distribution of differences in the corpus, i.e. the varying numbers of identical segment versions.</p>
      				
      				<h2>6.3. Limitations of Eddy and Viv</h2>
      				<p>If two translations of a sentence use the same words, but in a different order, or with different punctuation (potentially making a major difference to the meaning), they have the same Eddy value. Conversely, orthographic differences with no semantic significance lead to different Eddy values: this includes contractions, different spellings, and different renderings of compound words. But standardisation of orthography would be an enormous task, and would efface real textual differences.</p>
      				<p>In a short segment, when we compare 37 translations using Metric A, a unique version can have the lowest Eddy value, if its word list happens to place it closest to the notional &#8216;average&#8217; version. Conversely, ten or more versions may have identical wording, but nevertheless share a surprisingly high Eddy value, if several other versions are closer to the calculated &#8216;average&#8217;. See for example the First Officer&#8217;s line “Here is more news”, or Roderigo&#8217;s line “What say you?” These kinds of cases call for refinement of the metrics.</p>
      				<p>The calculation of Viv is problematic as long as Eddy is based solely on word-counts. Semantic content analysis can be envisaged, to distinguish differences in surface wording from differences in meaning: translators&#8217; differing interpretations. This may be becoming possible for texts in English, but for other languages, the necessary corpus linguistics resources are not available.</p>
      				<p>But Eddy and Viv are navigational aids based on relative differences, not exact measures of the properties of translations. They are intended to facilitate explorations of texts, not to substitute for reading them. A certain fuzziness is no bad thing.</p>
          		</section>
          		
          		<section id="transvis">
          			<div class="page-header">
          				<h1>7. What&#8217;s next? TransVis</h1>
          			</div>
          			
          			<p>More collaboration. Our work aims to excite and harness the knowledge and interests of users: students, researchers, theatre people, publishers, translators, fans (as with our ongoing <a href="http://www.delightedbeauty.org">crowd-sourcing project</a>). We aim to build global networks of collaboration, by enabling users to interact and input data, analysis, and comment. We are currently seeking funding for further work, under the general title: &#8216;TransVis – Translation Visualized&#8217;.</p>
          			<p>More texts. The more translations, the more powerful the Array. In German alone we can include more printed, typescript, and born-digital texts, prompt-scripts from theatre archives, and texts from audiovisual archives (film, radio, television, online multimedia). And of course …</p>
          			<p>More languages. With multiple translations from many languages, we can explore which differences among translations are due to properties of target languages and cultures, and which are due to properties of the translated work.</p>
          			<p>More Arrays. Our approach can be applied to any corpus of comparable translations of whole works or parts of works, from and to any languages.</p>
          			<p>More visualizations. We have only scratched the surface of ways in which information encoded in multiple translations can be extracted and presented with interactive visualizations. There is great scope for more flexible, scalable tools. We aim to develop an adaptable toolsuite which will allow users to develop their own modes of analysis.</p>
          			<p>More cultural context. To explore not just how but why translators translate differently, we need contextual data: author/translator biographies, data on editions and other related events: theatrical and media productions, with reviews, etc., including audio-visual material. Relevant contextual data expands to include all aspects of cultural and intercultural history.</p>
          			<p>More audio-visual data. Our approach is unapologetically &#8216;text-centric&#8217;, but performance dimensions should not be neglected. Media documents are not just sources of more texts. Performances show how words (whatever the language) are re-interpeted by dramatic context, intonation, address, gesture, etc.– and ideally we would also encompass audience reactions.</p>
          			<p>Paratexts. In many source text editions as well as translations, paratexts carry important information: introductions, afterwords, footnotes, endnotes, glosses, glossaries, back-cover texts, etc. The prototype excludes them only for pragmatic reasons.</p>
          			<p>Source text instability. Not only translations vary: so do source works. Our &#8216;base text&#8217; is yet another English version of Othello. Translations usually have plural sources: translators work from different editions, often more than one, and they usually also work from previous translations.</p>
          			<p>Genetics. Translators look at previous translations (whether or not they say so) and these influence their decisions. Translation history can be explored in Translation Arrays, revealing citations, dependencies, possibly plagiarism, but also negative relations: resistance through change. The history of translation theory is also implicit in translation texts, as well as sometimes explicit in paratexts.</p>
          			<p>Page images. The texts presented have been normalized in a process involving manual checking, hence errors, as well as omission of some formatting features. Page images will help users appreciate the materiality of the texts, as well as check the Array versions.</p>
          		</section>
          		
          		<section id="copyrightholders">
          			<div class="page-header">
          				<h1>8. Copyright holders</h1>
          			</div>
          			<p>All German texts are reproduced on our website with permission as follows:</p>
          			<table class="table table-condensed table-striped">
					  <thead>
					    <tr>
					      <th>Translators</th>
					      <th>Date</th>
					      <th>Format</th>
					      <th>Copyright</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td>BabandLevy</td>
					      <td>1923</td>
					      <td>book</td>
					      <td>outofcopyright</td>
					    </tr>
					    <tr>
					      <td>B&auml;rfuss</td>
					      <td>2001</td>
					      <td>typescript theatre script (pdf)</td>
					      <td>Hartmann &amp; Stauffacher GmbH </td>
					    </tr>
					    <tr>
					      <td>Baudissin ed. Brunner</td>
					      <td>1947 (1832)</td>
					      <td>book (dual language) (study edition)</td>
					      <td>Brunner &copy; not identified</td>
					    </tr>
					    <tr>
					      <td>Baudissin ed. Mommsen</td>
					      <td>1855 (1832)</td>
					      <td>book</td>
					      <td>out of copyright</td>
					    </tr>
					    <tr>
					      <td>Baudissin ed. Wenig</td>
					      <td>2000 (1832)</td>
					      <td>online (Gutenberg)</td>
					      <td>non-copyright</td>
					    </tr>
					    <tr>
					      <td>Bodenstedt</td>
					      <td>1867</td>
					      <td>book</td>
					      <td>out of copyright</td>
					    </tr>
					    <tr>
					      <td>Bolte and Hamblock</td>
					      <td>1985</td>
					      <td>book (dual langauge) (study edition)</td>
					      <td>Philipp Reclam jun.</td>
					    </tr>
					    <tr>
					      <td>Buhss</td>
					      <td>1996</td>
					      <td>typescript theatre script (pdf)</td>
					      <td>Henschel Schauspiel Theaterverlag </td>
					    </tr>
					    <tr>
					      <td>Engel</td>
					      <td>1939</td>
					      <td>typescript theatre script (typewriter)</td>
					      <td>Felix Bloch Erben</td>
					    </tr>
					    <tr>
					      <td>Engler</td>
					      <td>1976</td>
					      <td>book (dual language) (study edition)</td>
					      <td>Stauffenberg Verlag Brigitte Narr</td>
					    </tr>
					    <tr>
					      <td>Felsenstein</td>
					      <td>1980</td>
					      <td>book (dual language: Italian/German)</td>
					      <td>G Rircodi &amp; Co C.S.p.A., Milan </td>
					    </tr>
					    <tr>
					      <td>Flatter</td>
					      <td>1952</td>
					      <td>book and theatre script (print)</td>
					      <td>Theater-Verlag Desch</td>
					    </tr>
					    <tr>
					      <td>Fried</td>
					      <td>1970</td>
					      <td>book (dual language)</td>
					      <td>Felix Bloch Erben </td>
					    </tr>
					    <tr>
					      <td>Gildemeister</td>
					      <td>1871</td>
					      <td>book</td>
					      <td>out of copyright</td>
					    </tr>
					    <tr>
					      <td>Gundolf</td>
					      <td>1909</td>
					      <td>book</td>
					      <td>out of copyright</td>
					    </tr>
					    <tr>
					      <td>Günther</td>
					      <td>1992</td>
					      <td>book (dual language) and theatre script (print)</td>
					      <td>Hartmann &amp; Stauffacher GmbH</td>
					    </tr>
					    <tr>
					      <td>Karbus</td>
					      <td>2006</td>
					      <td>typescript theatre script (pdf)</td>
					      <td>Thomas Sessler Verlag </td>
					    </tr>
					    <tr>
					      <td>Laube</td>
					      <td>1978</td>
					      <td>typescript theatre script </td>
					      <td>Verlag der Autoren</td>
					    </tr>
					    <tr>
					      <td>Lauterbach</td>
					      <td>1973</td>
					      <td>typescript theatre script (typewriter)</td>
					      <td>Henschel Schauspiel Theaterverlag</td>
					    </tr>
					    <tr>
					      <td>Leonard</td>
					      <td>2010</td>
					      <td>typescript theatre script (pdf)</td>
					      <td>Christian Leonard &amp; Shakespeare Company </td>
					    </tr>
					    <tr>
					      <td>Motschach</td>
					      <td>1992</td>
					      <td>typescript theatre script (typewriter)</td>
					      <td>Drei Masken Verlag </td>
					    </tr>
					    <tr>
					      <td>Ortlepp </td>
					      <td>1839</td>
					      <td>book</td>
					      <td>out of copyright</td>
					    </tr>
					    <tr>
					      <td>Rothe </td>
					      <td>1956</td>
					      <td>book and theatre script (print)</td>
					      <td>Thomas Sessler Verlag</td>
					    </tr>
					    <tr>
					      <td>Rüdiger</td>
					      <td>1983</td>
					      <td>typescript theatre script (typewriter)</td>
					      <td>Felix Bloch Erben</td>
					    </tr>
					    <tr>
					      <td>Schaller</td>
					      <td>1959</td>
					      <td>book</td>
					      <td>Pegasus Theater- und Medienverlag GmbH / Verlag Autorenagentur </td>
					    </tr>
					    <tr>
					      <td>Schiller</td>
					      <td>1805</td>
					      <td>book (scholarly edition)</td>
					      <td>Hermann Böhlaus Nachfolger / J. B. Metzler </td>
					    </tr>
					    <tr>
					      <td>Schröder</td>
					      <td>1962</td>
					      <td>book </td>
					      <td>Suhrkamp Theater &amp; Medien</td>
					    </tr>
					    <tr>
					      <td>Schwarz</td>
					      <td>1941</td>
					      <td>typescript theatre script (typewriter)</td>
					      <td>Shakespeare-Bibliothek München </td>
					    </tr>
					    <tr>
					      <td>Swaczynna</td>
					      <td>1972</td>
					      <td>typescript theatre script (typewriter)</td>
					      <td>Jussenhoven &amp; Fischer </td>
					    </tr>
					    <tr>
					      <td>Vischer</td>
					      <td>1887</td>
					      <td>book (study edition)</td>
					      <td>out of copyright</td>
					    </tr>
					    <tr>
					      <td>Wachsmann</td>
					      <td>2005</td>
					      <td>typescript theatre script (typewriter)</td>
					      <td>Gustav Kiepenheuer Bühnenvertriebs GmbH</td>
					    </tr>
					    <tr>
					      <td>Wieland </td>
					      <td>1766</td>
					      <td>online (Gutenberg)</td>
					      <td>out of copyright</td>
					    </tr>
					    <tr>
					      <td>Wolff</td>
					      <td>1920</td>
					      <td>book</td>
					      <td>out of copyright</td>
					    </tr>
					    <tr>
					      <td>Zaimoglu and Senkel</td>
					      <td>2003</td>
					      <td>book and typescript theatre script (pdf)</td>
					      <td>Rowohlt Theaterverlag</td>
					    </tr>
					    <tr>
					      <td>Zeynek</td>
					      <td>1948</td>
					      <td>typescript theatre script (typewriter) </td>
					      <td>Ahn &amp; Simrock Bühnen- und Musikverlag </td>
					    </tr>
					    <tr>
					      <td>Zimmer</td>
					      <td>2007</td>
					      <td>typescript theatre script (pdf)</td>
					      <td>Deutscher Theaterverlag Weinheim</td>
					    </tr>
					  </tbody>
					</table>
          		</section>
          		
          		<section id="references">
          			<div class="page-header">
          				<h1>9. References</h1>
          			</div>
          			
          			<h2>9.1. Digital Work</h2>
          			<ul class="unstyled references">
          				<li>Drabek, P., T. Pavel et al. (2006), Kapradi (Czech translations of Shakespeare and other English drama). At: <a href="http://www.phil.muni.cz/kapradi/">http://www.phil.muni.cz/kapradi/</a></li>
          				<li>Eder, M. and J. Rybicki (2012), Computational Stylistics. At: <a href="https://sites.google.com/site/computationalstylistics/">https://sites.google.com/site/computationalstylistics/</a></li>
          				<li>Fry, B. (2009), On the Origin of Species: The Preservation of Favoured Traces. At: <a href="http://benfry.com/traces">http://benfry.com/traces</a></li>
          				<li>Meister, J. C, et al. (2012). CATMA 3.2. Computer Aided Textual Markup &amp; Analysis. At: <a href="http://catma.de/">http://catma.de</a></li>
          				<li>Rybicki, J. (2003) Original Characters. At: <a href="http://www.cyf-kr.edu.pl/~strybick/original_characters.htm">http://www.cyf-kr.edu.pl/~strybick/original_characters.htm</a></li>
          				<li>Schreibman, S. et al. (2010), Versioning Machine 4.0. At: <a href="http://v-machine.org">http://v-machine.org</a></li>
          				<li>Thiel, S. (2010), Understanding Shakespeare. <a href="www.understanding-shakespeare.com">www.understanding-shakespeare.com</a></li>
          			</ul>
          			
          			<h2>9.2. VVV Team Work</h2>
          			<ul class="unstyled references">
          				<li>Cheesman, T. and the Version Variation Visualization Team (2013) (forthcoming), Translation Sorting: Eddy and Viv in Translation Arrays. In MacLeod, M., Wiggin, B. et al. (eds), Un/Translatables: New Maps for Germanic Languages. Northwestern UP. At: <a href="http://www.scribd.com/doc/101114673/Eddy-and-Viv">http://www.scribd.com/doc/101114673/Eddy-and-Viv</a></li>
          				<li>Cheesman, T., Geng, Z., Laramee, R.S.,  Flanagan, K.,  Thiel, S., Hope, J., Ehrmann, A. (2012) Translation Arrays: Exploring Cultural Heritage Texts Across Languages. DH2012 Conference abstract. At: <a href="http://www.dh2012.uni-hamburg.de/conference/programme/abstracts/translation-arrays-exploring-cultural-heritage-texts-across-languages/">http://www.dh2012.uni-hamburg.de/conference/programme/abstracts/translation-arrays-exploring-cultural-heritage-texts-across-languages/</a></li>
          				<li>Cheesman, T., Geng, Z., Flanagan, K.,  Thiel, S. (2012), Exploring  Multiple Versions of Cultural Heritage Texts Across Languages. Leipzig E-Humanities Seminar abstract. At: <a href="http://www.e-humanities.net/assets/seminar/2012/seminar%20on%20oct%2024th.pdf">http://www.e-humanities.net/assets/seminar/2012/seminar%20on%20oct%2024th.pdf</a></li>
          				<li>Geng, Z.,  Laramee, R.S.,  Cheesman,  T., Berry, D.M.,  Ehrmann, A. (2011), Visualizing Translation Variation: Shakespeare&#8217;s Othello. Advances in Visual Computing: Lecture Notes in Computer Science, vol.  6938, pp. 653 – 663. At: <a href="http://www.cs.swan.ac.uk/~cszg/text/isvc.pdf">http://www.cs.swan.ac.uk/~cszg/text/isvc.pdf</a></li>
          				<li>Geng, Z.,  Laramee, R.S.,  Cheesman,  T., Rothwell, A.,  Berry, D.M.,  Ehrmann, A. (2011), Visualizing Translation Variation of Othello: A Survey of Text Visualization and Analysis Tools. Technical report.</li>
          				<li>Geng, Z., Laramee, R.S.,  Cheesman,  T., Flanagan, K.,  Thiel, S. (2013?), ShakerVis: Visual Analysis of Segment Variation of German Translations of Shakespeare&#8217;s Othello. Technical report. Under review for Information Visualization. See attached video.</li>
          				<li>Wilson, M. L., Geng, Z., Laramee, R.S., Cheesman, T., Rothwell, A.J., Berry, D.M., Ehrmann, A. (2011), Studying Variations in Culture and Literature: Visualizing Translation Variations in Shakespeare's Othello. Poster paper at the ACM Web Science 2011 Conference, Koblenz, Germany, 14-17 June 2011. At: <a href="http://journal.webscience.org/541/">http://journal.webscience.org/541/</a></li>
          			</ul>
          			
          			<h2>9.3. Other Work</h2>
          			<ul class="unstyled references">
          				<li>Altintas, K. et al. (2007), Language Change Quantification Using Time-separated Parallel Translations. Literary and Linguistic Computing 22/4: 375-393.</li>
          				<li>Cheesman, T. (2011), Thirty Times More Fair Than Black: Shakespeare Retranslation as Political Restatement. Angermion, 4: 1-51.</li>
          				<li>&#8212; (2014) (forthcoming), Mutations of a Difficult Moment: Othello 1.3. In Smith, B. and Rowe, K. (eds), Cambridge World Shakespeare Encyclopaedia, vol.2: The World&#8217;s Shakespeare. Cambridge UP.</li>
          				<li>Crane, G., and Lüdeling, A. (2012), Introduction to the Special Issue on Corpus and Computational Linguistics, Philology, and the Linguistic Heritage of Humanity. Journal of Computing and Cultural Heritage 5/1, article 1. At: <a href="http://dl.acm.org/citation.cfm?doid=2160165.2160166">http://dl.acm.org/citation.cfm?doid=2160165.2160166</a></li>
          				<li>Delabastita, D. (2003), Introduction: Shakespeare in Translation: A Bird&#8217;s Eye View of Problems and Perspectives. Ilha do Desterro 45, pp 103-115. At: <a href="http://www.ilhadodesterro.ufsc.br/pdf/45%20A/dirk%20delabastita%20A.pdf">http://dl.acm.org/citation.cfm?doid=2160165.2160166</a></li>
          				<li>&#8212; (2009) Shakespeare, In Baker and Saldanha, eds., Routledge  Encyclopedia of Translation Studies, 2nd  edition, US: Routledge, pp.258-69.</li>
          				<li>Foz, C and M.S.C.Serrano (2005), Dynamique historique des (re)traductions du Quijote en français: questions méthodologiques et premiers resultants, META 50/3: 1042-50.</li>
          				<li>Galey, A. and R.Siemens, eds (2008), “Reinventing Shakespeare in the Digital Humanities”, Shakespeare 4/3 (special issue: Reinventing Digital Shakespeare): 201-7.</li>
          				<li>Gürcaglar, S.T. (2009), Retranslation. In Baker, M. and Saldanha, G. (eds), Encyclopedia of Translation Studies. Routledge, Abingdon and New York, pp. 232-6.</li>
          				<li>Hanna, S.F. (2005), “Othello in Egypt: Translation and the (Un)making of National Identity”, in: Translation and the Construction of Identity, St.Jerome: 109-128.</li>
          				<li>Hope, J. and Witmore, M. (2010), “The Hundredth Psalm to the Tune of &#8216;Green Sleeves&#8217;”: Digital Approaches to Shakespeare&#8217;s Language of Genre. Shakespeare Quarterly 61/3: 357-90.</li>
          				<li>Mathijssen, J.W. (2007), The Breach and the Observance: Theatre retranslation as a strategy of artistic differentiation , with special reference to retranslations of Shakespeare&#8217;s Hamlet (1777-2001). PhD, Utrecht. At: <a href="http://www.dehamlet.nl/the-breach-and-the-observance.htm">http://www.dehamlet.nl/the-breach-and-the-observance.htm</a></li>
          				<li>Manning, C. D., P. Raghavan and H. Schütze, Introduction to Information Retrieval, Cambridge University Press. 2008. Companion website: <a href="http://nlp.stanford.edu/IR-book/">http://nlp.stanford.edu/IR-book/</a></li>
          				<li>Moretti, F (1998) Atlas of the European Novel, Verso,London.</li>
          				<li>&#8212; (2005) Graphs, Maps, Trees: Abstract Models for a Literary History. Verso, London.</li>
          				<li>&#8212; (2011) Network Theory, Plot Analysis. Standford Literary Lab Pamplets #2. At: <a href="http://litlab.stanford.edu/LiteraryLabPamphlet2.pdf">http://litlab.stanford.edu/LiteraryLabPamphlet2.pdf</a></li>
          				<li>Monroy, C. et al. (2002), “Visualization of Variants in Textual Collations to Analyze the Evolution of Literary Works in the Cervantes Project”, Lecture Notes in Computer Science 2458: 199-211.</li>
          				<li>Oakes, Michael P. and Meng Ji (2012), Quantitative Methods in Corpus-Based Translation Studies, John Benjamins</li>
          				<li>O'Driscoll, K. (2011), Retranslation through the Centuries: Jules Verne in English, Peter Lang.</li>
          				<li>Price, K. M. (2008), Electronic Scholarly Editions. In Schreibman, S., and Siemens, R. (eds), A Companion to Digital Literary Studies, Blackwell, Oxford. At: <a href="www.digitalhumanities.org/companionDLS">www.digitalhumanities.org/companionDLS</a></li>
          				<li>Pujante, A.L. and Hoenselaars, T., eds (2003) 400 Years of Shakespeare in Europe, U Delaware Press</li>
          				<li>Ramsay, S. (2007), Algorithmic Criticism. In Schreibman, S., and Siemens, R. (eds), A Companion to Digital Literary Studies, Blackwell, Oxford. At: <a href="www.digitalhumanities.org/companionDLS/">www.digitalhumanities.org/companionDLS/</a></li>
          				<li>Rumbold, K. (2010), From "Access" to "Creativity": Shakespeare Institutions, New Media, and the Language of Cultural Value. Shakespeare Quarterly 61/3: 313-336</li>
          				<li>Rybicki, J. (2006), Burrowing into Translation: Character Idiolects in Henryk Sienkiewicz's Trilogy and its Two English Translations. Literary and Linguistic Computing 21(1): 91-103.</li>
          				<li>&#8212; (2012), The Great Mystery of the (Almost) Invisible Translator: Stylometry in Translation. In Oakes, Michael P. and Meng Ji, Quantitative Methods in Corpus-Based Translation Studies, John Benjamins, 231-48.</li>
          				<li>Saldanha, Gabriela (2009), Principles of corpus linguistics and their application to translation studies research. Revista Tradumatica 7: 1-7.</li>
          				<li>Schmidt, D. (2010), The Inadequacy of Embedded Markup for Cultural Heritage Texts, Literary and Linguist Computing 25(3): 337-356.</li>
          				<li>Schmidt, D. and Colomb, R. (2009), A Data Structure for Representing Multi-version Texts Online. International Journal of Human-Computer Studies 67/6: 497-514.</li>
          				<li>Stein, P. (2005), Die Übersetzungen von Titus Livius' Ab Urbe condita […]. In C.D.Pusch et al. eds, Romance Corpus Linguistics II: Korpora und diachrone Sprachwissenschaft, Berlin: Narr: 57-70.</li>
          				<li>Trettien, W.A 2010 “Disciplining Digital Humanities, 2010” Shakespeare Quarterly 61/3: 391-400.</li>
          				<li>Venuti, L. (1995), The Translator's Invisibility: A History of Translation. Routledge.</li>
						<li>&#8212; (1998), The Scandals of Translation: Towards an Ethics of Difference. Routledge.</li>
          			</ul>
          			<h2>Citation suggestion for this page</h2>
          			<p>Tom Cheesman, Kevin Flanagan and Stephan Thiel, &#8216;Translation Array Prototype 1: Project Overview&#8217;, at <a href="www.delightedbeauty.org/vvv">www.delightedbeauty.org/vvv</a> (September 2012 - January 2013)</p>
          		</section>
			</div>
		</div>
	</div>
</asp:Content>
