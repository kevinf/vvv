﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.ResetPasswordModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    VVV - Reset password
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="page-header">
            <h2>
                Reset password</h2>
        </div>
        <div class="row">
            <div class="span12">
                <% using (Html.BeginForm())
                   {%>
                <%: Html.ValidationSummary(true) %>
                <fieldset>
                    <legend></legend>
                    <p>
                        Enter your user name and the email address recorded for you, and press 'Reset' to reset your
                        password.</p>
                        <br />
                    <div class="editor-label">
                        <%: Html.LabelFor(model => model.UserName) %>
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.UserName)%>
                        <%: Html.ValidationMessageFor(model => model.UserName)%>
                    </div>
                    <div class="editor-label">
                        <%: Html.LabelFor(model => model.UserEmail) %>
                    </div>
                    <div class="editor-field">
                        <%: Html.TextBoxFor(model => model.UserEmail) %>
                        <%: Html.ValidationMessageFor(model => model.UserEmail) %>
                    </div>
                    <p>
                        <input type="submit" value="Reset" />
                    </p>
                </fieldset>
                <% } %>
                <%: ViewData["resultmsg"] %>
            </div>
        </div>
    </div>
    <%--  <div>
        <%: Html.ActionLink("Back to List", "Index") %>
    </div>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="HelpMenuItem" runat="server">
</asp:Content>
