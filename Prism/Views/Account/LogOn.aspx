﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.LogOnModel>" %>

<asp:Content ID="loginTitle" ContentPlaceHolderID="TitleContent" runat="server">
    VVV - Log in
</asp:Content>
<asp:Content ID="loginContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container login-page">
        <div class="hero-unit">
            <h1>
                Welcome!</h1>
            <% if (bool.Parse(ViewData["ispublic"].ToString()))
               { %>
            <p>
                This part of VVV is open to the public. Feel free to explore all tools with the
                guest credentials we have entered for you below. You can even upload data, work
                on documents, etc.</p>
            <p>
                However, we will clean up the data for this demonstration site from time to time
                to make sure all visitors have the same experience.<br />
                If you want to work with VVV and want your data to be safe, we are more than happy
                to provide you with access to our <a href="http://www.delightedbeauty.org/vvvclosed">
                    research installation</a>.</p>
            <p>
                Simply send us an <a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;%74%72%61%6E%73%61%72%72%61%79%73%40%67%6D%61%69%6C%2E%63%6F%6D">
                    email.</a></p>
            <% } %>
            <% using (Html.BeginForm("LogOn", "Account", new {@returnUrl=Model.returnUrl}, FormMethod.Post, new { @class = "form-inline login-form" }))
               { %>
            <%: Html.ValidationSummary(true, "Login was unsuccessful. Please correct the errors and try again.", new {@class="alert alert-error"}) %>
            <%: Html.TextBoxFor(m => m.UserName, new {@placeholder="Your Username", @class="input-large"}) %>
            <%: Html.ValidationMessageFor(m => m.UserName) %>
            <%: Html.PasswordFor(m => m.Password, new {value=Model.Password, @placeholder="Your Password", @class="input-large"}) %>
            <%: Html.ValidationMessageFor(m => m.Password) %>
            <button type="submit" class="btn btn-large">
                Log in</button>
            <%: Html.CheckBoxFor(m => m.RememberMe) %>
            <%: Html.LabelFor(m => m.RememberMe) %>
            <% } %>
            <p class="small forgotten">
                Forgotten your password? Click
                <%: Html.ActionLink("here", "Reset", "Account") %>.</p>
        </div>
    </div>
</asp:Content>
