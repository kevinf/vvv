﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.DocumentExtractModel>" %>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="<%= Url.Content("~/Scripts/Prism-Formatting.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/Prism-Utils.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/Prism-Editor.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/Prism-Document.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/ckeditor/ckeditor.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/ckeditor/adapters/jquery.js") %>" type="text/javascript"></script>
    <link href="<%= Url.Content("~/Content/TaggingStyles.css") %>" rel="stylesheet" type="text/css" />
    <link id="markercss" href="<%= Url.Content("~/Content/EditorSegmentMarkers.css") %>" rel="stylesheet" type="text/css" />
    <link id="corpuscss" href="<%= Url.Content("~/Corpus/CorpusCss?CorpusName=" + Model.CorpusName) %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    VVV - <%: Model.CorpusName %>:
            <% if (string.IsNullOrEmpty(Model.NameIfVersion))
           { %>
        Base Text
        <% }
           else
           { %>
        '<%:Model.NameIfVersion%>'<% } %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="container">
		<div class="page-header">
			<div class="row">
				<div class="span9">
				    <h2>
				        <%: Model.CorpusName %>:
				        <% if (string.IsNullOrEmpty(Model.NameIfVersion))
				           { %> Base text <% } else { %><%:Model.NameIfVersion%><% } %>
						<small> <%: Model.ReferenceDate%></small>
					</h2>
					<p>
						<strong>Description:</strong> <%: Model.Description%>
					</p>
				</div>
				<div class="span3">
					<p>
						<strong>Author/Translator:</strong> <%: Model.AuthorTranslator%>
					</p>
					<p>
						<strong>Copyright Information:</strong> <%: Model.CopyrightInfo%>
					</p>
					<p>
						<strong>Document length:</strong> <%: Model.DocLength%>
					</p>	
					<p>
						<%--<%: Html.ActionLink("Upload document", "Upload", "Document", new { CorpusName = Model.CorpusName, NameIfVersion = Model.NameIfVersion }, null)%>--%>
					</p>
				</div>
			</div>
		</div>
	    <div class="row">
			<div class="span12">
				<div class="well">
					<% using (Html.BeginForm( null, null, FormMethod.Post, new { id="docextractform", @class="form-horizontal" }))
					{ %>
					<%: Html.ValidationSummary(false) %>
				    <%: Html.HiddenFor(model => model.CorpusName, new { id = "corpusname" })%>
					<%: Html.HiddenFor(model => model.NameIfVersion, new { id = "versionname" })%>
					<%: Html.HiddenFor(model => model.DocLength)%>
					<div class="pull-right">
						<input type="button" value="View" id="viewbutton" />
					</div>
					<fieldset>
						<legend>
							View extract
						</legend>
						<div class="control-group">
							<label class="control-label" for="">Extract to show</label>
							<div class="controls">
								<%: Html.DropDownListFor(model => model.ExtractSegmentID, (System.Collections.Generic.IEnumerable<SelectListItem>)ViewData["TOCSegments"]) %>
				                <%: Html.ValidationMessageFor(model => model.ExtractSegmentID) %>
							</div>
						</div>
					
						<div class="control-group">
							<label class="control-label">Segment filter <a href="#" class="tt" title="Names and values for segments can be freely defined for a corpus. Select if needed. Click ‘View’." rel="tooltip" data-placement="right"><i class="icon-question-sign"></i></a></label>
							<div class="controls">
								<table id="attribfiltertable" class="table">
	                                <thead>
	                                    <tr>
	                                        <th>
	                                            Name
	                                        </th>
	                                        <th>
	                                            Value
	                                        </th>
	                                        <th>
	                                        </th>
	                                    </tr>
	                                </thead>
	                                <tbody>
										<%
										if (Model.AttributeFilters != null && Model.AttributeFilters.Length > 0)
				                        for (int i = 0; i < Model.AttributeFilters.Length; i++)
				                        {
											string indexval = i.ToString();
											string rowid = "attribfilterrow-" + indexval;
											string rowhtml = Prism.Controllers.CorpusController.AttributeRowHtml("AttributeFilters", Model.CorpusName, i, Model.AttributeFilters[i].Name, Model.AttributeFilters[i].Value, false);                                                                                         
											%>
											<tr id='<%=rowid%>'>
											<%=rowhtml %>
											</tr>
										<% } %>
									</tbody>
								</table>
								<input type="button" value="Add" id="addfilterattrib" />
							</div>
						</div>
		
			        	<input type="hidden" id="nextattribfilterrowindex" value="<%= ViewData["nextattribfilterrowindex"] %>" />
	                    <%--                    <div class="editor-label">
	                        <%: Html.LabelFor(model => model.StartPos) %>
	                    </div>
	                    <div class="editor-field">
	                        <%: Html.TextBoxFor(model => model.StartPos) %>
	                        <%: Html.ValidationMessageFor(model => model.StartPos) %>
	                    </div>
	                    <div class="editor-label">
	                        <%: Html.LabelFor(model => model.Length) %>
	                    </div>
	                    <div class="editor-field">
	                        <%: Html.TextBoxFor(model => model.Length) %>
	                        <%: Html.ValidationMessageFor(model => model.Length) %>
	                    </div>
	                    <div class="editor-label">
	                        <%: Html.LabelFor(model => model.AddSegmentMarkers) %>
	                    </div>
	                    <div class="editor-field">
	                        <%: Html.CheckBoxFor(model => model.AddSegmentMarkers)%>
	                        <%: Html.ValidationMessageFor(model => model.AddSegmentMarkers)%>
	                    </div>
	                    <div class="editor-label">
	                        <%: Html.LabelFor(model => model.AddSegmentFormatting) %>
	                    </div>
	                    <div class="editor-field">
	                        <%: Html.CheckBoxFor(model => model.AddSegmentFormatting)%>
	                        <%: Html.ValidationMessageFor(model => model.AddSegmentFormatting)%>
	                    </div>--%>
	<%--                    <div style="float: right">
	                        <input type="button" value="Apply auto-segmentation" id="autoseg1" /><br />
	                    </div>--%>
					</fieldset>
					<% } %>
				</div>
			    <%--        <input type="button" value="Show segment markers" id="showsegs" />--%>
			    <div>
			        <%--<table style="width: 100%">
			            <tr>
			                <th style="width: 50%">
			                    Content
			                </th>
			                <th style="width: 50%">
			                    Raw content
			                </th>
			            </tr>
			            <tr valign="top">
			                <td id="extractcell"><div style="overflow: auto"><% Response.Write(ViewData["extract"].ToString()); %></div></td>
			                <td>
			                    <div style="overflow: auto; white-space:pre-wrap">
			                    
			                        <code>
			                            <%:  ViewData["extractindented"].ToString() %></code></div>
			                </td>
			            </tr>
			        </table>--%>
			        <%--<div  id="extractdiv" ><% Response.Write(ViewData["extract"].ToString()); %></div>
			        --%>
			        <div id="editorenclosurediv">
			            <textarea id="doceditor" name="doceditor">
			        <% Response.Write(ViewData["extract"].ToString()); %>
			<%--        &lt;span class=&quot;startmarker&quot;&gt;test&lt;br /&gt;&lt;/span&gt;
			        &lt;span style=&quot;background-color: #FFFF00&quot;&gt;test&lt;br /&gt;&lt;/span&gt;--%>
			        
			 <%--       <%: ViewData["extract"].ToString()%>--%>
			        
			        
			        </textarea>
			        </div>
			    </div>
			    <div>
                    <%  if (string.IsNullOrEmpty(Model.NameIfVersion))
                        { %>
                        <input type="button" value="Concordance" id="concordance" disabled="disabled" />
                        <br />
                    <% } %>
                    <input id="test" type="button"" value="Eddy chart" disabled="disabled" /> <br />
			        <%: Html.ActionLink("Back to corpus", "Index", "Corpus", new { CorpusName = Model.CorpusName }, new { @class="btn" })%>
			    </div>

			</div>
		</div>
	</div>
</asp:Content>
