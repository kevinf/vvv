﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.HtmlUploadModel>" %>

<%@ Import Namespace="Prism.HtmlHelpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    VVV - upload document
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="page-header">
            <h2>
                Upload
                <%: Model.CorpusName %>:
                <% if (string.IsNullOrEmpty(Model.NameIfVersion))
                   { %>
                base text
                <% }
                   else
                   { %>
                version '<%:Model.NameIfVersion %>'<% } %>
            </h2>
            <p>
                Files uploaded must be valid HTML documents. If your file is a Word document, you
                <i>could</i> use 'Save As' to convert it to HTML ... but that tends to produce results
                that are very messy internally. Another approach that usually gives cleaner results
                is to visit the CKEditor demo at <a href="http://ckeditor.com/demo">http://ckeditor.com/demo</a>,
                then do the following:</p>
            <ul>
                <li>Delete the demonstration document shown in the editor:
                    <ul>
                        <li>Click the 'Select all' button in the editor toolbar. </li>
                        <li>Press the 'Delete' key. </li>
                    </ul>
                </li>
                <li>Copy the contents of your Word document to the clipboard:
                    <ul>
                        <li>Open the document in Word. </li>
                        <li>Press 'Ctrl-A' to select everything.</li>
                        <li>Press 'Ctrl-C' to copy.</li>
                    </ul>
                </li>
                <li>Paste the document contents into CKEditor:
                    <ul>
                        <li>Click the 'Paste from Word' button in the editor toolbar. </li>
                    </ul>
                </li>
                <li>Copy the converted HTML results to the clipboard.
                    <ul>
                        <li>Click the 'Source' button in the editor toolbar. </li>
                        <li>Click the 'Select all' button in the editor toolbar. </li>
                        <li>Right-click the selected text and click 'Copy'. </li>
                    </ul>
                </li>
                <li>Create an HTML document from the clipboard contents.
                    <ul>
                        <li>Open the Notepad application (in Windows; or other basic text editor otherwise).</li>
                        <li>Click the 'Paste' option in the 'Edit' menu.</li>
                        <li>Click the 'Save as' option in the 'File' menu.</li>
                        <li>Change the 'Save as type' option to 'All files'.</li>
                        <li>Enter a filename ending '.htm', e.g. "MyConvertedDocument.htm"</li>
                        <li>Click the 'Save' button.</li>
                    </ul>
                </li>
            </ul>
            <p>
                NOTE: Uploading a document will delete any existing segmentation and alignment information
                relating to the document.</p>
        </div>
        <% using (Html.BeginForm(null, null, FormMethod.Post, new { enctype = "multipart/form-data" }))
           {%>
        <%: Html.ValidationSummary(true) %>
        <%: Html.HiddenFor(model => model.CorpusName)%>
        <%: Html.HiddenFor(model => model.NameIfVersion)%>
        <fieldset>
            <legend>Details</legend>
            <label for="file">
                Filename:</label>
            <input type="file" name="file" id="file" />
            <%: Html.EnumDropDownListFor(model => model.encoding) %>
            <p>
                <input type="submit" value="Upload" />
            </p>
        </fieldset>
        <% } %>
        <%--    <div>
        <%: Html.ActionLink("Back to corpus", "Index", "Corpus", ) %>
    </div>--%>
</asp:Content>
