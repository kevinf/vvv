﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.DocumentExtractModel>" %>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<%--    <script src="<%= Url.Content("~/Scripts/Prism-Formatting.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/Prism-Utils.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/Prism-Editor.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/Prism-Document.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/ckeditor/ckeditor.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/ckeditor/adapters/jquery.js") %>" type="text/javascript"></script>--%>

</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    VVV - <%= ViewData["VersionName"] %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="container">
		<header>
			<h1><%= ViewData["VersionName"] %> <small><%= ViewData["VersionRefDate"] %></small></h1>
			<p>Corpus: <%= ViewData["CorpusName"] %></p>
		</header>
		<section id="description">
			<div class="row">
				<div class="span5">
					<dl>
						<dt>Description</dt>
						<dd><%= ViewData["VersionDescription"] %></dd>
					</dl>
				</div>
				<div class="span7">
					<dl>
						<dt>Language</dt>
  						<dd><%= ViewData["VersionLanguage"] %></dd>
  						
						<dt>Reference Date</dt>
  						<dd><%= ViewData["VersionRefDate"] %></dd>
  						
  						<dt>Copyright Information</dt>
  						<dd><%= ViewData["VersionCopyrightInfo"] %></dd>
  						
  						<dt>Genre</dt>
  						<dd><%= ViewData["VersionGenre"] %></dd>
  						
  						<dt>Author/Translator</dt>
  						<dd><%= ViewData["VersionAuthor"] %></dd>
					</dl>
				</div>
			</div>
		</section>
		<section id="info">
			<div class="page-header">
				<h3>References &amp; Additional Information</h3>
			</div>
			
			<div class="row">
				<div class="span5">
					<p><%= ViewData["VersionInfo"] %></p>
				</div>
			</div>
		</section>
	</div>
</asp:Content>
