﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.SearchModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    VVV - Corpus:
    <%: Model.CorpusName%>
    - Eddy overview
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="page-header">
            <h1>
                <%: Model.CorpusName%>
                - Eddy overview</h1>
        </div>
        <div class="row">
        <div class="control-group">
            On this screen, you can select versions and plot the Eddy values for their segments.
            The X axis represents the position of the segment in the text, plotted against the
            Eddy value on the Y-axis. Hover the mouse pointer over a plot point to see the base
            text for the segment and the corresponding version text. Drag the mouse over the chart to select and zoom in on a rectangular
            region.
            </div>
            <div class="sorting container">
                <div class="control-label">
                    <a href="<%= Url.Content("~/Home/Project#eddyviv") %>" class="tt pull-left" title="The maths for <i>Eddy</i> and <i>Viv</i> algorithms is experimental. Click for more info"
                        rel="tooltip" data-placement="right"><i class="icon-question-sign pull-right"></i>
                        Variation metric:</a>
                </div>
                <div class="controls">
                    <select id="variation-type-select">
                        <!-- to be filled by JS -->
                    </select>
                </div>
            </div>
<%--            <div class="controls">
                Sort by:
                <select id="n">
                    <option value="name">Version Name</option>
                    <option value="eddy">Average Eddy Value</option>
                    <option value="year">Reference Date</option>
                </select>
            </div>--%>
        <div class="sorting container">
            <div class="pull-right subnav-item-right">
                Sort by:
                <select id="legend-sort-select">
                    <option value="name">Version Name</option>
                    <option value="eddy">Average Eddy Value</option>
                    <option value="year">Reference Date</option>
                </select>
            </div>
            <div class="control-label">Versions:</div>

        </div>

            <div id="legendcontainer">
            </div>
             <input type="button" id="zoomout" value="Zoom out" disabled="disabled" />
            <div id="chartxaxislabel">
<%--                <div class="pull-right subnav-item-right">
                <input type="button" id="zoomout" value="Zoom out" disabled="disabled" />
                </div>--%>
                Segment position in text
                </div>
            <div id="chart">
            </div>
            <%--  <br />
    Base text segment ID: <%: Model.BaseTextSegmentId %><br />
    Base text segment content:
    <br />
    <% Response.Write(ViewData["basetext"].ToString()); %>
            --%>
        </div>
    </div>
    <input id="corpusname" type="hidden" value="<%: Model.CorpusName %>" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="<%= Url.Content("~/Scripts/Prism-Utils.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/Prism-EddyOverviewChart.js") %>" type="text/javascript"></script>
    <style type="text/css">
        #legendcontainer
        {
            width: 1200px;
            height: 225px;
        }
        
        #chart
        {
            width: 1200px;
            height: 600px;
        }
        
        #chartxaxislabel
        {
            width: 1200px;
            text-align: center;
        }
    </style>
</asp:Content>
