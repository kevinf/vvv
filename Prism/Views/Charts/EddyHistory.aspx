﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.EddyHistorySeriesModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    VVV - Corpus:
    <%: Model.CorpusName%>
    - Eddy history
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="page-header">
            <h1>
                <%: Model.CorpusName%>
                - Eddy history</h1>
        </div>
        <div class="row">
            <div id="chart" style="width: 1200px; height: 600px">
            </div>
            <%--  <br />
    Base text segment ID: <%: Model.BaseTextSegmentId %><br />
    Base text segment content:
    <br />
    <% Response.Write(ViewData["basetext"].ToString()); %>
            --%>
        </div>
    </div>
    <input id="corpusname" type="hidden" value="<%: Model.CorpusName %>" />
    <input id="basetextsegid" type="hidden" value="<%: Model.BaseTextSegmentId %>" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="<%= Url.Content("~/Scripts/Prism-EddyHistory.js") %>" type="text/javascript"></script>
</asp:Content>
