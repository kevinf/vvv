﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Test
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="testdiv" style="height: 300px; z-index: 2">
    </div>
    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" style="position: absolute;
        top: 0; left: 0;">
   <line id="myline" x1="0" y1="0" x2="200" y2="200"
   style="stroke:rgb(255,0,0);stroke-width:2"/>
 </svg>
    <div style="position: relative">
        <table >
            <tr valign="top">
                <td>
                    <iframe id="leftiframe">
                        <%--                    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" >
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                    </head>
                    <body>
                    </body>
                    </html>
                        --%>
                    </iframe>
                </td>
                <td>
                    Buttons go here<br />
                    <input id="test" type="button" value="Test" />
                </td>
                <td>
                    <iframe id="rightiframe">
                        <%--                    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" >
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                    </head>
                    <body>
                    </body>
                    </html>
                        --%>
                    </iframe>
                </td>
            </tr>
        </table>
        <div id="svgbasics" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;
            overflow: hidden">
        </div>
    </div>
    <div id="leftdiv" style="height: 200px; overflow: auto;">
        Here's some longer text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        <span id="plumbleft">linked</span><br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
    </div>
    <div id="rightdiv" style="height: 200px; overflow: auto">
        Here's some longer text<br />
        Here's some text<br />
        Here's some text<br />
        <span id="plumbright">linked</span><br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
        Here's some text<br />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        
    </style>
    <script src="<%= Url.Content("~/Scripts/Prism-Test.js") %>" type="text/javascript"></script>
<%--    <script src="<%= Url.Content("~/Scripts/jquery.svg/jquery.svg.js") %>" type="text/javascript"></script>
--%>   

<%-- <script src="<%= Url.Content("~/Scripts/wz_jsgraphics.js") %>" type="text/javascript"></script>
--%>

  <script src="<%= Url.Content("~/Scripts/jsDraw2D_Uncompressed.js") %>" type="text/javascript"></script>


    
</asp:Content>
