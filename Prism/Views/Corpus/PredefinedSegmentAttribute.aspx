﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.PredefinedSegmentAttributeModel>" %>

<%@ Import Namespace="Prism.HtmlHelpers" %>
<asp:Content ID="Content4" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="<%= Url.Content("~/Scripts/Prism-PredefinedSegmentAttribute.js") %>"
        type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/jscolor/jscolor.js") %>" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Corpus:
    <%: Model.CorpusName%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="container">
		<div class="page-header">
    		<h2>Edit predefined segment attribute</h2>
		</div>
		<div class="row">
			<div class="span12">
    		<% using (Html.BeginForm(null, null, FormMethod.Post, new { @class = "form-horizontal" }))
	       		{%>
	    		<%: Html.ValidationSummary(false) %>
	    		<fieldset>
					<div class="well">
		        		<legend>Fields</legend>
		        		<%: Html.HiddenFor(model => model.PredefinedSegmentAttribute.ID)%>
		        		<%: Html.HiddenFor(model => model.CorpusName) %>
		        		<%--<div class="editor-label">
		            	</div>--%>
		        		<%--<div class="editor-field">
		                <%: Html.TextBoxFor(model => model.ID) %>
		                <%: Html.ValidationMessageFor(model => model.ID) %>
		            	</div>--%>
						
						<div class="control-group">
							<label class="control-label" for="segment-attr-name">Name</label>
							<div class="controls">
								<%: Html.TextBoxFor(model => model.PredefinedSegmentAttribute.Name, new  { id="segment-attr-name" })%>
								<%: Html.ValidationMessageFor(model => model.PredefinedSegmentAttribute.Name)%>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="segment-attr-type">Attribute type</label>
							<div class="controls">
								<%: Html.EnumDropDownListFor(model => model.PredefinedSegmentAttribute.AttributeType, new { id = "attribtype" })%>
		            			<%: Html.ValidationMessageFor(model => model.PredefinedSegmentAttribute.AttributeType)%>
							</div>
						</div>
		        		
		        		<div class="control-group">
							<label class="control-label" for="segment-attr-show">Show in Contents</label>
		        			<div class="controls">
		            			<%: Html.CheckBoxFor(x => x.PredefinedSegmentAttribute.ShowInTOC) %>
		            			<%: Html.ValidationMessageFor(model => model.PredefinedSegmentAttribute.ShowInTOC)%>
		        			</div>
						</div>
					</div>
				
	        		<div id="attribvallistpanel" class="well">
	            		<fieldset>
	                		<legend>Values</legend>
	                		<table id="valuetable" class="table">
		                    	<thead>
		                        <tr>
		                            <th>
		                                <span class="editor-label">Value</span>
		                            </th>
		                            <th>
		                                <span class="editor-label">Apply colour coding</span>
		                            </th>
		                            <th>
		                                <span class="editor-label">Colour code</span>
		                            </th>
		
		<%--                            <th>
		                                <span class="editor-label">Show segment in TOC</span>
		                            </th>
		                            <th>
		                                <span class="editor-label">Is TOC text</span>
		                            </th>--%>
		
		                            <th>
		                            </th>
		                        </tr>
		                    	</thead>
		                    	<tbody>
		                        	<% if (Model.PredefinedSegmentAttribute.Values != null)
		                               for (int i = 0; i < Model.PredefinedSegmentAttribute.Values.Length; i++)
		                               {
		                                   string rowid = "valuerow-" + i.ToString();
		                                   string rowhtml = Prism.Controllers.CorpusController.PredefinedAttributeValueRowHtml(i, Model.PredefinedSegmentAttribute.Values[i]);
		                        	%>
		                        	<tr id='<%=rowid %>'>
		                            	<%=rowhtml %>
		                        	</tr>
		                        	<% } %>
		                        	<%--                    <%= Html.SegValueRows( Model) %>--%>
		                    	</tbody>
	                		</table>
	                		<input type="button" class="btn" value="Add" id="addvalue" />
	            		</fieldset>
	        		</div>
	    		</fieldset>
				<input type="submit" class="btn btn-primary" value="Save" />
	        	<%: Html.ActionLink("Cancel", "PredefinedSegmentAttributes", "Corpus", new { CorpusName = Model.CorpusName }, new { @class="btn" })%>
	    		<% } %>
	    		<input type="hidden" id="nextvaluerowindex" value="<%= ViewData["nextvaluerowindex"] %>" />
			</div>
		</div>
		<div id="push"></div>
	</div>
</asp:Content>
