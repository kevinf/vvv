﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.SearchModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    VVV - Corpus:
    <%: Model.CorpusName%>
    - Search
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="page-header">
            <h1>
                <%: Model.CorpusName%>
                - Search</h1>
        </div>
        <div class="row">
            <% using (Html.BeginForm(null, null, FormMethod.Post, new { id = "docextractform", @class = "form-horizontal" }))
               { %>
            <fieldset>
                <legend>Base text attribute filter</legend>
                <table id="attribfiltertable" class="table">
                    <thead>
                        <tr>
                            <th>
                                Name
                            </th>
                            <th>
                                Value
                            </th>
                            <th>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
            if (Model.AttributeFilters != null && Model.AttributeFilters.Length > 0)
                for (int i = 0; i < Model.AttributeFilters.Length; i++)
                {
                    string indexval = i.ToString();
                    string rowid = "attribfilterrow-" + indexval;
                    string rowhtml = Prism.Controllers.CorpusController.AttributeRowHtml("AttributeFilters", Model.CorpusName, i, Model.AttributeFilters[i].Name, Model.AttributeFilters[i].Value, false);                                                                                         
											%>
                        <tr id='<%=rowid%>'>
                            <%=rowhtml%>
                        </tr>
                        <% } %>
                    </tbody>
                </table>
                <input type="button" value="Add" id="addfilterattrib" />
            </fieldset>
            <fieldset>
                <legend>Text attribute filter</legend>Base text contains:
                <input type="text" id='basetextfilter' />
                <br />
                Version contains:
                <input type="text" id='versionfilter' />
                <br />
            </fieldset>
            <input type="button" id="search" value="Search" />
        </div>
        <%} %>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
