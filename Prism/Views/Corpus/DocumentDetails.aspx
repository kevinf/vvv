<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.BaseDocumentModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: ViewData["title"] %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="page-header">
            <h2>
                <%: Model.CorpusName %>:
                <%: ViewData["subtitle"] %></h2>
        </div>
        <div class="row">
            <div class="span12">
                <% using (Html.BeginForm())
                   {%>
                <%: Html.ValidationSummary(true) %>
                <%: Html.HiddenFor(x => x.OriginalNameIfVersion) %>
                <%: Html.HiddenFor(x => x.IsVersion) %>
                <fieldset>
                    <div class="well">
                        <legend>Details</legend>
                        <% if (Model.IsVersion)
                           { %>
                        <div class="control-group">
                            <label class="control-label" for="segment-attr-name">
                                Version name</label>
                            <div class="controls">
                                <%: Html.TextBoxFor(model => model.NameIfVersion, new  { id="segment-attr-name" })%>
                                <%: Html.ValidationMessageFor(model => model.NameIfVersion)%>
                            </div>
                        </div>
                        <% } %>
                        <div class="control-group">
                            <label class="control-label" for="segment-attr-description">
                                Description</label>
                            <div class="controls">
                                <%: Html.TextAreaFor(model => model.Description, 5, 90, new  { id="segment-attr-description" })%>
                                <%: Html.ValidationMessageFor(model => model.Description)%>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="segment-attr-information">
                                Information</label>
                            <div class="controls">
                                <%: Html.TextAreaFor(model => model.Information, 5, 90, new  { id="segment-attr-information" })%>
                                <%: Html.ValidationMessageFor(model => model.Information)%>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="segment-attr-author">
                                Author/translator</label>
                            <div class="controls">
                                <%: Html.TextBoxFor(model => model.AuthorTranslator, new  { id="segment-attr-author" })%>
                                <%: Html.ValidationMessageFor(model => model.AuthorTranslator)%>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="segment-attr-genre">
                                Genre</label>
                            <div class="controls">
                                <%: Html.TextBoxFor(model => model.Genre, new  { id="segment-attr-genre" })%>
                                <%: Html.ValidationMessageFor(model => model.Genre)%>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="segment-attr-copy">
                                Copyright information</label>
                            <div class="controls">
                                <%: Html.TextBoxFor(model => model.CopyrightInfo, new  { id="segment-attr-copy" })%>
                                <%: Html.ValidationMessageFor(model => model.CopyrightInfo)%>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="segment-attr-date">
                                Reference date</label>
                            <div class="controls">
                                <%: Html.TextBoxFor(model => model.ReferenceDate, new  { id="segment-attr-date" })%>
                                <%: Html.ValidationMessageFor(model => model.ReferenceDate)%>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="segment-attr-lang">
                                Language</label>
                            <div class="controls">
                                <%: Html.DropDownListFor(model => model.LanguageCode, (System.Collections.Generic.IEnumerable<SelectListItem>) ViewData["langcodes"], new  { id="segment-attr-lang" })%>
                                <%: Html.ValidationMessageFor(model => model.LanguageCode)%>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <input type="submit" value="Save" class="btn btn-primary" />
                <%: Html.ActionLink("Cancel", "Index", new { CorpusName = Model.CorpusName }, new { @class="btn" })%>
                <% } %>
            </div>
        </div>
        <div id="push">
        </div>
    </div>
</asp:Content>
