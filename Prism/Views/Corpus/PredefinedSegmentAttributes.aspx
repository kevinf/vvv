﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.PredefinedSegmentAttributesModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	    Corpus: <%: Model.CorpusName%>  
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="container">
		<div class="page-header">
	    	<h2>Set up predefined segment attributes</h2>
			<p>List of predefined segment attribute names and types</p>
		</div>
		<div class="row">
	    	<table class="table span12">
	        	<tr>
	        		<th>
	        			Name
	        		</th>
	        		<th>
	        			Type
	        		</th>
	        		<th>
	        			Show in Contents
	        		</th>
	        		<th></th>
	        		<th></th>
	        	</tr>
	        
	        	<% if (Model.PredefinedSegmentAttributes != null) foreach (var item in Model.PredefinedSegmentAttributes)
	               { %>
	        	<tr>
	            	<td>
	                	<%: item.Name %>
	            	</td>
	            	<td>
	                	<%: item.AttributeType.ToString() %>
	            	</td>
	            	<td>
	                	<%: item.ShowInTOC.ToString() %>
	            	</td>
	            	<td>
	                	<%: Html.ActionLink("Edit", "PredefinedSegmentAttribute", "Corpus", new { CorpusName = Model.CorpusName, ID = item.ID }, new { @class="btn" } )%>
	            	</td>
	            	<td>
	                	<%: Html.ActionLink("Delete", "DeleteAttribute", new { CorpusName = Model.CorpusName, ID = item.ID }, new { @class="btn btn-danger", onclick = "return confirm('Are you sure you wish to delete this attribute?');" })%>
	            	</td>
	        	</tr>
	        	<% } %>
	        </table>
		</div>
		<div class="row">
			<div class="span12">
		        <%: Html.ActionLink("Add new", "PredefinedSegmentAttribute", new { CorpusName = Model.CorpusName }, new { @class="btn btn-primary" } )%>
				<%: Html.ActionLink("Back to corpus", "Index", "Corpus", new { CorpusName = Model.CorpusName }, new { @class="btn" } )%>
				<%--<fieldset>
			        <legend>Fields</legend>
			        
			        <div class="display-label">CorpusName</div>
			        <div class="display-field"><%: Model.CorpusName %></div>
			    	</fieldset>
				--%>    
				<%--<p>
			        <%: Html.ActionLink("Edit", "Edit", new { /* id=Model.PrimaryKey */ }) %> |
			        <%: Html.ActionLink("Back to List", "Index") %>
			    	</p>
				--%>
			</div>
		</div>
		<div id="push"></div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

