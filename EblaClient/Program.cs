﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EblaImpl;

namespace EblaClient
{
    class Program
    {
        static void Main(string[] args)
        {
            //ServiceReference1.ICorpusStore c = GetCorpusStoreHttp("localhost");
            ServiceReference1.ICorpusStore c = GetCorpusStoreNetTcp("localhost");

            Console.WriteLine(c.Test());

            Console.ReadKey();
        }



        private static void ConfigureHttpBinding(WSHttpBinding binding)
        {
            binding.MaxReceivedMessageSize = 500000;
            binding.SendTimeout = new TimeSpan(0, 10, 0);
        }


        private static void ConfigureNetTcpBinding(NetTcpBinding binding)
        {
            binding.MaxReceivedMessageSize = 500000;
            binding.SendTimeout = new TimeSpan(0, 10, 0);
        }

        private static ServiceReference1.ICorpusStore GetCorpusStoreHttp(string server)
        {
            WSHttpBinding binding = new WSHttpBinding();
            ConfigureHttpBinding(binding);
            EndpointAddress epadd = new EndpointAddress(EblaHelpers.CorpusStoreBaseAddressHttp(server));

            return new ServiceReference1.CorpusStoreClient(binding, epadd);

        }
        private static ServiceReference1.ICorpusStore GetCorpusStoreNetTcp(string server)
        {
            NetTcpBinding binding = new NetTcpBinding();
            ConfigureNetTcpBinding(binding);
            EndpointAddress epadd = new EndpointAddress(EblaHelpers.CorpusStoreBaseAddressNetTcp(server));
            return new ServiceReference1.CorpusStoreClient(binding, epadd);

        }

    }
}
