﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ebla.Tests
{
    /// <summary>
    /// Summary description for CorpusTests
    /// </summary>
    [TestClass]
    public class CorpusTests
    {
        public CorpusTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        [TestInitialize()]
        public void MyTestInitialize()
        {
            Helpers.CreateTestCorpus();
        }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        //[TestMethod]
        //public void UploadOriginal1()
        //{
        //    string path = System.IO.Path.Combine(Helpers.GetTestDataDir(TestContext), Helpers.TestDocuments[0]);

        //    Helpers.UploadOriginalToTestCorpus(path, System.Text.Encoding.UTF7);

        //}

        //[TestMethod]
        //public void UploadOriginal2()
        //{
        //    string path = System.IO.Path.Combine(Helpers.GetTestDataDir(TestContext), Helpers.TestDocuments[1]);

        //    Helpers.UploadOriginalToTestCorpus(path, System.Text.Encoding.UTF7);

        //}

        [TestMethod]
        public void UploadBaseTexts()
        {

            foreach (Helpers.TestDocument t in Helpers.TestDocs)
            {
                string path = System.IO.Path.Combine(Helpers.GetTestDataDir(TestContext), t.Filename);
                Helpers.UploadBaseTextToTestCorpus(path, t.FileEncoding);

            }
        }
    }
}
