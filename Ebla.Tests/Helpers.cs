﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EblaAPI;
using EblaImpl;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ebla.Tests
{
    internal class Helpers
    {
        public static string GetTestUsername()
        {
            return "KevinFlanagan";
        }

        public static string GetTestPassword()
        {
            return "";
        }

        public static string GetTestCorpusName()
        {
            return "TestCorpus";
        }

        internal static ICorpus GetCorpus(string name)
        {
            
            ICorpus corpus = EblaFactory.GetCorpus(ConfigurationManager.AppSettings);

            if (!corpus.Open(name, Helpers.GetTestUsername(), Helpers.GetTestPassword()))
                throw new Exception("You do not have permission to access the corpus.");

            return corpus;
        }

        static public ICorpusStore GetCorpusStore()
        {
            ICorpusStore store = EblaFactory.GetCorpusStore(ConfigurationManager.AppSettings);

            store.Open(Helpers.GetTestUsername(), Helpers.GetTestPassword());

            return store;
        }

        internal static IDocument GetBaseText(string corpusName)
        {
            
            IDocument doc = EblaFactory.GetDocument(ConfigurationManager.AppSettings);

            if (!doc.OpenBaseText(corpusName, Helpers.GetTestUsername(), Helpers.GetTestPassword()))
                throw new Exception("You do not have permission to access the document.");

            return doc;
        }



        static public void CreateTestCorpus()
        {

            ICorpusStore store = EblaFactory.GetCorpusStore(ConfigurationManager.AppSettings);

            
            Assert.IsTrue( store.Open(Helpers.GetTestUsername(), Helpers.GetTestPassword()));

            HashSet<string> corpora = new HashSet<string>(store.GetCorpusList());

            if (corpora.Contains(GetTestCorpusName()))
            {
                store.DeleteCorpus(GetTestCorpusName());
            }

            store.CreateCorpus(GetTestCorpusName(), string.Empty);


        }

        static public string ContentStringFromFile(string filename, System.Text.Encoding encoding)
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(filename, encoding);
            return sr.ReadToEnd();
        }

        static public void UploadBaseTextToTestCorpus(string filename, System.Text.Encoding encoding)
        {
            ICorpus corpus = GetCorpus(GetTestCorpusName());

            string html = ContentStringFromFile(filename, encoding);

            HtmlErrors errors = corpus.UploadBaseText(html);

            if (errors.HtmlParseErrors.Length > 0)
                throw new Exception("Errors were encountered parsing the HTML file.");

        }

        static public void UploadVersionToTestCorpus(string versionName, string filename, System.Text.Encoding encoding)
        {
            ICorpus corpus = GetCorpus(GetTestCorpusName());

            string html = ContentStringFromFile(filename, encoding);

            HtmlErrors errors = corpus.UploadVersion(versionName, html);

            if (errors.HtmlParseErrors.Length > 0)
                throw new Exception("Errors were encountered parsing the HTML file.");

        }


        static public string GetTestDataDir(TestContext testContext)
        {
            return System.IO.Path.Combine(testContext.DeploymentDirectory, @"..\..\..\Ebla.Tests\TestData");

        }

        static public string DocContentToHtmlDoc(string content)
        {
            return "<html><head></head><body>" + content + "</body></html>";

        }


        static public string DocContentToXmlDoc(string content)
        {
            return "<?xml version=\"1.0\" ?><body>" + content + "</body>";

        }

        //public static List<string> TestDocuments = new List<string>() { "gnu2007-de-short.html", "gnu2007-de.html", "gnu2007-en.html","WebElements.htm","StandardDeviation.html" };


        public static List<TestDocument> TestDocs = new List<TestDocument>() 
            { 
                new TestDocument("gnu2007-de-short.html", System.Text.Encoding.UTF7),
                new TestDocument("gnu2007-de.html", System.Text.Encoding.UTF7),
                new TestDocument("gnu2007-en.html", System.Text.Encoding.UTF7),
                new TestDocument("WebElements.htm", System.Text.Encoding.ASCII),
                new TestDocument("I.iii-Shakespeare.html", System.Text.Encoding.ASCII),
                new TestDocument("I.iii-Swaczynna.html", System.Text.Encoding.ASCII),
                new TestDocument("StandardDeviation.html", System.Text.Encoding.UTF8),
                new TestDocument("ZagorskaHeart.htm", System.Text.Encoding.Unicode)
            };

        public class TestDocument
        {
            public TestDocument(string filename, System.Text.Encoding encoding) { Filename = filename; FileEncoding = encoding; }
            public string Filename { get; set; }
            public System.Text.Encoding FileEncoding { get; set; }
        }
    }
}
