﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace DocConverter
{
    class Program
    {
        enum ParseState
        {
            none,
            stageDir,
            speech,
            inlineSD
        }

        static bool StartsWithSDTag(string line)
        {
            line = line.Trim();
            if (line.Length < 2)
                return false;

            return (line.Substring(0, 2).CompareTo("<d") == 0);
        }

        static bool EndsWithSDTag(string line)
        {
            line = line.Trim();
            if (line.Length < 3)
                return false;

            return (line.Substring(line.Length - 3).CompareTo("/d>") == 0);
        }

        static string SegmentMarkup(bool start, int ID, List<string> attribNames, List<string> attribVals)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<span data-eblatype='");
            sb.Append(start ? "startmarker" : "endmarker");
            sb.Append("' data-eblasegid='" + ID.ToString() + "' ");
            if (attribNames != null)
            {
                System.Diagnostics.Debug.Assert(attribNames.Count == attribVals.Count);
                for (int i = 0; i < attribNames.Count; i++)
                {
                    sb.Append("data-userattrib-" + attribNames[i]);
                    sb.Append("='");
                    sb.Append(attribVals[i]);
                    sb.Append("' ");
                }

            }
            sb.Append(" >");
            sb.Append(start ? "[" : "]");
            sb.Append("</span>");

            return sb.ToString();

        }

        static int _nextSegID = 1;

        static void WriteInlineSDs(StreamWriter outfile, List<string> lines, ref int lineCount)
        {
            // Try to detect some more of Tom's markup
            bool exclude = false;
            if (lines.Count > 0)
            {
                string temp = lines[0].Trim().ToLower();
                if (temp.IndexOf("[note") == 0)
                    exclude = true;
                if (temp.IndexOf("note") == 0)
                    exclude = true;
                if (temp.IndexOf("[footnote") == 0)
                    exclude = true;
                if (temp.IndexOf("footnote") == 0)
                    exclude = true;

            }
            outfile.Write("<i>");

            if (!exclude)
                outfile.Write(SegmentMarkup(true, _nextSegID, new List<string>() {"type"}, new List<string>() {"S.D."}));

            for (int i = 0; i < lines.Count; i++)
            {
                WriteOutput(outfile, lines[i].Trim(), lineCount);
                if ((i + 1) != lines.Count)
                {
                    outfile.WriteLine("<br />");
                    lineCount++;
                }
                else
                {
                    outfile.Write(" ");
                }
            }


            if (!exclude)
                outfile.Write(SegmentMarkup(false, _nextSegID, null, null));

            outfile.Write("</i>");

            _nextSegID++;
        }

        static void WriteSDs2(StreamWriter outfile, List<string> lines, ref int lineCount)
        {
            outfile.WriteLine("<blockquote>");
            outfile.WriteLine("<i>");
            outfile.Write(SegmentMarkup(true, _nextSegID, new List<string>() { "type" }, new List<string>() { "S.D." }));

            string firstLine = lines[0];
            // Remove '<d '
            firstLine = firstLine.Substring(2).Trim();
            lines[0] = firstLine;

            string lastLine = lines[lines.Count - 1];
            lastLine = lastLine.Substring(0, lastLine.Length - 3).Trim();
            lines[lines.Count - 1] = lastLine;

            for (int i = 0; i < lines.Count; i++)
            {
                string line = lines[i].Trim();
                if (i < lines.Count - 1)
                    outfile.WriteLine("<br />");
                WriteOutputLine(outfile, line, ref lineCount);
            }

            outfile.Write(SegmentMarkup(false, _nextSegID, null, null));

            _nextSegID++;

            outfile.WriteLine("</i>");
            outfile.WriteLine("</blockquote>");

            //lines.Clear();
        }


        //static void WriteSDs(StreamWriter outfile, List<string> lines, ref int lineCount)
        //{
        //    outfile.WriteLine("<blockquote>");
        //    outfile.WriteLine("<i>");

        //    string firstLine = lines[0];
        //    // Remove '<d '
        //    firstLine = firstLine.Substring(2).Trim();
        //    lines[0] = firstLine;

        //    string lastLine = lines[lines.Count - 1];
        //    lastLine = lastLine.Substring(0, lastLine.Length - 3).Trim();
        //    lines[lines.Count - 1] = lastLine;

        //    for (int i = 0; i < lines.Count; i++)
        //    {
        //        string line = lines[i];
        //        if (i < lines.Count - 1)
        //            outfile.WriteLine("<br />");
        //        WriteOutputLine(outfile, line, ref lineCount);
        //    }

        //    outfile.WriteLine("</i>");
        //    outfile.WriteLine("</blockquote>");

        //    lines.Clear();
        //}

        static void WriteSpeech2(StreamWriter outfile, List<string> lines, ref int lineCount, string speaker, bool initiate, bool terminate)
        {
            
            if (initiate)
                outfile.WriteLine("<blockquote>");
            else
                outfile.Write(" ");

            if (lines.Count == 1 && lines[0].Trim().Length == 0)
            {

            }
            else
            {
                if (lines.Count > 0)
                {
                    outfile.Write(SegmentMarkup(true, _nextSegID, new List<string>() { "type", "speaker" }, new List<string>() { "Speech", speaker }));

                    bool endmarkerwritten = false;
                    for (int i = 0; i < lines.Count; i++)
                    {
                        string line = lines[i].Trim();
                        if (string.IsNullOrEmpty(line))
                        {

                        }
                        else
                        {
                            WriteOutputLine(outfile, line, ref lineCount);

                        }
                         
                        //if (i < lines.Count - 1)

                        if (i == (lines.Count - 1))
                        {
                            endmarkerwritten = true;
                            outfile.Write(SegmentMarkup(false, _nextSegID, null, null));
                        }

                        if (terminate || (i < (lines.Count - 1)))
                            outfile.WriteLine("<br />");

                        //if (!terminate && i == (lines.Count - 1))
                        //    outfile.Write(" ");
                    }

                    System.Diagnostics.Debug.Assert(endmarkerwritten);

                    _nextSegID++;


                }


            }



            if (terminate)
                outfile.WriteLine("</blockquote>");

            //lines.Clear();

        }

        //static void WriteSpeech(StreamWriter outfile, List<string> lines, ref int lineCount)
        //{
        //    outfile.WriteLine("<blockquote>");

        //    for (int i = 0; i < lines.Count; i++)
        //    {
        //        string line = lines[i];
        //        //if (i < lines.Count - 1)
        //        WriteOutputLine(outfile, line, ref lineCount);
        //        outfile.WriteLine("<br />");

        //    }

            
            
        //    outfile.WriteLine("</blockquote>");

        //    lines.Clear();

        //}

        static void WriteOutputLine(StreamWriter outfile, string line, ref int lineCount)
        {
            if ((line.IndexOf("<") > -1) || line.IndexOf(">") > -1)
                throw new Exception("Illegal '<' or '>' in line " + (lineCount + 1).ToString() + ": " + line);

            outfile.WriteLine(line);
            lineCount++;
        }

        static void WriteOutput(StreamWriter outfile, string text, int lineCount)
        {
            if ((text.IndexOf("<") > -1) || text.IndexOf(">") > -1)
                throw new Exception("Illegal '<' or '>' in line " + (lineCount + 1).ToString() + ": " + text);

            outfile.Write(text);

        }

        static void ProcessFile2(StreamWriter outfile, List<string> lines)
        {
            List<string> linebuffer = new List<string>();
            bool seenFirstSD = false;

            outfile.WriteLine("<html>");
            outfile.WriteLine("<head></head>");
            outfile.WriteLine("<body>");

            ParseState state = ParseState.none;
            string speaker = string.Empty;
            int lineCount = 0;
            bool initiates = false;
            int i;
            foreach (string line in lines)
            {

                switch (state)
                {
                    case ParseState.none:
                        if (StartsWithSDTag(line))
                        {
                            seenFirstSD = true;
                            // outside of a speech, s.d. take up whole lines
                            linebuffer.Add(line);
                            if (EndsWithSDTag(line))
                            {
                                WriteSDs2(outfile, linebuffer, ref lineCount);
                                linebuffer.Clear();
                            }
                            else
                                state = ParseState.stageDir;
                        }
                        else
                        {
                            // Don't start collect speeches until we've seen at least one SD.
                            // This is to try to ensure we skip the metadata that may have
                            // been prepended to the text
                            if (seenFirstSD)
                                if (!string.IsNullOrWhiteSpace(line))
                                {
                                    // We expect it to be a speaker name
                                    outfile.WriteLine("<b>");
                                    WriteOutputLine(outfile, line, ref lineCount);
                                    speaker = line;
                                    initiates = true;
                                    outfile.WriteLine("</b>");
                                    state = ParseState.speech;

                                }
                        }
                        break;
                    case ParseState.stageDir:
                        linebuffer.Add(line);
                        if (EndsWithSDTag(line))
                        {
                            WriteSDs2(outfile, linebuffer, ref lineCount);
                            state = ParseState.none;
                            linebuffer.Clear();
                        }

                        break;
                    case ParseState.inlineSD:
                        i = line.IndexOf("/d>");
                        if (i < 0)
                        {
                            linebuffer.Add(line);
                            break;
                        }

                        linebuffer.Add(line.Substring(0, i));
                        WriteInlineSDs(outfile, linebuffer, ref lineCount);
                        linebuffer.Clear();
                        linebuffer.Add(line.Substring(i + 3));
                        state = ParseState.speech;
                        initiates = false;
                        break;

                    case ParseState.speech:
                        if (string.IsNullOrWhiteSpace(line))
                        {
                            WriteSpeech2(outfile, linebuffer, ref lineCount, speaker, initiates, true);
                            linebuffer.Clear();
                            state = ParseState.none;
                        }
                        else
                        {
                            string temp = line;
                            do
                            {
                                i = temp.IndexOf("<d");
                                if (i < 0)
                                {
                                    linebuffer.Add(temp);
                                    break;
                                }
                                linebuffer.Add(temp.Substring(0, i));
                                WriteSpeech2(outfile, linebuffer, ref lineCount, speaker, initiates, false);
                                initiates = false;
                                linebuffer.Clear();
                                temp = temp.Substring(i + 2);
                                i = temp.IndexOf("/d>");
                                if (i < 0)
                                {
                                    state = ParseState.inlineSD;
                                    linebuffer.Add(temp);
                                    break;
                                }
                                WriteInlineSDs(outfile, new List<string>() { temp.Substring(0, i) }, ref lineCount);
                                temp = temp.Substring(i + 3);
                                //linebuffer.Add(temp);

                            }
                            while (true);




                        }
                        break;
                }

            }



            outfile.WriteLine("</body>");
            outfile.WriteLine("</html>");

            outfile.Close();

        }

        //static void ProcessFile(StreamWriter outfile, List<string> lines)
        //{
        //    List<string> linebuffer = new List<string>();
        //    bool seenFirstSD = false;

        //    outfile.WriteLine("<html>");
        //    outfile.WriteLine("<head></head>");
        //    outfile.WriteLine("<body>");

        //    ParseState state = ParseState.none;

        //    int lineCount = 0;
        //    foreach (string line in lines)
        //    {

        //        switch (state)
        //        {
        //            case ParseState.none:
        //                if (StartsWithSDTag(line))
        //                {
        //                    seenFirstSD = true;
        //                    // outside of a speech, s.d. take up whole lines
        //                    linebuffer.Add(line);
        //                    if (EndsWithSDTag(line))
        //                    {
        //                        WriteSDs(outfile, linebuffer, ref lineCount);

        //                    }
        //                    else
        //                        state = ParseState.stageDir;
        //                }
        //                else
        //                {
        //                    // Don't start collect speeches until we've seen at least one SD.
        //                    // This is to try to ensure we skip the metadata that may have
        //                    // been prepended to the text
        //                    if (seenFirstSD)
        //                        if (!string.IsNullOrWhiteSpace(line))
        //                        {
        //                            // We expect it to be a speaker name
        //                            outfile.WriteLine("<b>");
        //                            WriteOutputLine(outfile, line, ref lineCount);
        //                            outfile.WriteLine("</b>");
        //                            state = ParseState.speech;

        //                        }
        //                }
        //                break;
        //            case ParseState.stageDir:
        //                linebuffer.Add(line);
        //                if (EndsWithSDTag(line))
        //                {
        //                    WriteSDs(outfile, linebuffer, ref lineCount);
        //                    state = ParseState.none;
        //                }

        //                break;

        //            case ParseState.speech:
        //                if (string.IsNullOrWhiteSpace(line))
        //                {
        //                    WriteSpeech(outfile, linebuffer, ref lineCount);
        //                    state = ParseState.none;
        //                }
        //                else
        //                {
        //                    string temp = line.Replace("<d ", "[");
        //                    temp = temp.Replace(" /d>", "]");
        //                    temp = temp.Replace("<d", "[");
        //                    temp = temp.Replace("/d> ", "]");
        //                    temp = temp.Replace("[ ", "[");
        //                    temp = temp.Replace(" ]", "]");
        //                    temp = temp.Replace("[(", "[");
        //                    temp = temp.Replace(")]", "]");
        //                    temp = temp.Replace(").]", "]");
        //                    temp = temp.Replace("[[", "[");
        //                    temp = temp.Replace("]]", "]");
        //                    linebuffer.Add(temp);

        //                }
        //                break;
        //        }

        //    }



        //    outfile.WriteLine("</body>");
        //    outfile.WriteLine("</html>");

        //    outfile.Close();

        //}

        static void Main(string[] args)
        {
            Microsoft.Office.Interop.Word.Application ap = null;
            string outfilespec = string.Empty;
            try
            {

                string filespec = string.Empty;

                if (args.Length > 0)
                    filespec = args[0];

                if (Path.GetExtension(filespec).ToLower().CompareTo(".docx") != 0)
                {
                    Console.WriteLine("Please specify a filespec with extension '.docx'");
                    return;
                }

                if (!Path.IsPathRooted(filespec))
                    filespec = Path.Combine(Directory.GetCurrentDirectory(), filespec);

                ap = new Microsoft.Office.Interop.Word.Application();


                foreach (var f in Directory.EnumerateFiles(Path.GetDirectoryName(filespec), Path.GetFileName(filespec)))
                {


                    string txtfilespec = Path.ChangeExtension(f, "txt");

                    Console.WriteLine("Opening document: " + f);
                    Microsoft.Office.Interop.Word.Document d = ap.Documents.Open(f);
                    d.SaveAs(txtfilespec, Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatUnicodeText, Encoding: Microsoft.Office.Core.MsoEncoding.msoEncodingUTF8);
                    d.Close(Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges);

                    Console.WriteLine("Reading input file: " + txtfilespec);

                    List<string> lines = new List<string>(File.ReadAllLines(txtfilespec, System.Text.Encoding.UTF8));

                    Console.WriteLine(lines.Count.ToString() + " lines read.");

                    outfilespec = Path.ChangeExtension(f, "htm");



                    using (StreamWriter outfile = new StreamWriter(outfilespec, false, System.Text.Encoding.UTF8))
                    {
                        ProcessFile2(outfile, lines);
                        outfilespec = string.Empty;
                    }
                   
                }
                if (ap != null)
                    ap.Quit(SaveChanges: Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges);

            }
            catch (Exception ex)
            {
                string errmsg = ex.Message.Replace("\r", "\r\n");
                
                Console.WriteLine("Error: " + errmsg);
                if (ap != null)
                    try
                    {
                        ap.Quit(SaveChanges: Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges);
                    }
                    catch
                    {
                    }

                if (!string.IsNullOrEmpty(outfilespec))
                    try
                    {
                        File.Delete(outfilespec);
                    }
                    catch { }

            }

            Console.WriteLine("Press a key");
            Console.ReadKey();
        }
    }
}
