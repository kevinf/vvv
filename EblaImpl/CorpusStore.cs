﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EblaAPI;
using System.ServiceModel;
using System.IO;
using MySql.Data.MySqlClient;
using System.Collections.Specialized;
using System.Net.Mail;

namespace EblaImpl
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    class CorpusStore : OpenableEblaItem, ICorpusStore
    {
      

        public CorpusStore(System.Collections.Specialized.NameValueCollection configProvider) : base(configProvider)
        {
        }

        public bool Open(string Username, string Password)
        {
            if (!base.Open(Username, Password))
                return false;

            _open = true;

            return true;
        }


        public string[] GetCorpusList()
        {
            CheckOpen();


            List<string> names = new List<string>();

            using (MySqlCommand cmd = new MySqlCommand("SELECT Name FROM corpora ORDER BY Name", GetConnection()))
            {
                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        names.Add(dr.GetString(dr.GetOrdinal("Name")));
                    }

                }

            }

           

            return names.ToArray();
        }



        public void CreateCorpus(string Name, string Description)
        {
            CheckOpen();
            CheckAdmin();
            

            EblaHelpers.IsCorpusNameValid(Name, true);

            using (MySqlTransaction txn = GetConnection().BeginTransaction())
            {
                int corpusID;

                // 'Name' col has unique index - clients should check
                using (MySqlCommand cmd = new MySqlCommand("INSERT INTO corpora (Name, Description) VALUES (@name, @description)", GetConnection()))
                {
                    cmd.Transaction = txn;
                    cmd.Parameters.AddWithValue("name", Name);
                    cmd.Parameters.AddWithValue("description", Description);
                    cmd.ExecuteNonQuery();

                    corpusID = EblaHelpers.GetLastInsertID(cmd);

                    cmd.CommandText = "INSERT INTO documents (CorpusID) VALUES (" + corpusID.ToString() + ")";

                    cmd.ExecuteNonQuery();

                }

                txn.Commit();
                
            }

            
            
        }

        public void DeleteCorpus(string Name)
        {
            CheckOpen();
            CheckAdmin();
            
            using (MySqlCommand cmd = new MySqlCommand("DELETE FROM corpora WHERE Name = @name", GetConnection()))
            {
                // cascade will get rid of everything else
                cmd.Parameters.AddWithValue("name", Name);
                cmd.ExecuteNonQuery();
            }
        }

        public void RenameCorpus(string OldName, string NewName)
        {
            CheckOpen();
            CheckAdmin();

            EblaHelpers.IsCorpusNameValid(NewName, true);


            // 'Name' col has unique index - clients should check
            using (MySqlCommand cmd = new MySqlCommand("UPDATE corpora SET Name = @newname WHERE Name = @oldname", GetConnection()))
            {
                cmd.Parameters.AddWithValue("oldname", OldName);
                cmd.Parameters.AddWithValue("newname", NewName);
                cmd.ExecuteNonQuery();
            }
        }

        public void CreateUser(string Username, string Password, bool CanAdmin, string Email)
        {
            CheckOpen();
            CheckAdmin();

            string salt = EblaHelpers.CreateSalt();
            string hashedpassword = EblaHelpers.CreateHashedPassword(salt, Password);

            
            string sql = "INSERT INTO users (Username, Password, Salt, IsAdmin, UserEmail) VALUES (@username, @password, @salt, @isadmin, @useremail)";

            using (MySqlCommand cmd = new MySqlCommand(sql, GetConnection()))
            {
                cmd.Parameters.AddWithValue("username", Username);
                cmd.Parameters.AddWithValue("password", hashedpassword);
                cmd.Parameters.AddWithValue("salt", salt);
                cmd.Parameters.AddWithValue("isadmin", CanAdmin);
                cmd.Parameters.AddWithValue("useremail", Email);
                cmd.ExecuteNonQuery();

            }

        }

        public void DeleteUser(string Username)
        {
            CheckOpen();
            CheckAdmin();
            

            string sql = "DELETE FROM users WHERE Username = ?username";

            using (MySqlCommand cmd = new MySqlCommand(sql, GetConnection()))
            {
                cmd.Parameters.AddWithValue("username", Username);
                cmd.ExecuteNonQuery();
            }

        }

        public void SetUserCorpusRights(string CorpusName, string Username, bool CanRead, bool CanWrite)
        {
            CheckOpen();
            CheckAdmin();

            string sql = "DELETE FROM userrights WHERE UserID = (SELECT ID FROM users WHERE Username = @username) AND CorpusID = " +
                " (SELECT ID FROM corpora WHERE Name = @corpusname)";

            using (MySqlCommand cmd = new MySqlCommand(sql, GetConnection()))
            {
                cmd.Parameters.AddWithValue("username", Username);
                cmd.Parameters.AddWithValue("corpusname", CorpusName);

                cmd.ExecuteNonQuery();
                sql = "INSERT INTO userrights (UserID, CorpusID, CanRead, CanWrite) VALUES ((SELECT ID FROM users WHERE Username = @username), " +
                        "(SELECT ID FROM corpora WHERE Name = @corpusname), @canread, @canwrite)";

                cmd.CommandText = sql;
                cmd.Parameters.AddWithValue("canread", CanRead);
                cmd.Parameters.AddWithValue("canwrite", CanWrite);

                cmd.ExecuteNonQuery();
            }

            
        }

        public void UpdateUser(string Username, bool CanAdmin, string Email)
        {
            CheckOpen();
            CheckAdmin();

            string sql = "UPDATE users SET IsAdmin = @isadmin, UserEmail = @useremail WHERE Username = @username";


            using (MySqlCommand cmd = new MySqlCommand(sql, GetConnection()))
            {
                cmd.Parameters.AddWithValue("username", Username);
                cmd.Parameters.AddWithValue("isadmin", CanAdmin);
                cmd.Parameters.AddWithValue("useremail", Email);

                cmd.ExecuteNonQuery();
            }
        }

        public void ChangePassword(string NewPassword)
        {
            CheckOpen();
            

            string salt = EblaHelpers.CreateSalt();
            string hashedpassword = EblaHelpers.CreateHashedPassword(salt, NewPassword);

            string sql = "UPDATE users SET Password = @password, Salt = @salt WHERE Username = @username";
            using (MySqlCommand cmd = new MySqlCommand(sql, GetConnection()))
            {
                cmd.Parameters.AddWithValue("username", base.Username());
                cmd.Parameters.AddWithValue("password", hashedpassword);
                cmd.Parameters.AddWithValue("salt", salt);
                cmd.ExecuteNonQuery();

            }

        }

        public void ResetPassword(string Username, string UserEmail)
        {
            string sql = "SELECT Username from users WHERE UserEmail = @useremail and username = @username";

            using (MySqlCommand cmd = new MySqlCommand(sql, GetConnection()))
            {
                cmd.Parameters.AddWithValue("useremail", UserEmail);
                cmd.Parameters.AddWithValue("username", Username);

                object o = cmd.ExecuteScalar();

                if (o == null || o == DBNull.Value)
                {
                    throw new Exception("The user name and email address combination was not found.");

                }

                string username = o.ToString();

                string salt = EblaHelpers.CreateSalt();
                string password = EblaHelpers.GeneratePassword();
                string hashedpassword = EblaHelpers.CreateHashedPassword(salt, password);

                sql = "UPDATE users SET Password = @password, Salt = @salt WHERE Username = @username";
                cmd.CommandText = sql;
                cmd.Parameters.AddWithValue("password", hashedpassword);
                cmd.Parameters.AddWithValue("salt", salt);
                cmd.ExecuteNonQuery();


                try
                {
                    MailMessage mailMessage = new MailMessage();

                    mailMessage.From = new MailAddress(
                        ConfigProvider["emailFromAddress"],
                        ConfigProvider["emailFromName"]
                    );
                    mailMessage.To.Add(UserEmail);
                    mailMessage.Subject = "VVV password reset";
                    mailMessage.IsBodyHtml = false;
                    mailMessage.Body = string.Format(@"You have been sent this email because a request was made to reset your password for VVV.

Username: {0}

Your password has been changed to: {1}

It is advisable to log in to VVV with the above password and change the password as soon as possible.

N.B. Please do not reply to this email as this is an automated response and your reply will not be received.", Username, password);

                    SmtpClient smtpClient = new SmtpClient(
                        ConfigProvider["emailHost"]
                    );

                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = new System.Net.NetworkCredential(
                        ConfigProvider["emailUsername"],
                        ConfigProvider["emailPassword"]
                    );

                    smtpClient.Send(mailMessage);

                }
                catch (Exception ex)
                {
                    // TODO -log
                    throw new Exception("Failed sending email.");
                }

            }


        }

        public UserInfo[] GetUserInfo(string UsernameFilter, string CorpusFilter)
        {
            CheckOpen();
            return GetUserInfo(UsernameFilter, CorpusFilter, GetConnection());
        }

        internal static UserInfo[] GetUserInfo(string UsernameFilter, string CorpusFilter, MySqlConnection cn)
        {
            string sql = "SELECT Username, users.ID as UserId, IsAdmin, UserEmail, corpora.Name, CanRead, CanWrite FROM users LEFT OUTER JOIN userrights ON users.ID = userrights.UserID " +
                    "LEFT OUTER JOIN corpora ON userrights.CorpusID = corpora.ID";


            var userInfo = new List<UserInfo>();

            using (MySqlCommand cmd = new MySqlCommand("", cn))
            {

                string filter = string.Empty;

                if (!string.IsNullOrEmpty(CorpusFilter))
                {
                    filter += " corpora.Name = @corpusname";
                    cmd.Parameters.AddWithValue("corpusname", CorpusFilter);
                }

                if (!string.IsNullOrEmpty(UsernameFilter))
                {
                    if (filter.Length > 0)
                        filter += " AND ";

                    filter += " users.Username = @username";
                    cmd.Parameters.AddWithValue("username", UsernameFilter);
                }

                if (filter.Length > 0)
                {
                    filter = " WHERE " + filter;
                }

                sql += filter;

                cmd.CommandText = sql;
                UserInfo lastui = null;
                int lastUserId = -1;
                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        //var ui = new UserInfo();

                        int usernameOrdinal = dr.GetOrdinal("Username");
                        int useridOrdinal = dr.GetOrdinal("UserID");
                        int corpusOrdinal = dr.GetOrdinal("Name");
                        int canreadOrdinal = dr.GetOrdinal("CanRead");
                        int canwriteOrdinal = dr.GetOrdinal("CanWrite");
                        int isadminOrdinal = dr.GetOrdinal("IsAdmin");
                        int emailOrdinal = dr.GetOrdinal("UserEmail");

                        int userId = dr.GetInt32(useridOrdinal);

                        if (userId != lastUserId)
                        {
                            lastui = new UserInfo();
                            lastui.Username = dr.GetString(usernameOrdinal);
                            lastui.IsAdmin = dr.GetBoolean(isadminOrdinal);
                            lastui.Email = dr.IsDBNull(emailOrdinal) ? string.Empty : dr.GetString(emailOrdinal);
                            lastui.CorpusRights = new CorpusRights[0];
                            lastUserId = userId;
                            userInfo.Add(lastui);
                        }

                        if (!dr.IsDBNull(corpusOrdinal))
                        {
                            var rightslist = new List<CorpusRights>(lastui.CorpusRights);
                            var rights = new CorpusRights();

                            rights.CorpusName = dr.GetString(corpusOrdinal);
                            rights.CanRead = dr.GetBoolean(canreadOrdinal);
                            rights.CanWrite = dr.GetBoolean(canwriteOrdinal);

                            rightslist.Add(rights);
                            lastui.CorpusRights = rightslist.ToArray();
                        }

                    }

                }
            }

            return userInfo.ToArray();
        }
    }
}
