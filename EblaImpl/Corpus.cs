/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EblaAPI;
using MySql.Data.MySqlClient;
using HtmlAgilityPack;
using System.Xml;


namespace EblaImpl
{
    class Corpus : OpenableEblaItem, ICorpus
    {
        #region "ICorpus"

        public PredefinedSegmentAttribute[] GetPredefinedSegmentAttributes()
        {
            CheckOpen();
            CheckCanRead(_name);

            string sql = "SELECT ID, Name, AttributeType, Value, ApplyColouring, ColourCode, ShowInTOC FROM predefinedsegmentattributes LEFT OUTER JOIN predefinedsegmentattributevalues ON predefinedsegmentattributes.ID = predefinedsegmentattributevalues.PredefinedSegmentAttributeID WHERE CorpusID = " + _id.ToString() + " ORDER BY predefinedsegmentattributes.ID";

            List<PredefinedSegmentAttribute> l = ReadPrefinedSegAttribs(sql);

            return l.ToArray();
        }

        public void CreatePredefinedSegmentAttribute(PredefinedSegmentAttribute PredefinedSegmentAttribute)
        {
            CheckOpen();
            CheckCanWrite(_name);

            if (PredefinedSegmentAttribute.AttributeType != PredefinedSegmentAttributeType.List || PredefinedSegmentAttribute.Values == null)
                PredefinedSegmentAttribute.Values = new PredefinedSegmentAttributeValue[0];

            using (MySqlTransaction txn = GetConnection().BeginTransaction())
            {
                string sql = "INSERT INTO predefinedsegmentattributes (Name, AttributeType, CorpusID, ShowInTOC) VALUES (@name, " + ((int)PredefinedSegmentAttribute.AttributeType).ToString() + ", " + _id.ToString() + ", @showintoc)";
                using (MySqlCommand cmd = new MySqlCommand(sql, GetConnection()))
                {
                    cmd.Transaction = txn;
                    cmd.Parameters.AddWithValue("name", PredefinedSegmentAttribute.Name);
                    cmd.Parameters.AddWithValue("showintoc", PredefinedSegmentAttribute.ShowInTOC);

                    cmd.ExecuteNonQuery();

                    PredefinedSegmentAttribute.ID = EblaHelpers.GetLastInsertID(cmd);

                    sql = "INSERT INTO predefinedsegmentattributevalues (PredefinedSegmentAttributeID, Value, ApplyColouring, ColourCode) VALUES (" + PredefinedSegmentAttribute.ID + ", @value, @applycolouring, @colourcode)";
                    cmd.CommandText = sql;
                    cmd.Parameters.Clear();
                    MySqlParameter valueparm = cmd.Parameters.AddWithValue("value", string.Empty);
                    MySqlParameter colouringparm = cmd.Parameters.AddWithValue("applycolouring", false);
                    MySqlParameter colourcodeparm = cmd.Parameters.AddWithValue("colourcode", string.Empty);
                    //MySqlParameter showintocparm = cmd.Parameters.AddWithValue("showintoc", false);
                    //MySqlParameter toctextparm = cmd.Parameters.AddWithValue("toctext", false);
                    foreach (PredefinedSegmentAttributeValue v in PredefinedSegmentAttribute.Values)
                    {
                        valueparm.Value = v.Value;
                        colouringparm.Value = v.ApplyColouring;
                        colourcodeparm.Value = v.ColourCode;
                        //showintocparm.Value = v.ShowInTOC;
                        //toctextparm.Value = v.IsTOCText;
                        cmd.ExecuteNonQuery();
                    }

                }


                txn.Commit();
            }
        }


        public void DeletePredefinedSegmentAttribute(int ID)
        {
            CheckOpen();
            CheckCanWrite(_name);

            string sql = "DELETE FROM predefinedsegmentattributes WHERE ID = " + ID.ToString() + " AND CorpusID = " + _id;

            using (MySqlCommand cmd = new MySqlCommand(sql, GetConnection()))
            {
                cmd.ExecuteNonQuery();
            }
        }

        public void UpdatePredefinedSegmentAttribute(PredefinedSegmentAttribute PredefinedSegmentAttribute)
        {
            CheckOpen();
            CheckCanWrite(_name);

            PredefinedSegmentAttribute current = GetPredefinedSegmentAttribute(PredefinedSegmentAttribute.ID);

            if (current == null)
                throw new Exception("PredefinedSegmentAttribute with ID " + PredefinedSegmentAttribute.ID.ToString() + " not found.");

            if (PredefinedSegmentAttribute.AttributeType != PredefinedSegmentAttributeType.List || PredefinedSegmentAttribute.Values == null)
                PredefinedSegmentAttribute.Values = new PredefinedSegmentAttributeValue[0];

            List<PredefinedSegmentAttributeValue> valuesToDelete = new List<PredefinedSegmentAttributeValue>();
            List<PredefinedSegmentAttributeValue> valuesToInsert = new List<PredefinedSegmentAttributeValue>();
            List<PredefinedSegmentAttributeValue> valuesToUpdate = new List<PredefinedSegmentAttributeValue>();

            foreach (var v in current.Values)
            {
                bool found = false;
                foreach (var v2 in PredefinedSegmentAttribute.Values)
                {
                    if (string.Compare(v.Value, v2.Value) == 0)
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    valuesToDelete.Add(v);

            }

            foreach (var v in PredefinedSegmentAttribute.Values)
            {
                bool found = false;
                foreach (var v2 in current.Values)
                {
                    if (string.Compare(v.Value, v2.Value) == 0)
                    {
                        found = true;
                        valuesToUpdate.Add(v);
                        break;
                    }
                }
                if (!found)
                    valuesToInsert.Add(v);
            }

            string sql = "UPDATE predefinedsegmentattributes SET Name = @name, ShowInTOC = @showintoc, AttributeType = " + ((int)PredefinedSegmentAttribute.AttributeType).ToString() + " WHERE ID = " + PredefinedSegmentAttribute.ID.ToString();
            using (MySqlTransaction txn = GetConnection().BeginTransaction())
            {
                using (MySqlCommand cmd = new MySqlCommand(sql, GetConnection()))
                {
                    cmd.Transaction = txn;
                    cmd.Parameters.AddWithValue("name", PredefinedSegmentAttribute.Name);
                    cmd.Parameters.AddWithValue("showintoc", PredefinedSegmentAttribute.ShowInTOC);

                    cmd.ExecuteNonQuery();


                    sql = "DELETE FROM predefinedsegmentattributevalues WHERE PredefinedSegmentAttributeID = " + PredefinedSegmentAttribute.ID.ToString() + " AND Value = @value";
                    cmd.Parameters.Clear();
                    MySqlParameter valueparm = cmd.Parameters.AddWithValue("value", string.Empty);
                    cmd.CommandText = sql;
                    foreach (var v in valuesToDelete)
                    {
                        valueparm.Value = v.Value;
                        cmd.ExecuteNonQuery();
                    }

                    sql = "INSERT INTO predefinedsegmentattributevalues (PredefinedSegmentAttributeID, Value, ApplyColouring, ColourCode) VALUES (" + PredefinedSegmentAttribute.ID + ", @value, @applycolouring, @colourcode)";
                    cmd.CommandText = sql;
                    MySqlParameter colouringparm = cmd.Parameters.AddWithValue("applycolouring", false);
                    MySqlParameter colourcodeparm = cmd.Parameters.AddWithValue("colourcode", string.Empty);
                    //MySqlParameter showintocparm = cmd.Parameters.AddWithValue("showintoc", false);
                    //MySqlParameter toctextparm = cmd.Parameters.AddWithValue("toctext", false);
                    foreach (var v in valuesToInsert)
                    {
                        valueparm.Value = v.Value;
                        colouringparm.Value = v.ApplyColouring;
                        colourcodeparm.Value = v.ColourCode;
                        //showintocparm.Value = v.ShowInTOC;
                        //toctextparm.Value = v.IsTOCText;
                        cmd.ExecuteNonQuery();
                    }

                    sql = "UPDATE predefinedsegmentattributevalues SET ApplyColouring = @applycolouring, ColourCode = @colourcode WHERE PredefinedSegmentAttributeID = " + PredefinedSegmentAttribute.ID.ToString() + " AND Value = @value";
                    cmd.CommandText = sql;
                    foreach (var v in valuesToUpdate)
                    {
                        valueparm.Value = v.Value;
                        colouringparm.Value = v.ApplyColouring;
                        colourcodeparm.Value = v.ColourCode;
                        //showintocparm.Value = v.ShowInTOC;
                        //toctextparm.Value = v.IsTOCText;
                        cmd.ExecuteNonQuery();

                    }
                }

                txn.Commit();
            }

        }

        public PredefinedSegmentAttribute GetPredefinedSegmentAttribute(int ID)
        {
            CheckOpen();
            CheckCanRead(_name);

            string sql = "SELECT ID, Name, AttributeType, Value, ApplyColouring, ColourCode, ShowInTOC FROM predefinedsegmentattributes LEFT OUTER JOIN predefinedsegmentattributevalues ON predefinedsegmentattributes.ID = predefinedsegmentattributevalues.PredefinedSegmentAttributeID WHERE CorpusID = " + _id.ToString() + " AND predefinedsegmentattributes.ID = " + ID.ToString();

            List<PredefinedSegmentAttribute> l = ReadPrefinedSegAttribs(sql);


            if (l.Count == 0)
                return null;

            System.Diagnostics.Debug.Assert(l.Count == 1);
            return l[0];

        }

        public SegmentDefinition[] Search(string BaseTextFilter, string VersionFilter, SegmentAttribute[] BaseTextAttributeFilters)
        {
            if (string.IsNullOrWhiteSpace(BaseTextFilter))
                throw new Exception("A base text filter must be specified for searching.");

            Document baseText = new Document(ConfigProvider);
            baseText.OpenWithoutPrivilegeCheck(_name, string.Empty, true, GetConnection());

            // First make a list of all base text occurrences



            throw new NotImplementedException();
        }

        public bool Open(string Name, string Username, string Password)
        {
            if (!base.Open(Username, Password))
                return false;

            using (MySqlCommand cmd = new MySqlCommand("SELECT ID, Description FROM corpora WHERE Name = @name", GetConnection()))
            {
                cmd.Parameters.AddWithValue("name", Name);
                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    if (!dr.Read())
                        throw new Exception("Corpus not found: " + Name);

                    _id = dr.GetInt32(dr.GetOrdinal("ID"));
                    int descOrdinal = dr.GetOrdinal("Description");
                    if (!dr.IsDBNull(descOrdinal))
                        _description = dr.GetString(descOrdinal);

                }

                cmd.CommandText = "SELECT ID FROM documents WHERE CorpusID = " + _id.ToString() + " AND NAME IS NULL";

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    if (!dr.Read())
                        throw new Exception("Corpus base text not found: " + Name);

                    _basetextid = dr.GetInt32(dr.GetOrdinal("ID"));

                }

            }

            _open = true;
            _name = Name;


            CheckCanRead(_name);

            return true;
        }


        public HtmlErrors UploadBaseText(string BaseTextHtml)
        {
            return UploadOrUpdateBaseText(BaseTextHtml, false);

        }

        protected HtmlErrors UploadOrUpdateBaseText(string BaseTextHtml, bool updating)
        {
            CheckOpen();
            CheckCanWrite(_name);

            return UploadDocument(_basetextid, BaseTextHtml, updating, null);

        }

        public string GetDescription()
        {
            return _description;
        }

        public void SetDescription(string Description)
        {
            throw new NotImplementedException();
        }

        public void CreateVersion(string Name, DocumentMetadata meta)
        {
            CheckOpen();
            CheckCanWrite(_name);

            EblaHelpers.IsVersionNameValid(Name, true);

            using (MySqlCommand cmd = new MySqlCommand("INSERT INTO documents (CorpusID, Name, Description, Information, AuthorTranslator, Genre, CopyrightInfo, ReferenceDate, LanguageCode) VALUES (@corpusid, @name, @description, @information, @authortranslator, @genre, @copyrightinfo, @referencedate, @langcode)", GetConnection()))
            {
                cmd.Parameters.AddWithValue("corpusid", _id);
                cmd.Parameters.AddWithValue("name", Name);
                cmd.Parameters.AddWithValue("description", meta.Description);
                cmd.Parameters.AddWithValue("information", meta.Information);
                cmd.Parameters.AddWithValue("authortranslator", meta.AuthorTranslator);
                cmd.Parameters.AddWithValue("genre", meta.Genre);
                cmd.Parameters.AddWithValue("copyrightinfo", meta.CopyrightInfo);
                if (meta.ReferenceDate.HasValue)
                    cmd.Parameters.AddWithValue("referencedate", meta.ReferenceDate.Value);
                else
                    cmd.Parameters.AddWithValue("referencedate", DBNull.Value);

                cmd.Parameters.AddWithValue("langcode", meta.LanguageCode);

                cmd.ExecuteNonQuery();
            }

        }

        public void DeleteVersion(string Name)
        {
            CheckOpen();
            CheckCanWrite(_name);

            EblaHelpers.IsVersionNameValid(Name, true);

            using (MySqlCommand cmd = new MySqlCommand("DELETE FROM documents WHERE CorpusID = @corpusid AND Name = @name", GetConnection()))
            {
                cmd.Parameters.AddWithValue("corpusid", _id);
                cmd.Parameters.AddWithValue("name", Name);

                cmd.ExecuteNonQuery();
            }

        }

        public void RenameVersion(string OldName, string NewName)
        {
            CheckOpen();
            CheckCanWrite(_name);

            EblaHelpers.IsVersionNameValid(NewName, true);

            using (MySqlCommand cmd = new MySqlCommand("UPDATE documents SET Name = @newname WHERE Name = @oldname AND CorpusID = @corpusid", GetConnection()))
            {
                cmd.Parameters.AddWithValue("corpusid", _id);
                cmd.Parameters.AddWithValue("oldname", OldName);
                cmd.Parameters.AddWithValue("newname", NewName);

                cmd.ExecuteNonQuery();

            }

        }

        public HtmlErrors UpdateVersion(string Name, string VersionHtml)
        {
            return UploadOrUpdateVersion(Name, VersionHtml, true);

        }

        public HtmlErrors UpdateBaseText(string VersionHtml)
        {
            return UploadOrUpdateBaseText(VersionHtml, true);

        }

        public HtmlErrors UploadVersion(string Name, string VersionHtml)
        {
            return UploadOrUpdateVersion(Name, VersionHtml, false);
        }

        public HtmlErrors UploadOrUpdateVersion(string Name, string VersionHtml, bool updating)
        {
            CheckOpen();
            CheckCanWrite(_name);

            EblaHelpers.IsVersionNameValid(Name, true);

            int versionid;
            using (MySqlCommand cmd = new MySqlCommand("SELECT ID FROM documents WHERE Name = @name AND CorpusID = @corpusid", GetConnection()))
            {
                cmd.Parameters.AddWithValue("corpusid", _id);
                cmd.Parameters.AddWithValue("name", Name);

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    if (!dr.Read())
                        throw new Exception("Version not found: " + Name);

                    versionid = dr.GetInt32(dr.GetOrdinal("ID"));

                }


            }

            return UploadDocument(versionid, VersionHtml, updating, Name);


        }

        public string[] GetVersionList()
        {
            CheckOpen();
            CheckCanRead(_name);

            List<string> versions = new List<string>();

            using (MySqlCommand cmd = new MySqlCommand("SELECT Name FROM documents WHERE Name IS NOT NULL AND CorpusID = @corpusid", GetConnection()))
            {
                cmd.Parameters.AddWithValue("corpusid", _id);

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        versions.Add(dr.GetString(dr.GetOrdinal("Name")));
                    }
                }

            }

            return versions.ToArray();
        }

        public void StartSegmentVariationCalculation(VariationMetricTypes metricType)
        {
            CheckOpen();
            CheckCanWrite(_name);
            _metricType = metricType;

            // Switch to background connection mode - hold/share a cxn
            SetConnection(EblaHelpers.GetConnection(ConfigProvider));
            _docCache = new Dictionary<string, Document>();
            _baseTextDoc = new Document(ConfigProvider);
            _baseTextDoc.OpenWithoutPrivilegeCheck(_name, null, true, GetConnection());

            _segs = _baseTextDoc.FindSegmentDefinitions(0, _baseTextDoc.Length(), false, false, null);

            // Delete any old calculated values from the corpus
            string sql = "DELETE FROM segmentattributes WHERE Name LIKE @eddyattribname OR Name LIKE @vivattribname AND SegmentDefinitionID IN " +
                "(SELECT segmentdefinitions.ID FROM segmentdefinitions INNER JOIN documents ON segmentdefinitions.DocumentID = documents.ID WHERE documents.CorpusID = " + _id.ToString() + ")";

            using (var cmd = new MySqlCommand(sql, GetConnection()))
            {
                cmd.Parameters.AddWithValue("eddyattribname", EddyAttribName(metricType));
                cmd.Parameters.AddWithValue("vivattribname", VivAttribName(metricType));

                cmd.ExecuteNonQuery();
            }


        }

        public SegmentVariationData RetrieveSegmentVariationData(int BaseTextSegmentID, VariationMetricTypes metricType)
        {
            var result = new SegmentVariationData();
            result.BaseTextSegmentID = BaseTextSegmentID;

            result.VersionNames = GetVersionList();
            result.EddyValues = new double[result.VersionNames.Length];

            string eddyAttribName = EddyAttribName(metricType);

            for (int i = 0; i < result.VersionNames.Length; i++)
            {
                string version = result.VersionNames[i];
                result.EddyValues[i] = -1;

                int docid = GetDocID(version);

                var alignmentSet = new AlignmentSet(ConfigProvider);
                alignmentSet.OpenWithoutPrivilegeCheck(_name, version, true, null);

                var alignment = alignmentSet.FindAlignment(BaseTextSegmentID, true);

                if (alignment != null)
                {

                    foreach (int segid in alignment.SegmentIDsInVersion)
                    {
                        SegmentDefinition seg = Document.GetSegmentDefinition(segid, docid, GetConnection(), true);

                        bool gotEddyAttrib = false;
                        foreach (var a in seg.Attributes)
                        {
                            if (string.Compare(a.Name, eddyAttribName) == 0)
                            {
                                double v = -1;
                                bool b = double.TryParse(a.Value, out v);
                                result.EddyValues[i] = v;
                                gotEddyAttrib = true;
                                break;
                            }

                        }
                        if (gotEddyAttrib)
                            break;
                        
                    } // foreach (version seg in alignment)


                }
            }

            return result;
        }

        class VariationEntry
        {
            public double eddyVal;
            public int versionSegID;
        }

        public MultipleSegmentVariationData RetrieveMultipleSegmentVariationData(SegmentAttribute[] BaseTextAttributeFilters, VariationMetricTypes metricType)
        {
            var result = new MultipleSegmentVariationData();
            var vernames = new List<string>(GetVersionList());
            vernames.Sort();

            result.VersionNames = vernames.ToArray();

            //result.VersionNames = new string[] {"Bab and Levy", "Zaimoglu" };
            result.EddyValues = new double[result.VersionNames.Length][];

            Document baseText = new Document(ConfigProvider);
            string eddyAttribName = EddyAttribName(metricType);

            baseText.OpenWithoutPrivilegeCheck(_name, null, true, null);

            // Get a list of base text segments matching the filter conditions
            var segdefs = baseText.FindSegmentDefinitions(0, baseText.Length(), false, false, BaseTextAttributeFilters);

            // Sort them in order of position in the text
            var sortedSegDefs = new List<SegmentDefinition>(segdefs);
            sortedSegDefs.Sort((a, b) => (a.StartPosition.CompareTo(b.StartPosition)));
            segdefs = sortedSegDefs.ToArray();            

            // Now get the eddy values for all version segments aligned to them.
            // Note - this reads all alignments. The values in BaseTextAttributeFilters (if any) could be incorporated in the SQL. But it's pretty fast like this.
            string sql = "SELECT BaseTextSegmentID, VersionSegmentID, Value, documents.Name FROM segmentdefinitions " +
                        "inner join segmentalignments on segmentalignments.BaseTextSegmentID = segmentdefinitions.id " +
                        "inner join segmentdefinitions as versionsegmentdefinitions on versionsegmentdefinitions.ID = VersionSegmentID " +
                        "inner join segmentattributes on segmentattributes.SegmentDefinitionID=versionsegmentid " +
                        "inner join documents on documents.ID = versionsegmentdefinitions.DocumentID " +
                        "where segmentdefinitions.DocumentID = " + _basetextid.ToString()  +
                        " and segmentattributes.Name = '" + eddyAttribName + "'";

            var versionsMap = new Dictionary<string, Dictionary<int, VariationEntry>>();
            foreach (string s in result.VersionNames)
                versionsMap.Add(s, new Dictionary<int, VariationEntry>());

            using (MySqlCommand cmd = new MySqlCommand(sql, GetConnection()))
            {
                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        int baseTextSegmentID = dr.GetInt32("BaseTextSegmentID");
                        int versionSegmentID = dr.GetInt32("VersionSegmentID");
                        string temp = dr.GetString("Value");
                        string version = dr.GetString("Name");

                        double eddyVal = 0;
                        double.TryParse(temp, out eddyVal); // TODO - report any error

                        var eddyMap = versionsMap[version];
                        // Seen this segment ID already?
                        if (eddyMap.ContainsKey(baseTextSegmentID))
                        {
                            // Yes - was it with the same versionSegID?
                            if (eddyMap[baseTextSegmentID].versionSegID == versionSegmentID)
                                // Yes - this must be a many-to-one alignment. We can't represent
                                // those yet. Mark as not usable
                                eddyMap[baseTextSegmentID].eddyVal = -1;

                            // Otherwise, it's a 1-to-many, and the eddyVal will be the same,
                            // so just ignore it
                        }
                        else
                        {
                            var ve = new VariationEntry();
                            ve.versionSegID = versionSegmentID;
                            ve.eddyVal = eddyVal;
                            eddyMap[baseTextSegmentID] = ve;
                        }
                    }
                }
  
                
            }

            result.BaseTextSegmentIDs = new int[segdefs.Length];

            for (int i = 0; i < segdefs.Length; i++)
                result.BaseTextSegmentIDs[i] = segdefs[i].ID;


            for (int i = 0; i < result.VersionNames.Length; i++)
            {
                string version = result.VersionNames[i];

                var eddyMap = versionsMap[version];
                result.EddyValues[i] = new double[segdefs.Length];

                for (int j = 0; j < segdefs.Length; j++)
                {
                    result.EddyValues[i][j] = -1;
                    if (eddyMap.ContainsKey(segdefs[j].ID))
                    {
                        if (eddyMap[segdefs[j].ID].eddyVal != -1)
                            result.EddyValues[i][j] = eddyMap[segdefs[j].ID].eddyVal;
                    }
                    
                }

            }

            return result;
        }

        //public MultipleSegmentVariationData _RetrieveMultipleSegmentVariationData(SegmentAttribute[] BaseTextAttributeFilters, VariationMetricTypes metricType)
        //{
        //    var result = new MultipleSegmentVariationData();
        //    result.VersionNames = GetVersionList();

        //    //result.VersionNames = new string[] {"Bab and Levy", "Zaimoglu" };
        //    result.EddyValues = new double[result.VersionNames.Length][];

        //    Document baseText = new Document(ConfigProvider);
        //    string eddyAttribName = EddyAttribName(metricType);



        //    baseText.OpenWithoutPrivilegeCheck(_name, null, true, null);

        //    var segdefs = baseText.FindSegmentDefinitions(0, baseText.Length(), false, false, BaseTextAttributeFilters);

        //    var sortedSegDefs = new List<SegmentDefinition>(segdefs);
        //    sortedSegDefs.Sort( (a, b)=> (a.StartPosition.CompareTo(b.StartPosition)) );
        //    segdefs = sortedSegDefs.ToArray();

        //    result.BaseTextSegmentIDs = new int[segdefs.Length];

        //    for (int i = 0; i < segdefs.Length; i++)
        //        result.BaseTextSegmentIDs[i] = segdefs[i].ID;


        //    for (int i = 0; i < result.VersionNames.Length; i++)
        //    {
        //        string version = result.VersionNames[i];
        //        result.EddyValues[i] = new double[segdefs.Length];

        //        int docid = GetDocID(version);

        //        var alignmentSet = new AlignmentSet(ConfigProvider);
        //        alignmentSet.OpenWithoutPrivilegeCheck(_name, version, true, null);

        //        for (int j = 0; j < segdefs.Length; j++)
        //        {
        //            result.EddyValues[i][j] = -1;

        //            var alignment = alignmentSet.FindAlignment(segdefs[j].ID, true);

        //            if (alignment != null)
        //            {

        //                foreach (int segid in alignment.SegmentIDsInVersion)
        //                {
        //                    SegmentDefinition seg = Document.GetSegmentDefinition(segid, docid, GetConnection(), true);

        //                    bool gotEddyAttrib = false;
        //                    foreach (var a in seg.Attributes)
        //                    {
        //                        if (string.Compare(a.Name, eddyAttribName) == 0)
        //                        {
        //                            double v = -1;
        //                            bool b = double.TryParse(a.Value, out v);
        //                            result.EddyValues[i][j] = v;
        //                            gotEddyAttrib = true;
        //                            break;
        //                        }

        //                    }
        //                    if (gotEddyAttrib)
        //                        break;

        //                } // foreach (version seg in alignment)


        //            }

        //        }

        //    }

        //    return result;

        //}

        public SegmentVariation RetrieveSegmentVariation(int BaseTextSegmentID, VariationMetricTypes metricType)
        {
            Document baseTextDoc = new Document(ConfigProvider);
            baseTextDoc.OpenWithoutPrivilegeCheck(_name, null, true, null);
            SegmentDefinition d = baseTextDoc.GetSegmentDefinition(BaseTextSegmentID);

            if (d.Length == 0)
                //throw new Exception("Unable to calculate variation for null alignments.");
                return null;

            return RetrieveSegmentVariation(baseTextDoc, d, metricType);

        }

        public double RetrieveAverageVariation(string VersionName, VariationMetricTypes metricType)
        {
            int id = GetDocID(VersionName);

            string sql = "select avg(segmentattributes.value - 0.0) from segmentattributes " +
                    " inner join segmentdefinitions on segmentattributes.segmentdefinitionid = segmentdefinitions.id " +
                    " where segmentdefinitions.documentid = " + id.ToString() +
                    " and segmentattributes.name = @attribname";

            using (MySqlCommand cmd = new MySqlCommand(sql, GetConnection()))
            {
                cmd.Parameters.AddWithValue("attribname", EddyAttribName(metricType));

                object o = cmd.ExecuteScalar();
                if (o == null)
                    return 0;

                if (o == DBNull.Value)
                    return 0;

                double d = 0;
                double.TryParse(o.ToString(), out d);
                return d;
            }


        }

        public SegmentVariation CalculateSegmentVariation(int BaseTextSegmentID, VariationMetricTypes metricType)
        {
            Document baseTextDoc = new Document(ConfigProvider);
            baseTextDoc.OpenWithoutPrivilegeCheck(_name, null, true, null);
            SegmentDefinition d = baseTextDoc.GetSegmentDefinition(BaseTextSegmentID);

            if (d.Length == 0)
                throw new Exception("Unable to calculate variation for null alignments.");

            return CalculateSegmentVariation(baseTextDoc, d, new Dictionary<string, Document>(), null, metricType);
        }


        public bool ContinueSegmentVariationCalculation(out int progress)
        {
            if (_segCalcCount == _segs.Length)
            {
                progress = 100;
                return false; // all done
            }

            var sv = CalculateSegmentVariation(_baseTextDoc, _segs[_segCalcCount], _docCache, GetConnection(), _metricType);

            // Now insert segment attributes
            SegmentDefinition sd = _baseTextDoc.GetSegmentDefinition(sv.BaseTextSegmentID);
            AddAttribute(sd, VivAttribName(_metricType), sv.VivValue.ToString());

            _baseTextDoc.UpdateSegmentDefinition(sd);

            foreach (var vsv in sv.VersionSegmentVariations)
            {
                Document version = _docCache[vsv.VersionName];
                foreach (int id in vsv.VersionSegmentIDs)
                {
                    sd = version.GetSegmentDefinition(id);
                    AddAttribute(sd, EddyAttribName(_metricType), vsv.EddyValue.ToString());
                    version.UpdateSegmentDefinition(sd);
                }
            }
            _segCalcCount++;
            progress = (int)(_segCalcCount * 1.0 / _segs.Length * 100);
            return true;
        }

        

        #endregion



        string _name = string.Empty;
        int _id;
        int _basetextid;
        string _description = string.Empty;

        public Corpus(System.Collections.Specialized.NameValueCollection configProvider)
            : base(configProvider)
        {
        }



        private List<PredefinedSegmentAttribute> ReadPrefinedSegAttribs(string sql)
        {
            List<PredefinedSegmentAttribute> segAttribs = new List<PredefinedSegmentAttribute>();

            using (MySqlCommand cmd = new MySqlCommand(sql, GetConnection()))
            {
                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                   
                    int idOrdinal = dr.GetOrdinal("ID");
                    int nameOrdinal = dr.GetOrdinal("Name");
                    int typeOrdinal = dr.GetOrdinal("AttributeType");
                    int valueOrdinal = dr.GetOrdinal("Value");
                    int applyColouringOrdinal = dr.GetOrdinal("ApplyColouring");
                    int colourCodeOrdinal = dr.GetOrdinal("ColourCode");
                    int showInTOCOrdinal = dr.GetOrdinal("ShowInTOC");
                    //int tocTextOrdinal = dr.GetOrdinal("TOCText");

                    PredefinedSegmentAttribute segAttr = null;

                    while (dr.Read())
                    {
                        int id = dr.GetInt32(idOrdinal);

                        if (segAttr != null)
                        {
                            if (id == segAttr.ID)
                            {
                                // Got another attribute for the same seg
                                AppendValueFromReader(segAttr, dr, valueOrdinal, applyColouringOrdinal, colourCodeOrdinal);//, showInTOCOrdinal, tocTextOrdinal);
                                //Array.Resize(ref segDef.Attributes, segDef.Attributes.Length + 1);
                                //segDef.Attributes[segDef.Attributes.Length - 1] = AttribFromReader(dr, nameOrdinal, valueOrdinal);
                                continue;
                            }
                        }

                        segAttr = new PredefinedSegmentAttribute();
                        segAttr.Values = new PredefinedSegmentAttributeValue[0];
                        segAttr.Name = dr.GetString(nameOrdinal);
                        segAttr.AttributeType = (PredefinedSegmentAttributeType)(dr.GetInt32(typeOrdinal));
                        segAttr.ShowInTOC = dr.IsDBNull(showInTOCOrdinal) ? false : dr.GetBoolean(showInTOCOrdinal);
                        segAttr.ID = id;
                        if (!dr.IsDBNull(valueOrdinal))
                            AppendValueFromReader(segAttr, dr, valueOrdinal, applyColouringOrdinal, colourCodeOrdinal); //, showInTOCOrdinal), tocTextOrdinal);

                        segAttribs.Add(segAttr);
                    }

                }

            }

            return segAttribs;

        }

        private void AppendValueFromReader(PredefinedSegmentAttribute segAttr, MySqlDataReader dr, int valueOrdinal, int applyColouringOrdinal, int colourCodeOrdinal)//, int showInTOCOrdinal, int tocTextOrdinal)
        {
            

            string value = dr.GetString(valueOrdinal);

            PredefinedSegmentAttributeValue val = new PredefinedSegmentAttributeValue();
            val.Value = value;
            val.ApplyColouring = dr.IsDBNull(applyColouringOrdinal) ? false :  dr.GetBoolean(applyColouringOrdinal);
            //val.ShowInTOC = dr.IsDBNull(showInTOCOrdinal) ? false : dr.GetBoolean(showInTOCOrdinal);
            //val.IsTOCText = dr.IsDBNull(tocTextOrdinal) ? false : dr.GetBoolean(tocTextOrdinal);
            val.ColourCode = dr.IsDBNull(colourCodeOrdinal) ? string.Empty : dr.GetString(colourCodeOrdinal);
                 

            Array.Resize(ref segAttr.Values, segAttr.Values.Length + 1);
            segAttr.Values[segAttr.Values.Length - 1] = val; // AttribFromReader(dr, nameOrdinal, valueOrdinal);

            //return attrib;
        }


        private static string EddyAttribName(VariationMetricTypes metricType)
        {
            string s = ":eddy-";
            switch (metricType)
            {
                case EblaAPI.VariationMetricTypes.metricA:
                    return s + "a";
                case EblaAPI.VariationMetricTypes.metricB:
                    return s + "b";
                case EblaAPI.VariationMetricTypes.metricC:
                    return s + "c";
            }
            throw new Exception("Unknown VariationMetricTypes: " + metricType.ToString());
        }

        private static string VivAttribName(VariationMetricTypes metricType)
        {
            string s = ":viv-";
            switch (metricType)
            {
                case EblaAPI.VariationMetricTypes.metricA:
                    return s + "a";
                case EblaAPI.VariationMetricTypes.metricB:
                    return s + "b";
                case EblaAPI.VariationMetricTypes.metricC:
                    return s + "c";
            }
            throw new Exception("Unknown VariationMetricTypes: " + metricType.ToString());
        }


        // member vars used during background viv/eddy calculation
        int _segCalcCount = 0;
        SegmentDefinition[] _segs = null;
        Dictionary<string, Document> _docCache = null;
        Document _baseTextDoc = null;
        VariationMetricTypes _metricType;

        //public int CalculationProgress()
        //{
        //    throw new NotImplementedException();
        //}


        private void AddAttribute(SegmentDefinition sd, string name, string value)
        {
            List<SegmentAttribute> attribs = new List<EblaAPI.SegmentAttribute>(sd.Attributes);
            attribs.Add(new SegmentAttribute() { Name = name, Value = value });
            sd.Attributes = attribs.ToArray();

        }

        private class VariationCalcInfo
        {
            public int AlignmentID;
            public string versionName;
            //public List<int> segIDs;

        }

        internal int GetDocID(string nameIfVersion)
        {
            using (MySqlCommand cmd = new MySqlCommand("", GetConnection()))
            {
                string sql = "SELECT ID FROM documents WHERE CorpusID = " + _id.ToString();
                if (string.IsNullOrEmpty(nameIfVersion))
                {
                    sql += " AND NAME IS NULL";
                }
                else
                {
                    sql += " AND NAME = @name";
                    cmd.Parameters.AddWithValue("name", nameIfVersion);
                }

                cmd.CommandText = sql;

                object o = cmd.ExecuteScalar();
                if (o != null)
                    if (o != DBNull.Value)
                        return (int)o;

                throw new Exception("Unable to retrieve ID for document with name: " + nameIfVersion);
            }


 
        }

        private SegmentVariation RetrieveSegmentVariation(Document baseTextDoc, SegmentDefinition d, VariationMetricTypes metricType)
        {
            SegmentVariation sv = new EblaAPI.SegmentVariation();
            sv.BaseTextContent = baseTextDoc.GetDocumentContentText(d.StartPosition, d.Length);
            sv.BaseTextSegmentID = d.ID;
            

            string[] versions = GetVersionList();
            Dictionary<string, VersionSegmentVariation> variations = new Dictionary<string, EblaAPI.VersionSegmentVariation>();

            string eddyAttribName = ":eddy-a";
            if (metricType == VariationMetricTypes.metricB)
                eddyAttribName = ":eddy-b";

            foreach (string version in versions)
            {
                var alignmentSet = new AlignmentSet(ConfigProvider);
                alignmentSet.OpenWithoutPrivilegeCheck(_name, version, true, null);


                VersionSegmentVariation e = new VersionSegmentVariation();
                e.VersionName = version;
                e.VersionText = string.Empty;
                e.EddyValue = 0;

                var alignment = alignmentSet.FindAlignment(d.ID, true);

                if (alignment != null)
                {
                    Document doc = new Document(ConfigProvider);
                    doc.OpenWithoutPrivilegeCheck(_name, version, true, null);
                    int docid = GetDocID(version);

                    bool gotEddyAttrib = false;
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    foreach (int segid in alignment.SegmentIDsInVersion)
                    {
                        SegmentDefinition seg = Document.GetSegmentDefinition(segid, docid, GetConnection(), true );
                        if (!gotEddyAttrib)
                        {
                            foreach (var a in seg.Attributes)
                            {
                                if (string.Compare(a.Name, eddyAttribName) == 0)
                                {
                                    double v = 0;
                                    bool b = double.TryParse(a.Value, out v);
                                    e.EddyValue = v;
                                    gotEddyAttrib = true;
                                    break;
                                }

                            }

                            if (sb.Length > 0)
                                sb.Append(" ");

                            sb.Append(doc.GetDocumentContentText(seg.StartPosition, seg.Length));

                        }
                    }

                    e.VersionText = sb.ToString();
                    variations[version] = e;

                }
                

            }

            List<VersionSegmentVariation> vsvs = new List<EblaAPI.VersionSegmentVariation>();
            foreach (string s in variations.Keys)
            {
                vsvs.Add(variations[s]);
            }
            sv.VersionSegmentVariations = vsvs.ToArray();
            return sv;
            
        }

        private SegmentVariation CalculateSegmentVariation(Document baseTextDoc, SegmentDefinition d, Dictionary<string, Document> docCache, MySqlConnection cn, VariationMetricTypes metricType)
        {
            SegmentVariation sv = new EblaAPI.SegmentVariation();
            

            sv.BaseTextContent = baseTextDoc.GetDocumentContentText(d.StartPosition, d.Length);

            string[] baseTokens = TokeniseString(sv.BaseTextContent);

            // Now find all alignments to this baseSegID

            string sql = "SELECT DISTINCT AlignmentID," + /* versionsegmentdefinitions.ID, */ " versionsegmentdefinitions.DocumentID, documents.Name ";
            sql += "FROM segmentalignments  ";
            sql += "INNER JOIN segmentdefinitions as basesegmentdefinitions ON segmentalignments.BaseTextSegmentID = basesegmentdefinitions.ID ";
            sql += "INNER JOIN segmentdefinitions as versionsegmentdefinitions ON segmentalignments.VersionSegmentID = versionsegmentdefinitions.ID ";
            sql += "INNER JOIN documents ON documents.ID = versionsegmentdefinitions.DocumentID ";
            sql += "WHERE basesegmentdefinitions.DocumentID = " + _basetextid.ToString() + " AND segmentalignments.BaseTextSegmentID = " + d.ID.ToString();
            sql += " ORDER BY versionsegmentdefinitions.DocumentID";

            int lastAlignmentID = -1;
            int lastDocID = -1;
            string lastDocName = string.Empty;
            List<int> segIDs = null;
            Dictionary<string, string> versionStrings = new Dictionary<string, string>();
            //string versionString = string.Empty;

            //Dictionary<string, string[]> allVersionTokens = new Dictionary<string, string[]>();

            Dictionary<string, VersionSegmentVariation> variations = new Dictionary<string, EblaAPI.VersionSegmentVariation>();

            List<VariationCalcInfo> calcInfoList = new List<VariationCalcInfo>();

            using (MySqlCommand cmd = new MySqlCommand(sql, GetConnection()))
            {
                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    int alignmentIDOrdinal = dr.GetOrdinal("AlignmentID");
                    //int segIDOrdinal = dr.GetOrdinal("ID");
                    int docIDOrdinal = dr.GetOrdinal("DocumentID");
                    int docNameOrdinal = dr.GetOrdinal("Name");

                    while (dr.Read())
                    {
                        int alignmentID = dr.GetInt32(alignmentIDOrdinal);
                        //int segID = dr.GetInt32(segIDOrdinal);
                        int docID = dr.GetInt32(docIDOrdinal);
                        string docName = dr.GetString(docNameOrdinal);

                        VariationCalcInfo calcInfo = new VariationCalcInfo();
                        calcInfo.AlignmentID = alignmentID;
                        calcInfo.versionName = docName;
                        calcInfoList.Add(calcInfo);
                        /*if (alignmentID == lastAlignmentID)
                        {
                            System.Diagnostics.Debug.Assert(docID == lastDocID);
                            segIDs.Add(segID);

                        }
                        else
                        {
                            if (lastAlignmentID != -1)
                            {
                                VariationCalcInfo calcInfo = new VariationCalcInfo();
                                calcInfo.segIDs = segIDs;
                                calcInfo.versionName = lastDocName;
                                calcInfoList.Add(calcInfo);


                            }

                            // Prepare to collect more
                            segIDs = new List<int>();
                            lastDocID = docID;
                            lastDocName = docName;
                            segIDs.Add(segID);
                            lastAlignmentID = alignmentID;
                        }*/
                    } // while (reading)

                    /*if (segIDs != null && segIDs.Count > 0)
                    {
                        VariationCalcInfo calcInfo = new VariationCalcInfo();
                        calcInfo.segIDs = segIDs;
                        calcInfo.versionName = lastDocName;
                        calcInfoList.Add(calcInfo);
                    }*/
                    //ProcessAlignment(lastDocName, segIDs, versionStrings, variations);


                }
            }

            List<string> versionsToProcess = new List<string>();
            List<List<int>> segmentsToProcess = new List<List<int>>();
            foreach (var info in calcInfoList)
            {
                AlignmentSet a = new AlignmentSet(ConfigProvider);

                a.OpenWithoutPrivilegeCheck(_name, info.versionName, true, GetConnection());

                var alignment = a.GetAlignment(info.AlignmentID);
                
                // Check it isn't a many-to-one, since this simple non-aggregating first implementation
                // can't use that
                if (alignment.SegmentIDsInBaseText.Length == 1)
                {
                    versionsToProcess.Add(info.versionName);
                    segmentsToProcess.Add(new List<int>(alignment.SegmentIDsInVersion));
                }

            }

            //foreach (var info in calcInfoList)
            for (int i = 0; i < versionsToProcess.Count; i++)
                ProcessAlignment(versionsToProcess[i], segmentsToProcess[i], versionStrings, variations, docCache, cn);

            Dictionary<string, string[]> allVersionTokens = new Dictionary<string, string[]>();

            foreach (string s in versionStrings.Keys)
            {
                allVersionTokens.Add(s, TokeniseString(versionStrings[s]));
            }


            //var segmentVariations = new List<EblaAPI.VersionSegmentVariation>();
            List<TokenInfo> tokenInfos = new List<EblaAPI.TokenInfo>();

            // Figure out how many unique base text tokens there are
            var temp = new HashSet<string>();
            foreach (string s in baseTokens)
                if (!temp.Contains(s))
                    temp.Add(s);

            //double f = CalculateViv(temp.Count, allVersionTokens, variations, tokenInfos);

            // Change args to make it easier to share code with Geng
            List<string> versionNames = new List<string>();
            List<string[]> tokenisedVersions = new List<string[]>();
            List<double> eddyValues = new List<double>();

            foreach (string s in versionStrings.Keys)
            {
                versionNames.Add(s);
                tokenisedVersions.Add(allVersionTokens[s]);
                //eddyValues.Add(0);
            }

            // Use this to store counts of how often each unique token occurs in the whole corpus
            Dictionary<string, int> uniqueTokenCountsForCorpus = new Dictionary<string, int>();

            // Each entry in this list records the unique tokens for each version, and how often they occur in that version
            List<Dictionary<string, int>> uniqueTokenCountsPerVersion = new List<Dictionary<string, int>>();

            GetTokenCounts(tokenisedVersions, uniqueTokenCountsForCorpus, uniqueTokenCountsPerVersion);

            double f = CalculateVariation(baseTokens.Length, uniqueTokenCountsForCorpus, uniqueTokenCountsPerVersion, eddyValues, metricType);

            //if (metricType == EblaAPI.VariationMetricTypes.metricA)
            //{
            //    // Try an adjustment based on no. of unique tokens in base text 
            //    if (temp.Count > 0)
            //    {
            //        f /=  temp.Count; // Math.Log( temp.Count);
            //        System.Diagnostics.Debug.Assert(!double.IsNaN(f));
            //    }
            //    else
            //        // No alignments?
            //        f = 0;

            //}

            // Fixup results from calculation
            for (int i = 0; i < versionNames.Count; i++)
            {
                variations[versionNames[i]].EddyValue = eddyValues[i];
                Dictionary<string, int> uniqueTokenCountsForVersion = uniqueTokenCountsPerVersion[i];
                List<TokenInfo> tokenInfoForVersion = new List<TokenInfo>();
                foreach (string s in uniqueTokenCountsForVersion.Keys)
                {
                    TokenInfo t = new TokenInfo();
                    t.Token = s;
                    t.AverageCount = uniqueTokenCountsForVersion[s];
                    tokenInfoForVersion.Add(t);
                }
                variations[versionNames[i]].TokenInfo = tokenInfoForVersion.ToArray();
                variations[versionNames[i]].LanguageCode = docCache[versionNames[i]].GetMetadata().LanguageCode;
				variations[versionNames[i]].ReferenceDate = docCache[versionNames[i]].GetMetadata().ReferenceDate;
            }

            foreach (string token in uniqueTokenCountsForCorpus.Keys)
            {
                var ti = new TokenInfo();
                ti.Token = token;
                if (uniqueTokenCountsPerVersion.Count > 0)
                    ti.AverageCount = uniqueTokenCountsForCorpus[token] * 1.0 / uniqueTokenCountsPerVersion.Count;
                else
                    ti.AverageCount = 0;
                tokenInfos.Add(ti);
            }


            sv.BaseTextSegmentID = d.ID;

            sv.VivValue = f;

            List<VersionSegmentVariation> vsvs = new List<EblaAPI.VersionSegmentVariation>();
            foreach (string s in variations.Keys)
            {
                vsvs.Add(variations[s]);
            }
            sv.VersionSegmentVariations = vsvs.ToArray();
            sv.TokenInfo = tokenInfos.ToArray();
            

            return sv;

        }

        private void ProcessAlignment(string docName, List<int> segIDs, Dictionary<string, string> versionStrings, Dictionary<string, VersionSegmentVariation> variations, Dictionary<string, Document> docCache, MySqlConnection cn)
        {
            Document versionDoc = null;
            if (docCache.ContainsKey(docName))
                versionDoc = docCache[docName];
            else
            {
                versionDoc = new Document(ConfigProvider);
                versionDoc.OpenWithoutPrivilegeCheck(_name, docName, true, cn);
                docCache.Add(docName, versionDoc);
            }
            

            string s = CreateVersionString(segIDs, versionDoc);
                                
            versionStrings.Add(docName, s);

            var vsv = new VersionSegmentVariation();
            vsv.VersionName = docName;
            vsv.VersionSegmentIDs = segIDs.ToArray();
            vsv.VersionText = s;
            variations.Add(docName, vsv);

        }


        static void GetTokenCounts(List<string[]> tokenisedVersions, Dictionary<string, int> uniqueTokenCountsForCorpus, List<Dictionary<string, int>> uniqueTokenCountsPerVersion)
        {
            // Go through all the versions provided, counting token occurrences
            for (int versionIndex = 0; versionIndex < tokenisedVersions.Count; versionIndex++)
            {
                string[] tokens = tokenisedVersions[versionIndex];

                Dictionary<string, int> uniqueTokenCountsForThisVersion = new Dictionary<string, int>();
                uniqueTokenCountsPerVersion.Add(uniqueTokenCountsForThisVersion);

                // Look at all the tokens in this version
                foreach (string token in tokens)
                {
                    // Increment the occurrence-count-in-whole-corpus
                    if (uniqueTokenCountsForCorpus.ContainsKey(token))
                        uniqueTokenCountsForCorpus[token] = uniqueTokenCountsForCorpus[token] + 1;
                    else
                        uniqueTokenCountsForCorpus.Add(token, 1);

                    // Increment the occurrence-count-in-this-version
                    if (uniqueTokenCountsForThisVersion.ContainsKey(token))
                        uniqueTokenCountsForThisVersion[token] = uniqueTokenCountsForThisVersion[token] + 1;
                    else
                        uniqueTokenCountsForThisVersion.Add(token, 1);

                }

            }


        }

        /// <summary>
        /// Calculates Viv and Eddy values for a set of versions w.r.t. the 'corpus', that is, the set containing only those versions.
        /// </summary>
        /// <param name="uniqueTokenCountsForCorpus">A dictionary of unique tokens found in the corpus, with a count of occurrences in the corpus for each token.</param>
        /// <param name="uniqueTokenCountsPerVersion">A list of dictionaries, one per version, where each dictionary contains the unique tokens found in the version, with a count of occurrences in the version for each token.</param>
        /// <param name="eddyValues">A list of doubles, one per version, used to return the Eddy values for each version.</param>
        /// <returns>The Viv value for the corpus.</returns>
        static double CalculateVariation(int baseSegmentWordCount, Dictionary<string, int> uniqueTokenCountsForCorpus, List<Dictionary<string, int>> uniqueTokenCountsPerVersion, List<double> eddyValues, VariationMetricTypes metricType)
        {
            switch (metricType)
            {
                case EblaAPI.VariationMetricTypes.metricA:
                    return CalculateVariationTypeA(baseSegmentWordCount, uniqueTokenCountsForCorpus, uniqueTokenCountsPerVersion, eddyValues);
                case EblaAPI.VariationMetricTypes.metricB:
                    return CalculateVariationTypeB(baseSegmentWordCount, uniqueTokenCountsForCorpus, uniqueTokenCountsPerVersion, eddyValues);
                case EblaAPI.VariationMetricTypes.metricC:
                    return CalculateVariationTypeC(baseSegmentWordCount, uniqueTokenCountsForCorpus, uniqueTokenCountsPerVersion, eddyValues);

            }

            throw new Exception("Unknown VariationMetricTypes: " + metricType.ToString());
        }

        static double CalculateVariationTypeB(int baseSegmentWordCount, Dictionary<string, int> uniqueTokenCountsForCorpus, List<Dictionary<string, int>> uniqueTokenCountsPerVersion, List<double> eddyValues)
        {
            int noOfVersions = uniqueTokenCountsPerVersion.Count;
            if (noOfVersions == 0)
                return 0;

            eddyValues.Clear();
            // Tom's original formulae

            // For each version, eddy is sum of D/tf for each token t in the version
            double sumOfEddyValues = 0;
            for (int versionIndex = 0; versionIndex < uniqueTokenCountsPerVersion.Count; versionIndex++)
            {

                var uniqueTokenCountsForThisVersion = uniqueTokenCountsPerVersion[versionIndex];

                double eddyValue = 0;
                foreach (string token in uniqueTokenCountsForThisVersion.Keys)
                {
                    eddyValue += (1.0 * noOfVersions) / uniqueTokenCountsForCorpus[token] /* * uniqueTokenCountsForThisVersion[token] */;
                }
                sumOfEddyValues += eddyValue;
                eddyValues.Add(eddyValue);
            }

            return sumOfEddyValues / noOfVersions;
        }

        //static double CalculateVariationTypeC(Dictionary<string, int> uniqueTokenCountsForCorpus, List<Dictionary<string, int>> uniqueTokenCountsPerVersion, List<double> eddyValues)
        //{
        //    int noOfVersions = uniqueTokenCountsPerVersion.Count;
        //    if (noOfVersions == 0)
        //        return 0;

        //    eddyValues.Clear();
            
        //    // Test implementation

            
        //    double sumOfEddyValues = 0;
        //    for (int versionIndex = 0; versionIndex < uniqueTokenCountsPerVersion.Count; versionIndex++)
        //    {

        //        var uniqueTokenCountsForThisVersion = uniqueTokenCountsPerVersion[versionIndex];

        //        double eddyValue = 0;
        //        foreach (string token in uniqueTokenCountsForCorpus.Keys)
        //        {
        //            double thisVal = 0;
        //            if (uniqueTokenCountsForThisVersion.ContainsKey(token))
        //                thisVal = uniqueTokenCountsForThisVersion[token];
        //            eddyValue += Math.Abs((uniqueTokenCountsForCorpus[token] * 1.0 / noOfVersions) - thisVal);

        //        }
        //        eddyValue /= uniqueTokenCountsForCorpus.Count;

        //        sumOfEddyValues += eddyValue;
        //        eddyValues.Add(eddyValue);
        //    }

        //    return sumOfEddyValues / noOfVersions;
        //}

        static double CalculateVariationTypeC(int baseSegmentWordCount, Dictionary<string, int> uniqueTokenCountsForCorpus, List<Dictionary<string, int>> uniqueTokenCountsPerVersion, List<double> eddyValues)
        {
            var unused = CalculateVariationTypeA(baseSegmentWordCount, uniqueTokenCountsForCorpus, uniqueTokenCountsPerVersion, eddyValues);

            return StandardDeviation(eddyValues);
        }

        public static double StandardDeviation(List<double> valueList)
        {
            double M = 0.0;
            double S = 0.0;
            int k = 1;
            foreach (double value in valueList)
            {
                double tmpM = M;
                M += (value - tmpM) / k;
                S += (value - tmpM) * (value - M);
                k++;
            }
            return Math.Sqrt(S / (k - 1));
        }

        static double CalculateVariationTypeA(int baseSegmentWordCount, Dictionary<string, int> uniqueTokenCountsForCorpus, List<Dictionary<string, int>> uniqueTokenCountsPerVersion, List<double> eddyValues)
        {
            int noOfVersions = uniqueTokenCountsPerVersion.Count;
            eddyValues.Clear();

            // Calculate centre of "finite set of points" (http://en.wikipedia.org/wiki/Centroid)

            Dictionary<string, double> tokenAverages = new Dictionary<string, double>();

            foreach (string token in uniqueTokenCountsForCorpus.Keys)
                tokenAverages.Add(token, uniqueTokenCountsForCorpus[token] * 1.0 / noOfVersions);


            double divisor = Math.Log((baseSegmentWordCount + 50) * 1.0 / 50) * 8 + 1;

            double sumOfEddyValuesOldFormula = 0;
            double sumOfEddyValues = 0;
            //double variance = 0;
            int totalWordsInVersions = 0;
            // For each version, calculate distance from centre, i.e. the Eddy value
            for (int versionIndex = 0; versionIndex < uniqueTokenCountsPerVersion.Count; versionIndex++)
            {
                double temp = 0;
                var uniqueTokenCountsForThisVersion = uniqueTokenCountsPerVersion[versionIndex];

                // work out total number of (significant) words in this version's segment
                int segmentlength = 0;
                foreach (string s in uniqueTokenCountsForThisVersion.Keys)
                    segmentlength += uniqueTokenCountsForThisVersion[s];

                // Calculate n-dimensional Euclidean distance (http://en.wikipedia.org/wiki/Euclidean_distance)
                foreach (string token in uniqueTokenCountsForCorpus.Keys)
                {
                    // What is this distance between the average and this version?
                    double thisVal = 0;
                    if (uniqueTokenCountsForThisVersion.ContainsKey(token))
                        thisVal = uniqueTokenCountsForThisVersion[token];
                    double distance = (tokenAverages[token] - thisVal);
                    temp += distance * distance;
                    //variance += temp;

                }
                double eddyValue = Math.Sqrt(temp);
                sumOfEddyValuesOldFormula += eddyValue;
                eddyValue /= divisor;
                eddyValues.Add(eddyValue);

                //double divisor = Math.Log((segmentlength + 50) * 1.0 / 50) * 8 + 1;

                sumOfEddyValues += eddyValue; // / divisor;
                //variance /= uniqueTokenCountsForCorpus.Count;
                totalWordsInVersions += segmentlength;
            }

            //if (noOfVersions > 0)
            //    variance /= noOfVersions;

            double vivValue = 0;

            if (noOfVersions > 0)
            {
                vivValue = sumOfEddyValues / noOfVersions;


                double averageWordCountInVersions = totalWordsInVersions * 1.0 / noOfVersions;
                double divisorOldFormula = Math.Log((averageWordCountInVersions + 50) * 1.0 / 50) * 8 + 1;
                //vivValue /= divisor;
                double vivValueOldFormula = sumOfEddyValuesOldFormula / noOfVersions / divisorOldFormula;

                if ((vivValue / vivValueOldFormula) > 1.5)
                {
                    string s = "gkjg";
                }
                if ((vivValueOldFormula / vivValue) > 1.5)
                {
                    string s = "gkjg";
                }

            }

            
            //// try using s.d. instead
            //vivValue = Math.Sqrt(variance);

            return vivValue;
        }



        string[] TokeniseString(string text)
        {
            // Tom may want apostrophes taken out, but for now we'll just drag out alpha sequences
            //string[] tokens = System.Text.RegularExpressions.Regex.Split(text, @"[a-zA-Z]+");

            var matches= System.Text.RegularExpressions.Regex.Matches(text, @"\w+");
            List<string> tokens = new List<string>();

            foreach (System.Text.RegularExpressions.Match match in matches)
                tokens.Add(match.Value.ToLower());

            List<string> result = new List<string>();
            foreach (string s in tokens)
                if (!_deStopWords.Contains(s))
                    result.Add(s);


            //return tokens.ToArray();
            return result.ToArray();

            //for (int i = 0; i < tokens.Length; i++)
            //    tokens[i] = tokens[i].ToLower();

            //return tokens;
        }

        string CreateVersionString(List<int> segIDs, Document versionDoc)
        {

            StringBuilder sb = new StringBuilder();
            foreach (int id in segIDs)
            {
                if (sb.Length > 0)
                    sb.Append(" ");
                SegmentDefinition d = versionDoc.GetSegmentDefinition(id);

                if (d.Length > 0)
                    sb.Append(versionDoc.GetDocumentContentText(d.StartPosition, d.Length));
            }

            return sb.ToString();
        }

        private HtmlErrors UploadDocument(int ID, string html, bool updating, string nameIfVersion)
        {
            CheckOpen();

            HtmlErrors errors = new HtmlErrors();

            string TOC = string.Empty;
            int length = 0;

            //List<DocumentOffset> offsetList = new List<DocumentOffset>();

            List<SegmentDefinition> segdefs = new List<EblaAPI.SegmentDefinition>();

            string xml = ParseHtml(html, errors, out TOC, out length, segdefs); //, offsetList);

            if (errors.HtmlParseErrors.Length > 0)
                return errors;

            Document docIfUpdating = null;

            if (updating)
            {
                // Get existing segment defs
                docIfUpdating = new Document(ConfigProvider);

                docIfUpdating.OpenWithoutPrivilegeCheck(_name, nameIfVersion, true, GetConnection());

                var oldsegdefs = docIfUpdating.FindSegmentDefinitions(0, docIfUpdating.Length(), false, false, null);

                var oldsegdefmap = new Dictionary<int, SegmentDefinition>();
                var oldsegdefids = new HashSet<int>();
                foreach (var s in oldsegdefs)
                {
                    oldsegdefmap.Add(s.ID, s);
                    oldsegdefids.Add(s.ID);
                }

                foreach (var s in segdefs)
                    if (oldsegdefids.Contains(s.ID))
                    {
                        oldsegdefids.Remove(s.ID);
                    }
                    else
                        throw new Exception("A segment definition with ID " + s.ID.ToString() + " was found, but no segment definition with that ID has been defined for the document.");

                if (oldsegdefids.Count > 0)
                {
                    var e = oldsegdefids.GetEnumerator();
                    e.MoveNext();
                    throw new Exception(oldsegdefids.Count.ToString() + " segment definitions were missing. First ID: " + e.Current);
                }
                
            }

            using (MySqlTransaction txn = GetConnection().BeginTransaction())
            {
                using (MySqlCommand cmd = new MySqlCommand("UPDATE documents SET Content = @content, TOC = @toc, Length = @length WHERE ID = " + ID.ToString(), GetConnection()))
                {
                    cmd.Transaction = txn;

                    cmd.Parameters.AddWithValue("content", xml);
                    cmd.Parameters.AddWithValue("toc", TOC);
                    cmd.Parameters.AddWithValue("length", length);
                    
                    cmd.ExecuteNonQuery();

                    if (updating)
                    {
                        cmd.Parameters.Clear();
                        var startPosParm = cmd.Parameters.AddWithValue("startpos", 0);
                        var lengthParm = cmd.Parameters.AddWithValue("length", 0);
                        var idParm = cmd.Parameters.AddWithValue("id", 0);
                        
                        string sql = "UPDATE segmentdefinitions SET StartPosition = @startpos, Length = @length WHERE ID = @id";
                        cmd.CommandText = sql;

                        foreach (var segdef in segdefs)
                        {

                            idParm.Value = segdef.ID;


                            startPosParm.Value = segdef.StartPosition;
                            lengthParm.Value = segdef.Length;

                            cmd.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        cmd.Parameters.Clear();
                        cmd.CommandText = "DELETE FROM segmentdefinitions WHERE DocumentID = " + ID.ToString();
                        cmd.ExecuteNonQuery();

                        foreach (var segdef in segdefs)
                        {
                            Document.CreateSegmentDefinition(ID, segdef, cmd);
                        }
                    }

                    //cmd.CommandText = "DELETE FROM documentoffsetlists WHERE DocumentID = " + ID.ToString();
                    //cmd.ExecuteNonQuery();

                    /*
                    cmd.CommandText = "INSERT INTO documentoffsetlists (DocumentID, Offset, ParentID, ChildIndex) VALUES (" + ID.ToString() + ", @offset, @parentid, @childindex)";
                    MySqlParameter offsetparm = cmd.Parameters.AddWithValue("offset", 0);
                    MySqlParameter parentidparm = cmd.Parameters.AddWithValue("parentid", 0);
                    MySqlParameter childindexparm = cmd.Parameters.AddWithValue("childindex", 0);

                    foreach (var o in offsetList)
                    {
                        offsetparm.Value = o.offset;
                        parentidparm.Value = o.parentID;
                        childindexparm.Value = o.childIndex;
                        cmd.ExecuteNonQuery();
                    }*/



                }


                txn.Commit();
            }

            
            


            return errors;
        }


        private string ParseHtml(string html, HtmlErrors errors, out string TOC, out int length, List<SegmentDefinition> segdefs) //, List<DocumentOffset> offsetList)
        {
            TOC = string.Empty;
            length = 0;

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);

            List<EblaAPI.HtmlParseError> parseErrors = new List<EblaAPI.HtmlParseError>();

            foreach (var e in doc.ParseErrors)
            {
                EblaAPI.HtmlParseError error = new EblaAPI.HtmlParseError();
                error.SourceText = e.SourceText;
                error.Line = e.Line;
                error.Reason = e.Reason;
                error.StreamPosition = e.StreamPosition;
                error.LinePosition = e.LinePosition;

                parseErrors.Add(error);
            }

            errors.HtmlParseErrors = parseErrors.ToArray();

            if (parseErrors.Count > 0)
                return string.Empty;


            HtmlNodeCollection bodyNodes = doc.DocumentNode.SelectNodes("/html/body");

            if (bodyNodes == null || bodyNodes.Count == 0)
            {
                EblaAPI.HtmlParseError error = new EblaAPI.HtmlParseError();
                error.SourceText = string.Empty;
                error.Reason = "HTML 'body' element not found";
                error.StreamPosition = -1;
                error.Line = -1;
                error.LinePosition = -1;
                parseErrors.Add(error);
                errors.HtmlParseErrors = parseErrors.ToArray();

                return string.Empty;
            }

            if (bodyNodes.Count > 1)
            {
                EblaAPI.HtmlParseError error = new EblaAPI.HtmlParseError();
                error.SourceText = string.Empty;
                error.Reason = "More than one HTML 'body' element found";
                error.StreamPosition = -1;
                error.Line = -1;
                error.LinePosition = -1;
                parseErrors.Add(error);
                errors.HtmlParseErrors = parseErrors.ToArray();

                return string.Empty;

            }

            HtmlNode bodyNode = bodyNodes[0];

            XmlDocument xmlDoc = EblaHelpers.CreateXmlDocument("body");

            XmlAttribute attrib = xmlDoc.CreateAttribute("id");
            attrib.Value = "0";
            xmlDoc.DocumentElement.Attributes.Append(attrib);


            XmlDocument xmlDocTOC = EblaHelpers.CreateXmlDocument("TOC");

            int totalChars = 0;
            int id = 1;
            AppendNodes(xmlDoc.DocumentElement, bodyNode, ref id, xmlDocTOC.DocumentElement, ref totalChars);

            // Go through a to-string/from-string cycle, as this changes the doc representation.
            // For instance, <p>&nbsp;</p> in the htmlagility doc is an element containing the text node, so the 
            // node-by-node copy creates that structure in the xml doc. However, when saved to XML then reloaded,
            // the xml doc contains only <p />, *even though the &nbsp remains in the XML*. We need to have the xml
            // doc in an as-loaded-from-xml state rather than in an as-built-by-appending-nodes state before 
            // measuring any segment marker offsets etc.
            string temp = EblaHelpers.XmlDocumentToString(xmlDoc);
            xmlDoc.LoadXml(temp);
#if _VVV_PRESERVEWHITESPACE
            EblaHelpers.ConvertWhitespaceNodesToTextNodes(xmlDoc.DocumentElement);
#endif
            bool lastTextNodeHadTrailingWhitespace = false;
            totalChars = 0;
            RemoveUnnecessaryWhitespace(xmlDoc.DocumentElement, ref lastTextNodeHadTrailingWhitespace, ref totalChars, Document.IgnoreElm);


            // Deal with any segment markers that have been put in the document
            XmlNodeList eblaSpans = xmlDoc.SelectNodes("//span[@data-eblatype='startmarker']");
            var idsUsed = new HashSet<int>();
            foreach (XmlNode segstart in eblaSpans)
            {
                XmlAttribute idattrib = segstart.Attributes["data-eblasegid"];
                if (idattrib == null)
                    throw new Exception("Start marker found with no id attrib.");

                int segid = 0;
                if (!Int32.TryParse(idattrib.Value, out segid))
                    throw new Exception("Start marker found with invalid attrib.");

                if (idsUsed.Contains(segid))
                    throw new Exception("Start marker found with repeated seg id: " + segid.ToString());


                if (segstart.ChildNodes.Count != 1)
                    throw new Exception("Start marker found with incorrect no. of child nodes");

                if (segstart.ChildNodes[0].NodeType != XmlNodeType.Text)
                    throw new Exception("Start marker found with child element that is not text");

                if (string.Compare(segstart.ChildNodes[0].Value, "[") != 0)
                    throw new Exception("Start marker found with illegal marker text");


                XmlNodeList endSegs = xmlDoc.SelectNodes("//span[@data-eblasegid='" + segid.ToString() + "' and @data-eblatype='endmarker']");
                if (endSegs == null || endSegs.Count == 0)
                    throw new Exception("End marker not found for id: " + segid.ToString());

                if (endSegs.Count > 1)
                    throw new Exception("Too many end markers for id: " + segid.ToString());

                XmlNode segend = endSegs[0];

                if (segend.ChildNodes.Count != 1)
                    throw new Exception("End marker found with incorrect no. of child nodes");

                if (segend.ChildNodes[0].NodeType != XmlNodeType.Text)
                    throw new Exception("End marker found with child element that is not text");

                if (string.Compare(segend.ChildNodes[0].Value, "]") != 0)
                    throw new Exception("End marker found with illegal marker text");




                //if (segstart.NextSibling != null)
                //    if (segstart.NextSibling.NodeType == XmlNodeType.Text)
                //    {
                //        XmlText text = (XmlText)(segstart.NextSibling);
                //        if (string.Compare(text.Value, "[") == 0)
                //        {
                //            if (text.NextSibling != null)
                //            {

                //            }
                //        }

                //    }


                List<string> attribNames = new List<string>();
                List<string> attribVals = new List<string>();
                string userattribprefix = "data-userattrib-";

                foreach (XmlAttribute a in segstart.Attributes)
                {
                    if (a.Name.Length > userattribprefix.Length)
                        if (string.Compare(a.Name.Substring(0, userattribprefix.Length), userattribprefix) == 0)
                        {
                            attribNames.Add(a.Name.Substring(userattribprefix.Length));
                            attribVals.Add(a.Value);
                        }
                }

                XmlDocPos pos = new XmlDocPos();
                pos.textContent = (XmlText)(segstart.ChildNodes[0]);
                XmlAttribute eblaatt = segstart.Attributes["data-eblatype"];
                segstart.Attributes.RemoveNamedItem("data-eblatype"); // temporarily stop it being ignored, so it can be sought
                pos.offset = 0;
                

#if _VVV_PRESERVEWHITESPACE
                int startOffset = XmlDocPos.MeasureTo(pos, true);
#else
                int startOffset = XmlDocPos.MeasureTo(pos, false, Document.IgnoreElm);

#endif
//                segstart.Attributes.Append(eblaatt);

                segstart.ParentNode.RemoveChild(segstart);

                var startPos = new XmlDocPos();
                startPos.offset = 0;
                startPos.textContent = pos.textContent;

                pos.textContent = (XmlText)(segend.ChildNodes[0]);
                eblaatt = segend.Attributes["data-eblatype"];
                segend.Attributes.RemoveNamedItem("data-eblatype"); // temporarily stop it being ignored, so it can be sought
                

#if _VVV_PRESERVEWHITESPACE
                int endOffset = XmlDocPos.MeasureTo(pos, true);
#else
                int endOffset = XmlDocPos.MeasureTo(pos, false, Document.IgnoreElm);
#endif
//                segend.Attributes.Append(eblaatt);
                segend.ParentNode.RemoveChild(segend);

                if (endOffset < startOffset)
                    throw new Exception("End marker before start for id: " + segid.ToString());


                int seglength = endOffset - startOffset; 
                bool ignore = false;
                if (seglength == 0)
                {
                    //ignore = true;
                }
                else
                {
                    //startPos.textContent = Document.NextTextNode(segstart, segstart.ParentNode, Document.IgnoreElm);

                    //if (startPos.textContent == null)
                    //    throw new Exception("Unable to find next text node after start marker for segid: " + segid.ToString());

                    int tempOffset = startOffset;
                    startPos = XmlDocPos.SeekToOffset(ref tempOffset, xmlDoc.DocumentElement, Document.IgnoreElm);

                    // If the segment only contains whitespace, ignore it, because segments
                    // like that really screw things up (because of lossy HTML whitespace presentation in the browser)

                    // Actually, our enhanced whitepspace-removal code should already have stripped it out.

                    XmlDocument tempDoc = EblaHelpers.CreateXmlDocument("content");
                    var status = new Document.CloneStatus();
                    status.StartPosFound = false;
                    status.LengthRemaining = seglength;

                    // This is a rather expensive way of getting just the text content between two positions.
                    // TODO - optimise.
                    Document.CloneBetweenPositions(xmlDoc.DocumentElement, tempDoc.DocumentElement, startPos, status, false, Document.IgnoreElm);

                    //ignore = string.IsNullOrWhiteSpace(tempDoc.DocumentElement.InnerText);
                    
                    // The most there should be is a single whitespace char.
                    if (string.IsNullOrWhiteSpace(tempDoc.DocumentElement.InnerText))
                        if (tempDoc.DocumentElement.InnerText.Length > 1)
                            throw new Exception("Unexpected whitespace segment, id: " + segid.ToString());
                }



                if (!ignore) 
                {
                    SegmentDefinition segdef = new SegmentDefinition();
                    segdef.StartPosition = startOffset;
                    segdef.Length = seglength;
                    segdef.ID = segid; // when updating.

                    List<SegmentAttribute> segattribs = new List<EblaAPI.SegmentAttribute>();

                    for (int attindex = 0; attindex < attribNames.Count; attindex++)
                    {
                        SegmentAttribute segattrib = new SegmentAttribute();
                        segattrib.Name = attribNames[attindex];
                        segattrib.Value = attribVals[attindex];
                        segattribs.Add(segattrib);
                    }

                    segdef.Attributes = segattribs.ToArray();

                    segdefs.Add(segdef);
                }


                idsUsed.Add(segid);

                // XmlDocPos.MeasureTo(
            }

                      


            length = totalChars;
                
            TOC = EblaHelpers.XmlDocumentToString(xmlDocTOC);

            string content = EblaHelpers.XmlDocumentToString(xmlDoc);

            //if (true)
            //{
            //    XmlDocument test = new XmlDocument();
            //    test.LoadXml(content);

            //    int origLength = XmlDocPos.Measure(xmlDoc);
            //    int newLength = XmlDocPos.Measure(test);

            //    bool same = XmlDocPos.Compare(xmlDoc.DocumentElement, test.DocumentElement);

            //    System.Diagnostics.Debug.Assert(same);

            //}

            return content;
        }

        bool AttribPermitted(string attribName)
        {
            if (attribName.Length < 5)
                return false;

            if (string.Compare(attribName.Substring(0, 5), "data-") == 0)
                return true;

            if (_legalAttribs.Contains(attribName.ToLower()))
                return true;

            return false;
        }

        bool TagRemovesWhitespace(string tagName)
        {
            if (!EblaHelpers.IsInlineTag(tagName))
                return true;

            return string.Compare(tagName, "br", true) == 0;

        }

        void RemoveUnnecessaryWhitespace(XmlNode node, ref bool lastTextNodeHadTrailingWhitespace, ref int totalChars, XmlDocPos.IgnoreElmDelg IgnoreElmImpl)
        {

            switch (node.NodeType)
            {
                case XmlNodeType.Element:
                    // Are we something to ignore?
                    if (IgnoreElmImpl.Invoke(node))
                    {
                        return;
                    }
                    //if (string.Compare(node.Name, "span") == 0)
                    //{
                    //    XmlAttribute eblaattrib = node.Attributes["data-eblatype"];
                    //    if (eblaattrib != null)
                    //        switch (eblaattrib.Value)
                    //        {
                    //            case "startmarker":
                    //            case "endmarker":
                    //                return;
                    //        }
                    //}

                    // Do we just contain something like 'nbsp;'?
                    // (The .NET XML parser tells us this is an empty element, so when counting offsets it 
                    // yields 0 text characters, even though

                    break;
                case XmlNodeType.Text:

                    // Are we the very first node?
                    if (totalChars == 0)
                        node.Value = node.Value.TrimStart();

                    // Are we the first child of a block-level element?
                    XmlNode temp = node;
                    while (temp.ParentNode != null)
                    {
                        XmlNode firstChild = temp.ParentNode.FirstChild; 
                        while (IgnoreElmImpl.Invoke(firstChild))
                        {
                            firstChild = firstChild.NextSibling;
                            if (firstChild == null)
                                break;
                        }

                        if (object.ReferenceEquals(firstChild, temp))
                        {
                            if (temp.ParentNode.NodeType == XmlNodeType.Element)
                                if (!EblaHelpers.IsInlineTag(temp.ParentNode.Name))
                                {
                                    // Yes
                                    node.Value = node.Value.TrimStart();
                                    break;
                                }

                        }
                        else
                            break;
                        temp = temp.ParentNode;
                    }

                    // Are we the last child of a block-level element?
                    temp = node;
                    while (temp.ParentNode != null)
                    {
                        XmlNode lastChild = temp.ParentNode.LastChild;
                        while (IgnoreElmImpl.Invoke(lastChild))
                        {
                            lastChild = lastChild.PreviousSibling;
                            if (lastChild == null)
                                break;
                        }
                        if (object.ReferenceEquals(lastChild, temp))
                        {
                            if (temp.ParentNode.NodeType == XmlNodeType.Element)
                                if (!EblaHelpers.IsInlineTag(temp.ParentNode.Name))
                                {
                                    // Yes
                                    node.Value = node.Value.TrimEnd();
                                    break;
                                }

                        }
                        else
                            break;
                        temp = temp.ParentNode;
                    }

                    // Are we preceded by a block-level element
                    temp = node;
                    while (true)
                    {
                        XmlNode sibling = temp.PreviousSibling;
                        while (sibling != null)
                            if (IgnoreElmImpl.Invoke(sibling))
                                sibling = sibling.PreviousSibling;
                            else
                                break;

                        if (sibling != null)
                        {
                            if (sibling.NodeType == XmlNodeType.Element)
                                if (TagRemovesWhitespace(sibling.Name))
                                {
                                    // Yes
                                    node.Value = node.Value.TrimStart();
                                }

                            break;
                        }

                        if (temp.ParentNode == null)
                            break;

                        temp = temp.ParentNode;

                    }

                    // Are we followed by a block-level element
                    temp = node;
                    while (true)
                    {
                        XmlNode sibling = temp.NextSibling;
                        while (sibling != null)
                            if (IgnoreElmImpl.Invoke(sibling))
                                sibling = sibling.NextSibling;
                            else
                                break;

                        if (sibling != null)
                        {
                            if (sibling.NodeType == XmlNodeType.Element)
                                if (TagRemovesWhitespace(sibling.Name))
                                {
                                    // Yes
                                    node.Value = node.Value.TrimEnd();
                                }

                            break;
                        }

                        if (temp.ParentNode == null)
                            break;

                        temp = temp.ParentNode;

                    }

                    //XmlNode sibling = node.PreviousSibling;
                    //if (sibling != null)
                    //{
                    //    if (sibling.NodeType == XmlNodeType.Element)
                    //        if (TagRemovesWhitespace(sibling.Name))
                    //        {
                    //            // Yes
                    //            node.Value = node.Value.TrimStart();
                    //        }

                    //}


                    // Are we followed by a block-level element
                    //sibling = node.NextSibling;
                    //if (sibling != null)
                    //{
                    //    if (sibling.NodeType == XmlNodeType.Element)
                    //        if (TagRemovesWhitespace(sibling.Name))
                    //        {
                    //            // Yes
                    //            node.Value = node.Value.TrimEnd();
                    //        }

                    //}

                    // Did the previous text node have trailing whitespace?
                    if (lastTextNodeHadTrailingWhitespace)
                        node.Value = node.Value.TrimStart();

                    // Do we still have leading whitespace?
                    if (node.Value.Length > 0)
                    {
                        // ensure it's the minimum
                        if (char.IsWhiteSpace(node.Value[0]))
                            node.Value = " " + node.Value.TrimStart();
                    }

                    // Do we have trailing whitespace?
                    if (node.Value.Length > 0)
                    {
                        lastTextNodeHadTrailingWhitespace = char.IsWhiteSpace(node.Value.Substring(node.Value.Length - 1)[0]);

                        if (lastTextNodeHadTrailingWhitespace)
                        {
                            // ensure it's the minimum
                            node.Value = node.Value.TrimEnd() + " ";
                        }
                    }

                    totalChars += node.Value.Length;

                    break;

            }

            foreach (XmlNode n in node.ChildNodes)
            {
                RemoveUnnecessaryWhitespace(n, ref lastTextNodeHadTrailingWhitespace, ref totalChars, Document.IgnoreElm);
            }
        }

        void AppendNodes(XmlNode xmlNode, HtmlNode htmlNode, ref int id, XmlNode rootTOC, ref int totalChars)
        {
            string tagName;
            foreach (var n in htmlNode.ChildNodes)
            {
                switch (n.NodeType)
                {
                    case HtmlNodeType.Element:
                        tagName = n.Name;
                        if (string.Compare(tagName, _segMarkerEditPlaceholder) == 0)
                            tagName = "span";
                        if (TagIsPermitted(tagName))
                        {
                            XmlElement elm = xmlNode.OwnerDocument.CreateElement(tagName.ToLower());
                            XmlAttribute attrib = xmlNode.OwnerDocument.CreateAttribute("id");
                            attrib.Value = id.ToString();
                            id++;
                            elm.Attributes.Append(attrib);

                            foreach(HtmlAttribute a in n.Attributes)
                            {
                                if (AttribPermitted(a.Name))
                                {
                                    attrib = xmlNode.OwnerDocument.CreateAttribute(a.Name.ToLower());
                                    attrib.Value = a.Value;
                                    elm.Attributes.Append(attrib);
                                }
                            }

                            xmlNode.AppendChild(elm);
                            int tempPos = totalChars;
                            AppendNodes(elm, n, ref id, rootTOC, ref totalChars);



                            if (IncludeTagInTOC(n))
                            {
                                XmlElement elmTOC = rootTOC.OwnerDocument.CreateElement(n.Name);
                                XmlAttribute attribTOC = rootTOC.OwnerDocument.CreateAttribute("id");
                                attribTOC.Value = attrib.Value;
                                elmTOC.Attributes.Append(attribTOC);
                                attribTOC = rootTOC.OwnerDocument.CreateAttribute("pos");
                                attribTOC.Value = tempPos.ToString();
                                elmTOC.Attributes.Append(attribTOC);
                                XmlNode textElm = rootTOC.OwnerDocument.CreateTextNode(elm.InnerText);
                                elmTOC.AppendChild(textElm);
                            }
                            
                        }

                        break;
                    case HtmlNodeType.Comment:
                        break;
                    case HtmlNodeType.Text:
                        if (string.IsNullOrWhiteSpace(n.InnerText))
                        {
                            // Skip - otherwise serialisation makes it different
                        }
                        else
                        {
                            // The HTML read may include escape sequences such as &lt;
                            // If we pass that to xml node creation as-is, the '&' will be turned
                            // into its own escape sequence, leaving &amp;lt;
                            // So, we have to decode the string first.
                            String s = System.Net.WebUtility.HtmlDecode(n.InnerText);
                            // The HTML that's been read may have line endings as \r\n, which counts as 2 chars.
                            // However, when that's rendered in the browser, it will only count as one char
                            // in the DOM. This means that web-front-end 'selection count' maths will
                            // be wrong when trying (say) to create segments. So, we normalise
                            // line ends here.
                            s = s.Replace("\r\n", "\n");

                            // Actually, while web-front-end 'selection count' code then works fine
                            // for normal rendering, whitespace gets removed when something like 
                            // ckeditor is used. So we need to try to anticipate that here,
                            // by removing it first, so the maths still works.
                            s = s.Replace("\r", " ");
                            s = s.Replace("\n", " ");
                            s = s.Replace("\t", " ");
                            s = System.Text.RegularExpressions.Regex.Replace(s, @"\s+", " ");

                            XmlText xmlText = xmlNode.OwnerDocument.CreateTextNode(s);
                            
                            // We'll need to post-process the doc to trim whitespace
                            // where possible from block-level elements.

                            xmlNode.AppendChild(xmlText);
                            totalChars += xmlText.Value.Length;

                            //if (true)
                            //{
                            //    // diagnostic
                            //    if (totalChars > 0)
                            //    {
                            //        int temp = totalChars - 1;
                            //        XmlDocPos pos = XmlDocPos.SeekToOffset(ref temp, xmlNode.OwnerDocument.DocumentElement);
                            //        System.Diagnostics.Debug.Assert(pos != null);
                            //    }
                            //}

                        }
                        break;
                    default:
                        throw new Exception("Unexpected HtmlNodeType: " + n.NodeType.ToString());
                }

                
            }
            

        }

        bool IncludeTagInTOC(HtmlNode node)
        {
            if (_tocTags.Contains(node.Name.ToLower()))
                return true;

            return false;
        }

        bool TagIsPermitted(string tagName)
        {
            if (_illegalTags.Contains(tagName.ToLower()))
                return false;

            return true;
        }

        internal const string _segMarkerEditPlaceholder = "q";

        HashSet<string> _illegalTags = new HashSet<string>() { "script", _segMarkerEditPlaceholder };

        HashSet<string> _legalAttribs = new HashSet<string>() { "class" };

        HashSet<string> _tocTags = new HashSet<string>() { "h1", "h2", "h3" };

        HashSet<string> _deStopWords = new HashSet<string>() { };

        // Entirely provisional and hard-coded
        HashSet<string> __unused = new HashSet<string>()
        {

"aber",
"alle",
"allem",
"allen",
"aller",
"alles",
"als",
"also",
"am",
"an",
"ander",
"andere",
"anderem",
"anderen",
"anderer",
"anderes",
"anderm",
"andern",
"anderr",
"anders",
"auch",
"auf",
"aus",
"bei",
"bin",
"bis",
"bist",
"da",
"damit",
"dann",
"der",
"den",
"des",
"dem",
"die",
"das",
"daß",
"derselbe",
"derselben",
"denselben",
"desselben",
"demselben",
"dieselbe",
"dieselben",
"dasselbe",
"dazu",
"dein",
"deine",
"deinem",
"deinen",
"deiner",
"deines",
"denn",
"derer",
"dessen",
"dich",
"dir",
"du",
"dies",
"diese",
"diesem",
"diesen",
"dieser",
"dieses",
"doch",
"dort",
"durch",
"ein",
"eine",
"einem",
"einen",
"einer",
"eines",
"einig",
"einige",
"einigem",
"einigen",
"einiger",
"einiges",
"einmal",
"er",
"ihn",
"ihm",
"es",
"etwas",
"euer",
"eure",
"eurem",
"euren",
"eurer",
"eures",
"für",
"gegen",
"gewesen",
"hab",
"habe",
"haben",
"hat",
"hatte",
"hatten",
"hier",
"hin",
"hinter",
"ich",
"mich",
"mir",
"ihr",
"ihre",
"ihrem",
"ihren",
"ihrer",
"ihres",
"euch",
"im",
"in",
"indem",
"ins",
"ist",
"jede",
"jedem",
"jeden",
"jeder",
"jedes",
"jene",
"jenem",
"jenen",
"jener",
"jenes",
"jetzt",
"kann",
"kein",
"keine",
"keinem",
"keinen",
"keiner",
"keines",
"können",
"könnte",
"machen",
"man",
"manche",
"manchem",
"manchen",
"mancher",
"manches",
"mein",
"meine",
"meinem",
"meinen",
"meiner",
"meines",
"mit",
"muss",
"musste",
"nach",
"nicht",
"nichts",
"noch",
"nun",
"nur",
"ob",
"oder",
"ohne",
"sehr",
"sein",
"seine",
"seinem",
"seinen",
"seiner",
"seines",
"selbst",
"sich",
"sie",
"ihnen",
"sind",
"so",
"solche",
"solchem",
"solchen",
"solcher",
"solches",
"soll",
"sollte",
"sondern",
"sonst",
"über",
"um",
"und",
"uns",
"unse",
"unsem",
"unsen",
"unser",
"unses",
"unter",
"viel",
"vom",
"von",
"vor",
"während",
"war",
"waren",
"warst",
"was",
"weg",
"weil",
"weiter",
"welche",
"welchem",
"welchen",
"welcher",
"welches",
"wenn",
"werde",
"werden",
"wie",
"wieder",
"will",
"wir",
"wird",
"wirst",
"wo",
"wollen",
"wollte",
"würde",
"würden",
"zu",
"zum",
"zur",
"zwar",
"zwischen"

        };

    }
}
