﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Collections.Specialized;

namespace EblaImpl
{
    internal class OpenableEblaItem
    {

        private NameValueCollection _configProvider;

        private MySqlConnection _cn;
        protected bool _open = false;
        private bool _isAdmin = false;

        private string _username;
        
        protected void CheckOpen()
        {
            if (!_open)
                throw new Exception(this.GetType().Name + " is not open.");
        }

        protected void CheckAdmin()
        {
            if (!_isAdmin)
                throw new Exception("Only administrators can perform this function.");
        }

        protected void MakeAdmin()
        {
            _isAdmin = true;
        }

        protected void SetConnection(MySqlConnection cn)
        {
            _cn = cn;
        }

        protected void CheckCanRead(string CorpusName)
        {
            if (_isAdmin)
                return;

            EblaAPI.CorpusRights[] rights = GetCorpusRights();

            foreach (var r in rights)
            {
                if (string.Compare(r.CorpusName, CorpusName) == 0)
                {
                    // TODO - wrap up rights logic elsewhere
                    if (r.CanRead || r.CanWrite)
                    {
                        return;
                    }
                    break;
                }
            }

            throw new Exception("You do not have sufficient rights to the corpus.");

        }

        protected void CheckCanWrite(string CorpusName)
        {
            if (_isAdmin)
                return;

            EblaAPI.CorpusRights[] rights = GetCorpusRights();

            foreach (var r in rights)
            {
                if (string.Compare(r.CorpusName, CorpusName) == 0)
                {
                    // TODO - wrap up rights logic elsewhere
                    if (r.CanWrite)
                    {
                        return;
                    }
                    break;
                }
            }

            throw new Exception("You do not have sufficient rights to the corpus.");

        }


        protected NameValueCollection ConfigProvider
        {
            get
            {
                return _configProvider;
            }
        }

        protected MySqlConnection GetConnection()
        {
            if (_cn != null)
                return _cn;

            MySqlConnection cn = null;

            // Are we running within some app that's trying to manage connections
            // for us?
            if (!EblaCxnMgr.Initialised())
            {
                // No
                _cn = EblaHelpers.GetConnection(_configProvider);
                return _cn;
            }

            object o = EblaCxnMgr.GetConnection();

            if (o == null)
            {
                cn = EblaHelpers.GetConnection(_configProvider);
                EblaCxnMgr.SetConnection(cn);
            }
            else
            {
                System.Diagnostics.Debug.Assert(o is MySqlConnection);
                cn = (MySqlConnection)o;
            }

            return cn;
        }

        protected EblaAPI.CorpusRights[] GetCorpusRights()
        {
            // TODO - more efficient caching

            EblaAPI.CorpusRights[] rights = null;
            object o = EblaCxnMgr.GetRequestData();

            if (o == null)
            {
                rights = CorpusStore.GetUserInfo(Username(), null, GetConnection())[0].CorpusRights;
                EblaCxnMgr.SetRequestData(rights);
            }
            else
            {
                System.Diagnostics.Debug.Assert(o is EblaAPI.CorpusRights[]);

                rights = (EblaAPI.CorpusRights[])o;
            }

            return rights;
        }



        protected OpenableEblaItem(System.Collections.Specialized.NameValueCollection configProvider)
        {
            _configProvider = configProvider;
        }

        protected bool Open(string Username, string Password)
        {
            if (!EblaHelpers.Authenticate(Username, Password, GetConnection(), out _isAdmin))
                return false;

            _username = Username;
            return true;
        }

        protected bool IsAdmin() { return _isAdmin; }
        protected string Username() { return _username; }

    }
}
