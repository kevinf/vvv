﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EblaAPI;
using MySql.Data.MySqlClient;

namespace EblaImpl
{
    internal class AlignmentSet : OpenableEblaItem, IAlignmentSet
    {

        #region "IAlignmentSet"


        public bool Open(string CorpusName, string VersionName, string Username, string Password)
        {
            if (!base.Open(Username, Password))
                return false;

            OpenWithoutPrivilegeCheck(CorpusName, VersionName, false, null);

            return true;
        }

        internal void OpenWithoutPrivilegeCheck(string CorpusName, string VersionName, bool makeAdmin, MySqlConnection cn)
        {
            if (makeAdmin)
                base.MakeAdmin();

            if (cn != null)
                SetConnection(cn);
            // Need the IDs of the two documents for later

            // First get corpus ID
            using (MySqlCommand cmd = new MySqlCommand("SELECT ID FROM corpora WHERE Name = @name", GetConnection()))
            {
                MySqlParameter nameParm = cmd.Parameters.AddWithValue("name", CorpusName);
                object o = cmd.ExecuteScalar();
                if (o == null)
                    throw new Exception("Corpus not found: " + CorpusName);

                _corpusid = (Int32)o;


                string sql = "SELECT ID FROM documents WHERE CorpusID = " + _corpusid.ToString();
                cmd.CommandText = sql + " AND Name IS NULL";

                o = cmd.ExecuteScalar();
                if (o == null)
                    throw new Exception("Base text not found for corpus: " + CorpusName);

                _basetextid = (Int32)o;
                cmd.CommandText = sql + " AND Name = @name";
                nameParm.Value = VersionName;
                o = cmd.ExecuteScalar();
                if (o == null)
                    throw new Exception("Version '" + VersionName + "' not found for corpus: " + CorpusName);

                _versionid = (Int32)o;

                _corpusname = CorpusName;
                _versionname = VersionName;

            }
            CheckCanRead(_corpusname);
            _open = true;

           
        }


        public int CreateAlignment(SegmentAlignment NewAlignment)
        {
            CheckOpen();
            CheckCanWrite(_corpusname);

            CheckAlignment(NewAlignment);


            using (MySqlTransaction txn = GetConnection().BeginTransaction())
            {
                using (MySqlCommand cmd = new MySqlCommand("INSERT INTO alignmentdetails (AlignmentType, AlignmentStatus, Notes) VALUES (@alignmenttype, @alignmentstatus, @notes)", GetConnection()))
                {
                    cmd.Transaction = txn;

                    cmd.Parameters.AddWithValue("alignmenttype", (int)(NewAlignment.AlignmentType));
                    cmd.Parameters.AddWithValue("alignmentstatus", (int)(NewAlignment.AlignmentStatus));
                    //cmd.Parameters.AddWithValue("basetextsegmentid", NewAlignment.SegmentIDInBaseText);
                    //cmd.Parameters.AddWithValue("versionsegmentid", NewAlignment.SegmentIDInVersion);
                    cmd.Parameters.AddWithValue("notes", NewAlignment.Notes);

                    cmd.ExecuteNonQuery();

                    int alignmentID = EblaHelpers.GetLastInsertID(cmd);

                    cmd.CommandText = "INSERT INTO segmentalignments (AlignmentID, BaseTextSegmentID, VersionSegmentID) VALUES (" + alignmentID.ToString() + ", @basetextsegmentid, @versionsegmentid)";
                    var basetextsegmentidparm = cmd.Parameters.AddWithValue("basetextsegmentid", 0);
                    var versionsegmentidparm = cmd.Parameters.AddWithValue("versionsegmentid", 0);
                    if (NewAlignment.SegmentIDsInBaseText.Length == 1)
                    {
                        basetextsegmentidparm.Value = NewAlignment.SegmentIDsInBaseText[0];
                        foreach (int versionsegmentid in NewAlignment.SegmentIDsInVersion)
                        {
                            versionsegmentidparm.Value = versionsegmentid;
                            cmd.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        versionsegmentidparm.Value = NewAlignment.SegmentIDsInVersion[0];
                        foreach (int basetextsegmentid in NewAlignment.SegmentIDsInBaseText)
                        {
                            basetextsegmentidparm.Value = basetextsegmentid;
                            cmd.ExecuteNonQuery();
                        }

                    }


                    txn.Commit();

                    return alignmentID;

                }

            }

        }

        public SegmentAlignment FindAlignment(int segIdFilter, bool baseText)
        {
            CheckOpen();
            CheckCanRead(_corpusname);

            string filter;
            if (baseText)
                filter = "BaseTextSegmentID = " + segIdFilter.ToString();
            else
                filter = "VersionSegmentID = " + segIdFilter.ToString();

            SegmentAlignment[] a = ReadAlignments(filter, null, null, _basetextid, _versionid, GetConnection(), null);

            if (a.Length == 0)
                return null;

            if (a.Length > 1)
                throw new Exception("Too many matching alignments!");

            return a[0];

        }

        public SegmentAlignment FindAlignment(int baseTextSegId, int versionSegId)
        {
            CheckOpen();
            CheckCanRead(_corpusname);

            string filter = "BaseTextSegmentID = " + baseTextSegId.ToString() + " AND VersionSegmentID = " + versionSegId.ToString();

            SegmentAlignment[] a = ReadAlignments(filter, null, null, _basetextid, _versionid, GetConnection(), null);

            if (a.Length == 0)
                return null;

            if (a.Length > 1)
                throw new Exception("Too many matching alignments!");

            return a[0];

        }

        public SegmentAlignment GetAlignment(int ID)
        {
            CheckOpen();
            CheckCanRead(_corpusname);

            string filter = "alignmentdetails.ID = " + ID.ToString();

            SegmentAlignment[] a = ReadAlignments(filter, null, null, _basetextid, _versionid, GetConnection(), null);

            if (a.Length == 0)
                return null;

            if (a.Length > 1)
                throw new Exception("Too many matching alignments!");

            return a[0];

        }

        public void UpdateAlignment(SegmentAlignment UpdatedAlignment)
        {
            CheckOpen();
            CheckCanWrite(_corpusname);

            CheckAlignment(UpdatedAlignment);

            SegmentAlignment current = GetAlignment(UpdatedAlignment.ID);
            if (current == null)
                throw new Exception("Alignment with ID " + UpdatedAlignment.ID.ToString() + " not found.");

            Dictionary<string, List<int>> existingPairs = new Dictionary<string, List<int>>();
            if (current.SegmentIDsInBaseText.Length == 1)
            {
                foreach (int id in current.SegmentIDsInVersion)
                {
                    existingPairs.Add(current.SegmentIDsInBaseText[0].ToString() + "-" + id.ToString(), new List<int>() { current.SegmentIDsInBaseText[0], id });
                }
            }
            else
            {
                foreach (int id in current.SegmentIDsInBaseText)
                {
                    existingPairs.Add(id.ToString() + "-" + current.SegmentIDsInVersion[0].ToString(), new List<int>() { id, current.SegmentIDsInVersion[0] });
                }

            }

            using (MySqlTransaction txn = GetConnection().BeginTransaction())
            {
                using (MySqlCommand cmd = new MySqlCommand("UPDATE alignmentdetails SET AlignmentType = @alignmenttype, AlignmentStatus = @alignmentstatus, Notes = @notes WHERE ID = " + UpdatedAlignment.ID.ToString(), GetConnection()))
                {
                    cmd.Transaction = txn;
                    cmd.Parameters.AddWithValue("alignmenttype", (int)(UpdatedAlignment.AlignmentType));
                    cmd.Parameters.AddWithValue("alignmentstatus", (int)(UpdatedAlignment.AlignmentStatus));
                    //cmd.Parameters.AddWithValue("basetextsegmentid", UpdatedAlignment.SegmentIDInBaseText);
                    //cmd.Parameters.AddWithValue("versionsegmentid", UpdatedAlignment.SegmentIDInVersion);
                    cmd.Parameters.AddWithValue("notes", UpdatedAlignment.Notes);

                    cmd.ExecuteNonQuery();

                    cmd.CommandText = "INSERT INTO segmentalignments (AlignmentID, BaseTextSegmentID, VersionSegmentID) VALUES (" + UpdatedAlignment.ID.ToString() + ", @basetextsegmentid, @versionsegmentid)";
                    var basetextsegmentidparm = cmd.Parameters.AddWithValue("basetextsegmentid", 0);
                    var versionsegmentidparm = cmd.Parameters.AddWithValue("versionsegmentid", 0);

                    if (UpdatedAlignment.SegmentIDsInBaseText.Length == 1)
                    {
                        basetextsegmentidparm.Value = UpdatedAlignment.SegmentIDsInBaseText[0];
                        foreach (int id in UpdatedAlignment.SegmentIDsInVersion)
                        {
                            string key = UpdatedAlignment.SegmentIDsInBaseText[0].ToString() + "-" + id.ToString();
                            if (existingPairs.ContainsKey(key))
                            {
                                existingPairs.Remove(key);
                            }
                            else
                            {
                                versionsegmentidparm.Value = id;
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                    else
                    {
                        versionsegmentidparm.Value = UpdatedAlignment.SegmentIDsInVersion[0];
                        foreach (int id in UpdatedAlignment.SegmentIDsInBaseText)
                        {
                            string key = id.ToString() + "-" + UpdatedAlignment.SegmentIDsInVersion[0].ToString();
                            if (existingPairs.ContainsKey(key))
                            {
                                existingPairs.Remove(key);
                            }
                            else
                            {
                                basetextsegmentidparm.Value = id;
                                cmd.ExecuteNonQuery();
                            }

                        }

                    }

                    // in principle, the AlignmentID filter isn't necessary ...
                    cmd.CommandText = "DELETE FROM segmentalignments WHERE AlignmentID = " + UpdatedAlignment.ID.ToString() + " AND BaseTextSegmentID = @basetextsegmentid AND VersionSegmentID = @versionsegmentid";
                    foreach (List<int> pair in existingPairs.Values)
                    {
                        basetextsegmentidparm.Value = pair[0];
                        versionsegmentidparm.Value = pair[1];
                        cmd.ExecuteNonQuery();
                    }

                }


                txn.Commit();
            }



        }

        public void DeleteAlignment(int ID)
        {
            CheckOpen();
            CheckCanWrite(_corpusname);

            using (MySqlCommand cmd = new MySqlCommand("DELETE FROM alignmentdetails  WHERE ID = " + ID.ToString(), GetConnection()))
            {
                cmd.ExecuteNonQuery();

            }
        }

        public void DeleteAlignments(bool DraftOnly, SegmentAttribute[] BaseTextAttributeFilters, SegmentAttribute[] VersionAttributeFilters, int basetextStartPos, int baseTextLength, int versionStartPos, int versionLength)
        {
            CheckOpen();
            CheckCanWrite(_corpusname);

            List<MySqlParameter> parms = new List<MySqlParameter>();

            string filter = string.Empty;

            filter = "(" + Document.SegdefBoundsFilter(basetextStartPos, baseTextLength, true, true, "basesegmentdefinitions.") + ") ";
            filter += " AND (" + Document.SegdefBoundsFilter(versionStartPos, versionLength, true, true, "versionsegmentdefinitions.") + ") ";

            string sql = "DELETE FROM alignmentdetails WHERE ID IN (";


            sql += GetNestedSelect(filter, BaseTextAttributeFilters, VersionAttributeFilters, parms, _basetextid, _versionid);


            //sql += "SELECT AlignmentID ";
            //sql += "FROM segmentalignments  ";
            //sql += "INNER JOIN segmentdefinitions as basesegmentdefinitions ON segmentalignments.BaseTextSegmentID = basesegmentdefinitions.ID ";
            //sql += "INNER JOIN segmentdefinitions as versionsegmentdefinitions ON segmentalignments.VersionSegmentID = versionsegmentdefinitions.ID ";
            //sql += "WHERE basesegmentdefinitions.DocumentID = " + _basetextid.ToString() + " ";
            //sql += "AND versionsegmentdefinitions.DocumentID = " + _versionid.ToString() + " ";
            sql += ")";
            if (DraftOnly)
                sql += " AND AlignmentStatus = " + ((int)(AlignmentStatuses.draft)).ToString();

            using (MySqlCommand cmd = new MySqlCommand(sql, GetConnection()))
            {
                cmd.Parameters.AddRange(parms.ToArray());
                cmd.ExecuteNonQuery();

            }

        }

        public SegmentAlignment[] FindAlignments(SegmentAttribute[] BaseTextAttributeFilters, SegmentAttribute[] VersionAttributeFilters, int basetextStartPos, int baseTextLength, int versionStartPos, int versionLength)
        {
            CheckOpen();
            CheckCanRead(_corpusname);

            string filter = string.Empty;
            //int basetextEndPosition = basetextStartPos + baseTextLength;
            //int versionEndPosition = versionStartPos + versionLength;

            filter = "(" + Document.SegdefBoundsFilter(basetextStartPos, baseTextLength, true, true, "basesegmentdefinitions.") + ") ";
            filter += " AND (" + Document.SegdefBoundsFilter(versionStartPos, versionLength, true, true, "versionsegmentdefinitions.") + ") ";


            //if (basetextStartPos.HasValue)
            //    filter = AndFilter("basesegmentdefinitions.StartPosition >= " + basetextStartPos.Value.ToString());
            //if (versionStartPos.HasValue)
            //    filter = AndFilter("versionsegmentdefinitions.StartPosition >= " + versionStartPos.Value.ToString());

            return ReadAlignments(filter, BaseTextAttributeFilters, VersionAttributeFilters, _basetextid, _versionid, GetConnection(), null);
        }

        public void ConfirmAlignments(SegmentAttribute[] BaseTextAttributeFilters, SegmentAttribute[] VersionAttributeFilters, int basetextStartPos, int baseTextLength, int versionStartPos, int versionLength)
        {
            CheckOpen();
            CheckCanWrite(_corpusname);

            string filter = string.Empty;

            filter = "(" + Document.SegdefBoundsFilter(basetextStartPos, baseTextLength, true, true, "basesegmentdefinitions.") + ") ";
            filter += " AND (" + Document.SegdefBoundsFilter(versionStartPos, versionLength, true, true, "versionsegmentdefinitions.") + ") ";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            // TODO - refactor to get rid of duplication versus ReadAlignments

            string sql = "UPDATE alignmentdetails SET AlignmentStatus = " + ((int)AlignmentStatuses.confirmed).ToString() + " WHERE AlignmentStatus = " + ((int)AlignmentStatuses.draft).ToString();
            sql += " AND ID IN (";

            sql += GetNestedSelect(filter, BaseTextAttributeFilters, VersionAttributeFilters, parms, _basetextid, _versionid);

            sql += ") ";

            using (MySqlCommand cmd = new MySqlCommand(sql, GetConnection()))
            {
                cmd.Parameters.AddRange(parms.ToArray());

                cmd.ExecuteNonQuery();
            }


        }



        #endregion


        int _corpusid;
        int _basetextid;
        int _versionid;
        string _versionname;
        string _corpusname;

        public AlignmentSet(System.Collections.Specialized.NameValueCollection configProvider)
            : base(configProvider)
        {

        }

        private void CheckAlignment(SegmentAlignment alignment)
        {
            if (alignment.SegmentIDsInBaseText == null || alignment.SegmentIDsInVersion == null || alignment.SegmentIDsInBaseText.Length == 0 || alignment.SegmentIDsInVersion.Length == 0)
                throw new Exception("At least one segment ID must be specified for the base text and the version document.");

            if ((alignment.SegmentIDsInBaseText.Length > 1) && (alignment.SegmentIDsInVersion.Length > 1))
                throw new Exception("Many-to-many alignments are not supported.");

#if DEBUG
            // Check segments refer to correct docs 
            string debugsql = "SELECT DocumentID FROM segmentdefinitions WHERE ID = @id";
            using (MySqlCommand cmd = new MySqlCommand(debugsql, GetConnection()))
            {
                MySqlParameter idparm = cmd.Parameters.AddWithValue("id", 0);

                foreach (int id in alignment.SegmentIDsInBaseText)
                {
                    idparm.Value = id;

                    object o = cmd.ExecuteScalar();
                    if (o == null)
                        throw new Exception("Segment definition not found with ID: " + id.ToString());

                    if ((Int32)o != _basetextid)
                        throw new Exception("Segment definition with ID " + id.ToString() + " does not refer to base text");
                }

                foreach (int id in alignment.SegmentIDsInVersion)
                {
                    idparm.Value = id;

                    object o = cmd.ExecuteScalar();
                    if (o == null)
                        throw new Exception("Segment definition not found with ID: " + id.ToString());

                    if ((Int32)o != _versionid)
                        throw new Exception("Segment definition with ID " + id.ToString() + " does not refer to version");
                }


            }
#endif


        }



        internal static SegmentAlignment[] ReadAlignments(string filter, SegmentAttribute[] BaseTextAttributeFilters, SegmentAttribute[] VersionAttributeFilters, int baseTextID, int? versionID, MySqlConnection cn, string attributeSought)//, int? baseTextSegIdFilter, int? versionSegIdFilter)
        {
            List<MySqlParameter> parms = new List<MySqlParameter>();

            string sql = "SELECT alignmentdetails.ID, AlignmentType, AlignmentStatus, BaseTextSegmentID, VersionSegmentID, alignmentdetails.Notes ";

            if (attributeSought != null)
                sql += ", DocumentID, Value";

            sql += "FROM alignmentdetails ";
            sql += "INNER JOIN segmentalignments ON alignmentdetails.ID = segmentalignments.AlignmentID ";
            if (attributeSought != null)
                sql += "INNER JOIN segmentattributes ON segmentalignments.VersionSegmentID = segmentattributes.SegmentDefinitionID INNER JOIN segmentdefinitions ON segmentattributes.SegmentDefinitionID = segmentdefinitions.ID ";
            sql += " WHERE alignmentdetails.ID IN (";

            sql += GetNestedSelect(filter, BaseTextAttributeFilters, VersionAttributeFilters, parms, baseTextID, versionID);

            sql += ") ORDER BY alignmentdetails.ID ";
            
            List<SegmentAlignment> alignments = new List<SegmentAlignment>();
            using (MySqlCommand cmd = new MySqlCommand(sql, cn))
            {
                cmd.Parameters.AddRange(parms.ToArray());

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {

                    if (dr.Read())
                    {
                        int alignmenttypeOrdinal = dr.GetOrdinal("AlignmentType");
                        int alignmentstatusOrdinal = dr.GetOrdinal("AlignmentStatus");
                        int basetextsegmentidOrdinal = dr.GetOrdinal("BaseTextSegmentID");
                        int versionsegmentidOrdinal = dr.GetOrdinal("VersionSegmentID");
                        int notesOrdinal = dr.GetOrdinal("Notes"); 
                        int idOrdinal = dr.GetOrdinal("ID");

                        SegmentAlignment lastAlignment = null;

                        do
                        {
                            int id = dr.GetInt32(idOrdinal);
                            int basetextsegid = dr.GetInt32(basetextsegmentidOrdinal);
                            int versionsegid = dr.GetInt32(versionsegmentidOrdinal);

                            if (lastAlignment != null)
                                if (id == lastAlignment.ID)
                                {
                                    var baseTextSegIds = new HashSet<int>(lastAlignment.SegmentIDsInBaseText);
                                    var versionSegIds = new HashSet<int>(lastAlignment.SegmentIDsInVersion);
                                    if (!baseTextSegIds.Contains(basetextsegid))
                                        baseTextSegIds.Add(basetextsegid);

                                    if (!versionSegIds.Contains(versionsegid))
                                        versionSegIds.Add(versionsegid);

                                    if (baseTextSegIds.Count > 1 && versionSegIds.Count > 1)
                                        throw new Exception("Segment alignment data corrupted!");


                                    var sortedBaseTextSegIds = new List<int>(baseTextSegIds);
                                    sortedBaseTextSegIds.Sort();
                                    var sortedVersionSegIds = new List<int>(versionSegIds);
                                    sortedVersionSegIds.Sort();

                                    lastAlignment.SegmentIDsInBaseText = sortedBaseTextSegIds.ToArray();
                                    lastAlignment.SegmentIDsInVersion = sortedVersionSegIds.ToArray();

                                    continue;
                                }

                            SegmentAlignment a = new SegmentAlignment();

                            a.ID = dr.GetInt32(idOrdinal);
                            a.AlignmentType = (AlignmentTypes)(dr.GetInt32(alignmenttypeOrdinal));
                            a.AlignmentStatus = (AlignmentStatuses)(dr.GetInt32(alignmentstatusOrdinal));
                            a.SegmentIDsInBaseText = new int[] { basetextsegid };
                            a.SegmentIDsInVersion = new int[] {versionsegid} ;
                            a.Notes = dr.IsDBNull(notesOrdinal) ? string.Empty : dr.GetString(notesOrdinal);

                            lastAlignment = a;
                            alignments.Add(a);

                        } while (dr.Read());

                        
                    }
                }


                
            }

            return alignments.ToArray();

            
        }


        internal static string GetNestedSelect(string filter, SegmentAttribute[] BaseTextAttributeFilters, SegmentAttribute[] VersionAttributeFilters, List<MySqlParameter> parms, int baseTextID, int? versionID)
        {
            string sql = "SELECT AlignmentID ";
            sql += "FROM segmentalignments  ";
            sql += "INNER JOIN segmentdefinitions as basesegmentdefinitions ON segmentalignments.BaseTextSegmentID = basesegmentdefinitions.ID ";
            sql += "INNER JOIN segmentdefinitions as versionsegmentdefinitions ON segmentalignments.VersionSegmentID = versionsegmentdefinitions.ID ";
            if (BaseTextAttributeFilters != null)
            {
                int count = 0;
                foreach (var f in BaseTextAttributeFilters)
                {
                    sql += "INNER JOIN segmentattributes as basetextsegmentattributes" + count.ToString() + " ON basesegmentdefinitions.ID = basetextsegmentattributes" + count.ToString() + ".SegmentDefinitionID ";
                    if (!string.IsNullOrEmpty(filter))
                        filter += " AND ";
                    string nameparmname = "baseattribname" + count.ToString();
                    string valparmname = "baseattribvalue" + count.ToString();
                    MySqlParameter p = new MySqlParameter(nameparmname, f.Name);
                    parms.Add(p);
                    p = new MySqlParameter(valparmname, f.Value);
                    parms.Add(p);
                    filter += " (basetextsegmentattributes" + count.ToString() + ".Name = @" + nameparmname + " AND basetextsegmentattributes" + count.ToString() + ".Value = @" + valparmname + ") ";
                    count++;
                }
            }
            if (VersionAttributeFilters != null)
            {
                int count = 0;
                foreach (var f in VersionAttributeFilters)
                {
                    sql += "INNER JOIN segmentattributes as versionsegmentattributes" + count.ToString() + " ON versionsegmentdefinitions.ID = versionsegmentattributes" + count.ToString() + ".SegmentDefinitionID ";
                    if (!string.IsNullOrEmpty(filter))
                        filter += " AND ";
                    string nameparmname = "versionattribname" + count.ToString();
                    string valparmname = "versionattribvalue" + count.ToString();
                    MySqlParameter p = new MySqlParameter(nameparmname, f.Name);
                    parms.Add(p);
                    p = new MySqlParameter(valparmname, f.Value);
                    parms.Add(p);
                    filter += " (versionsegmentattributes" + count.ToString() + ".Name = @" + nameparmname + " AND versionsegmentattributes" + count.ToString() + ".Value = @" + valparmname + ") ";
                    count++;
                }
            }


            sql += "WHERE basesegmentdefinitions.DocumentID = " + baseTextID.ToString() + " ";
            if (versionID.HasValue)
                sql += "AND versionsegmentdefinitions.DocumentID = " + versionID.Value.ToString() + " ";


            if (!string.IsNullOrEmpty(filter))
                sql += " AND " + filter + " ";


            return sql;
        }

   

    }
}
